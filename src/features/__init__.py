from src.features.feature_extractors.adjacency_matrix_extractor import AdjacencyMatrixExtractor
from src.features.feature_extractors.all_distance_matrix_extractor import AllDistanceMatrixExtractor
from src.features.feature_extractors.decoy_distance_matrix_extractor import DecoyDistanceMatrixExtractor
from src.features.feature_extractors.distance_matrix_extractor import DistanceMatrixAveExtractor, DistanceMatrixStdExtractor, \
    DistanceMatrixMinExtractor, DistanceMatrixMaxExtractor, DistanceMatrixSeqExtractor

from src.features.feature_extractors.HSExposureExtractor import HSExposureExtractor
from src.features.feature_extractors.half_sphere_amino_acid_composition_extractor import \
    HalfSphereAminoAcidCompositionExtractor
from src.features.feature_extractors.predicted_ss_extractor import PredictedSSExtractor
from src.features.feature_extractors.profile_extractor import ProfileExtractor
from src.features.feature_extractors.protrusion_index_extractor import ProtrusionIndexExtractor
from src.features.feature_extractors.rasa_extractor import RASAExtractor
from src.features.feature_extractors.residue_depth_extractor import ResidueDepthExtractor
from src.features.feature_extractors.stride_secondary_structure_extractor import StrideSecondaryStructureExtractor
from src.features.feature_extractors.wpsfm_extractor import WPSFMExtractor
from src.features.feature_extractors.wpssm_extractor import WPSSMExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

Features = {
    "SEQUENCE_PROFILE": 0,
    "POSITION_SPECIFIC_SCORING_MATRIX": 1,
    "POSITION_SPECIFIC_FREQUENCY_MATRIX": 2,
    "WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX": 3,
    "WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX": 4,
    "D1_PLAIN_SHAPE_DISTRIBUTION": 5,
    "D2_PLAIN_SHAPE_DISTRIBUTION": 6,
    "D2_CATEGORY_SHAPE_DISTRIBUTION": 7,
    "SECONDARY_STRUCTURE": 8,
    "RELATIVE_ACCESSIBLE_SURFACE_AREA": 9,
    "ACCESSIBLE_SURFACE_AREA": 10,
    "PHI": 11,
    "PSI": 12,
    "D2_SURFACE_SHAPE_DISTRIBUTION": 13,
    "NUMBER_OF_NEIGHBOURS": 14,
    "D1_SURFACE_SHAPE_DISTRIBUTION": 15,
    "RESIDUE_DEPTH": 16,
    "HALF_SPHERE_EXPOSURE": 17,
    "RESIDUE_NEIGHBOURHOOD": 18,
    "B_VALUE": 19,
    "PROTRUSION_INDEX": 20,
    "D1_CATEGORY_SHAPE_DISTRIBUTION": 21,
    "PROFILE": 22,
    "D1_SURFACE_ATOM_SHAPE_DISTRIBUTION": 23,
    "D2_SURFACE_ATOM_SHAPE_DISTRIBUTION": 24,
    "D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION": 25,
    "D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION": 26,
    "PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA": 27,
    "D1_MULTI_LEVEL_SHAPE_DISTRIBUTION": 28,
    "SHORT_PROFILE": 29,
    "NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT": 30,
    "NEIGHBOURHOOD_SURFACE_DOT_PRODUCT": 31,
    "SURFACE_ATOMS_CURVATURE": 32,
    "COMPLETE_GRAPH_NN": 33,
    "GAUSSIAN_KERNEL_DISTANCE": 34,
    "EUCLIDEAN_DISTANCE": 35,
    "BIOLOGICAL_NOISE": 36,
    "INTERFACE_CLASS_LABELS": 37,
    "RES_DIST_MIN": 38,
    "RES_DIST_MAX": 39,
    "RES_DIST_AVE": 40,
    "RES_DIST_STD": 41,
    "RES_DIST_CA": 42,
    "RES_DIST_SEQ": 43,
    "RES_DIST_HST": 44,
    "ANGLE_CCAO": 45,
    "ANGLE_CCAO_COS": 46,
    "ANGLE_CANC": 47,
    "ANGLE_CANC_COS": 48,
    "ANGLE_HSAA": 49,
    "ANGLE_HSAA_COS": 50,
    "ANGLE_SURF": 51,
    "ANGLE_SURF_COS": 52,
    "CONTACT_LOCATIONS": 53,
    "STRIDE_SECONDARY_STRUCTURE": 54,
    "RES_ALL_DIST": 55,
    "RES_DECOY_DIST": 56
}

FeatureExtractors = {
    "WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX":
        (WPSSMExtractor, "WPSSM", "vertex"),
    "WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX":
        (WPSFMExtractor, "WPSFM", "vertex"),
    "PROFILE":
        (ProfileExtractor, "Profile", "vertex"),
    "RELATIVE_ACCESSIBLE_SURFACE_AREA":
        (RASAExtractor, "rASA", "vertex"),
    "STRIDE_SECONDARY_STRUCTURE":
        (StrideSecondaryStructureExtractor, "Stride Secondary ", "vertex"),
    "RESIDUE_DEPTH":
        (ResidueDepthExtractor, "Residue Depth", "vertex"),
    "HS_EXPOSURE":
        (HSExposureExtractor, "Half Sphere Exposure", "vertex"),
    "HALF_SPHERE_EXPOSURE":
        (HalfSphereAminoAcidCompositionExtractor, "Half Sphere Amino Acid Composition", "vertex"),
    "PROTRUSION_INDEX":
        (ProtrusionIndexExtractor, "Protrusion Index", "vertex"),
    "PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA":
        (PredictedSSExtractor, "Predicted rASA", "vertex"),
    # "SHORT_PROFILE:
    #     (ShortWindowProfileExtractor, "Short Profile", "vertex"),
    # "NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT:
    #     (NeighbourhoodResidueDotProductExtractor, "Neighbourhood Residue Dot Product", "vertex"),
    # "NEIGHBOURHOOD_SURFACE_DOT_PRODUCT:
    #     (NeighbourhoodSurfaceDotProductExtractor, "Neighbourhood Surface Dot Product", "vertex"),
    # "SURFACE_ATOMS_CURVATURE:
    #     (SurfaceAtomsCurvatureExtractor, "Surface Atoms Curvature", "vertex"),
    # "GAUSSIAN_KERNEL_DISTANCE:
    #     (None, "Gaussian Kernel Distance", "edge"),
    #    "BIOLOGICAL_NOISE:
    #        (BiologicalNoiseExtractor, "Biological Noise", "vertex"),
    # "CONTACT_LOCATIONS:
    #     (ContactExampleExtractor, "Residue Contacts Class Labels", "label"),
    # "INTERFACE_CLASS_LABELS:
    #     (ExampleExtractor, "Residue Interface Class Labels", "label"),
    "ADJ_MAT":
        (AdjacencyMatrixExtractor, "Adjacency Matrix ", "edge"),
    "RES_DIST_MIN":
        (DistanceMatrixMinExtractor, "Minimum Atomic Distance Between Residues", "edge"),
    "RES_DIST_MAX":
        (DistanceMatrixMaxExtractor, "Maximum Atomic Distance Between Residues", "edge"),
    "RES_DIST_AVE":
        (DistanceMatrixAveExtractor, "Average Atomic Distance Between Residues", "edge"),
    "RES_DIST_STD":
        (DistanceMatrixStdExtractor, "Standard Deviation of Atomic Distance Between Residues", "edge"),
    # "RES_DIST_CA:
    #     (DistanceMatrixCAExtractor, "Alpha Carbon Distance Between Residues", "edge"),
    "RES_DIST_SEQ":
        (DistanceMatrixSeqExtractor, "Sequence Distance Between Residues", "edge"),
    "RES_ALL_DIST":
        (AllDistanceMatrixExtractor, "All Distance features", "edge"),
    "RES_DECOY_DIST":
        (DecoyDistanceMatrixExtractor, "Decoy Ranking Distance features", "edge")
    # "ANGLE_CCAO:
    #     (AngleMatrixCCAOExtractor, "CA-C=O Plane Normal Angle Between Residues", "edge"),
    # "ANGLE_CCAO_COS:
    #     (AngleMatrixCCAOCosExtractor, "CA-C=O Plane Normal Cos Angle Between Residues", "edge"),
    # "ANGLE_CANC:
    #     (AngleMatrixCANCExtractor, "C-CA-N Normal Angle Between Residues", "edge"),
    # "ANGLE_CANC_COS:
    #     (AngleMatrixCANCCosExtractor, "C-CA-N Normal Cos Angle Between Residues", "edge"),
    # "ANGLE_HSAA:
    #     (AngleMatrixHSAAExtractor, "HSAA Normal Angle Between Residues", "edge"),
    # "ANGLE_HSAA_COS:
    #     (AngleMatrixHSAACosExtractor, "HSAA Normal Cos Angle Between Residues", "edge"),
    # "ANGLE_SURF:
    #     (AngleMatrixSURFExtractor, "Nearest Surface Angle Between Residues", "edge"),
    # "ANGLE_SURF_COS:
    #     (AngleMatrixSURFCosExtractor, "Nearest Surface Cos Angle Between Residues", "edge"),
}