import os
from Bio.PDB import NeighborSearch
import numpy as np

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ResidueNeighbourhoodExtractor(AbstractFeatureExtractor):
    def __init__(self, conf):
        self.radius = conf["neighborhood_r"] if "neighborhood_r" in conf else conf["reddinng"]["neighborhood_r"]
        super(ResidueNeighbourhoodExtractor, self).__init__(conf)

    def _get_directory(self):
        return os.path.join(self._configuration["reddinng"]["neighbourhood_directory"], str(self.radius))

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, protein_name + ".npy")

    def extract(self, protein, save=True):
        residue_neighbourhood_file = self._get_file_name(protein.name)
        if not os.path.exists(residue_neighbourhood_file):
            self._logger.info("Computing residue neighbourhood for {0}".format(protein.name))
            n = len(protein.residues)
            ns = NeighborSearch(protein.atoms)
            neighbourhood = []
            max_length = 0
            for i, query_residue in enumerate(protein.residues):
                neighbors = []
                for atom in query_residue.residue.get_list():
                    for r in ns.search(atom.get_coord(), self.radius, 'R'):
                        if r in protein.residues_index and protein.residues_index[r] != i:
                            neighbors.append(protein.residues_index[r])
                neighbors = list(set(neighbors))
                neighbourhood.append(neighbors)
                if len(neighbors) > max_length:
                    max_length = len(neighbors)
            neighborhood_array = -np.ones((n, max_length))
            for i, neighbors in enumerate(neighbourhood):
                neighborhood_array[i, :len(neighbors)] = neighbors
            if save:
                np.save(residue_neighbourhood_file, neighborhood_array)
            return neighborhood_array.astype(int)
        else:
            return np.load(residue_neighbourhood_file).astype(int)
