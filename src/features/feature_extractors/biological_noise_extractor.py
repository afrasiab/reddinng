import os
import logging
import pickle
import numpy as np

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
import Bio
from data.feature_extractors import AbstractFeatureExtractor
from configuration import biological_noise_directory

__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'

__all__ = [
    "BiologicalNoiseExtractor"
]


class BiologicalNoiseExtractor(AbstractFeatureExtractor):
    """Note: having this extractor compute (when needed) the graph representations as inputs to the neural network
    would cause a circular import. Therefore the representations must be computed already"""
    def __init__(self, database_directory, protein_name=None, protein=None, bionoise_params=None, match_window=None, **kwargs):
        super(BiologicalNoiseExtractor, self).__init__(database_directory, protein_name, protein, **kwargs)
        if bionoise_params is None:
            logger.error("No params provided for BiologicalNoiseExtractor")
            raise ValueError
        self.match_window = bionoise_params["match_window"]
        self.dataset = bionoise_params["dataset"]
        self.cluster_file = bionoise_params["clusterfile"]
        assert (self.match_window % 2 == 1)

    def _get_file_name(self, protein_name):
        directory = os.path.join(self.features_directory, biological_noise_directory, self.match_window)
        if not os.path.exists(directory):
            os.makedirs(directory)
        return os.path.join(directory, protein_name + ".npy")

    def load(self, protein_name):
        filename = self._get_file_name(protein_name)
        feature = np.load(filename)
        return feature

    def extract(self):
        filen = self._get_file_name(self.protein_name)
        if not os.path.exists(filen):
            logger.info("Computing biological noise for {0}".format(self.protein_name))
            # align similar proteins to this protein
            self.find_sequence_analogs()
            # calculate the desired representation for each similar protein
            reps = {}
            for nbor in self.cluster_neighbors:
                for extractor in feature
                rep._protein = nbor
                rep.chain_code = None
                rep.compute_representation()
                reps[nbor.name] = rep._load_representation(None)

            # create the representation
            if len(self.cluster_neighbors) == 0:
                # if no neighbors, then no noise
                rep._protein = self._protein
                rep.chain_code = None
                rep.compute_representation()
                data = rep._load_representation(None)[self.element]
                data.fill(np.nan)
            else:
                n_residues = len(self._protein.residues)
                shape = reps.values()[0][self.element].shape
                data = np.empty((n_residues, shape[1], shape[2]))
                data.fill(np.nan)
                for i in range(n_residues):
                    if str(i) in self.matched_residues:
                        # randomly pick an aligned neighbor (from among best alignments)
                        idx = np.random.randint(len(self.matched_residues[str(i)]))
                        nbor_name, nbor_idx, _ = self.matched_residues[str(i)][idx]
                        data[i] = reps[nbor_name][self.element][nbor_idx]
            np.save(filen, data)

    def get_cluster_neighbors(self):
        all_neighbors = ProteinSet(self.protein_set)
        cluster_dict = pickle.load(open(selected_chains_file))
        if self.name in cluster_dict:
            # this protein is a key in the dict
            key = self.name
        else:
            # this protein is a val in the dict
            for key, val in cluster_dict.items():
                if self.name in val:
                    break
        nbor_names = []
        self.cluster_neighbors = []
        for nbor in cluster_dict[key]:
            if all([nbor.name not in name for name in nbor_names]):
                try:
                    self.cluster_neighbors.append(all_neighbors.get_protein(key))
                except:
                    logger.error("Cannot load protein {}".format(key))
        # self.cluster_neighbors = [all_neighbors.get_protein(nbor) for nbor in cluster_dict[key]]
        if all([key not in name for name in nbor_names]):
            try:
                self.cluster_neighbors.append(all_neighbors.get_protein(key))
            except:
                logger.error("Cannot load protein {}".format(key))
        # remove self
        self.cluster_neighbors = list(set([prot for prot in self.cluster_neighbors if prot.name not in self.name]))
        return self.cluster_neighbors

    def find_sequence_analogs(self):
        matched_residues = {}
        self.get_cluster_neighbors()
        hw = (self.match_window - 1) / 2  # half window width
        self.hw = hw
        for nbor in self.cluster_neighbors:
            aln = Bio.pairwise2.align.globalxs(self.protein.sequence, nbor.sequence, -1, -0.1)
            best_alignment = aln[0]
            alignment_seq1 = best_alignment[0]
            alignment_seq2 = best_alignment[1]
            idx1 = idx2 = hw
            # for each residue index (excluding edges)
            for i in range(hw, len(alignment_seq1) - hw):
                if alignment_seq1[i] == "-" or idx1 == len(self.protein.sequence)-hw or idx2 == len(nbor.sequence)-hw:
                    continue
                score = 0
                # check for indels/mismatch
                for k in range(-hw, hw):
                    if alignment_seq1[i + k] != alignment_seq2[i + k]:
                        score += 1
                # add to list of matched residues if equal or lower score than best score for this position
                key = "{}".format(idx1)
                if key not in matched_residues:
                    matched_residues[key] = [(nbor.name, idx2, score)]
                else:
                    if score < matched_residues[key][0][2]:
                        matched_residues[key] = [(nbor.name, idx2, score)]
                    elif score == matched_residues[key][0][2]:
                        matched_residues[key].append((nbor.name, idx2, score))

                idx1 += alignment_seq1[i] != '-'
                idx2 += alignment_seq2[i] != '-'
        self.matched_residues = matched_residues
        return matched_residues


    #Basir exmple function
#    def biological_noise(match_window=5):
#        selected_chains = cPickle.load(open(selected_chains_file))
#        matched_residues = {}
#        # for each cluster
#        for representative in selected_chains:
#            cluster_chains = set(selected_chains[representative])
#            cluster_chains.add(representative)
#            cluster_chains = list(cluster_chains)
#            n = len(cluster_chains)
#            # for each protein pair in the cluster
#            for i in range(n):
#                for j in range(i, n):
#                    c1, c2 = cluster_chains[i], cluster_chains[j]
#                    protein_objs = []
#                    # load each protein
#                    for chain in [c1, c2]:
#                        pdb_code = chain.split(".")[0]
#                        compressed_file = glob.glob("{0}{1}.*.gz".format(unsupervised_pdb_directory, pdb_code))[0]
#                        decompressed_file = compressed_file.replace(".gz", "")
#                        if not os.path.exists(decompressed_file):
#                            os.system("gunzip -c {} > {}".format(compressed_file, decompressed_file))
#                        protein_objs.append(Protein(*PDB.read_pdb_file(decompressed_file)))
#                    # get the sequence
#                    seq1 = protein_objs[0].sequence
#                    seq2 = protein_objs[1].sequence
#                    # align them
#                    aln = Bio.pairwise2.align.globalxs(seq1, seq2, -1, -0.1)
#                    best_alignment = aln[0]
#                    alignment_seq1 = best_alignment[0]
#                    alignment_seq2 = best_alignment[1]
#                    c1_index = match_window
#                    c2_index = match_window
#
#                    # for each residue index (excluding edges)
#                    for ii in range(match_window, len(alignment_seq1) - match_window):
#                        matched = True
#                        # check for indels
#                        for k in range(-match_window, match_window):
#                            if k == 0:
#                                continue
#                            if alignment_seq1[ii + k] != alignment_seq2[ii + k]:
#                                matched = False
#                                break
#                        # if no indels:
#                        if matched:
#                            key = "{}_{}".format(c1, c2)
#                            if key not in matched_residues:
#                                matched_residues[key] = []
#                            matched_residues[key].append((c1_index, c2_index))
#                        c1_index += alignment_seq1[i] != '-'
#                        c2_index += alignment_seq2[i] != '-'
#        # return a dict of lists, each list for a different protein pair. elements are tuples of matched indices
#        return matched_residues
