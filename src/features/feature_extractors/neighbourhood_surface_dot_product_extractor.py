import os
import logging
import pickle
import numpy as np

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import neighbourhood_surface_dot_product_directory, \
    curv_def_number_of_bins, curv_def_seed, curv_def_dot_prod_neighbour_surface_radius_inner, \
                                            curv_def_dot_prod_neighbour_surface_radius_outer, \
                                            curv_def_dot_prod_cone_angle
from data.feature_extractors import AbstractFeatureExtractor
from data.tools.msms import get_surface_atoms
from numpy.random.mtrand import seed
from sklearn.preprocessing import normalize

__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'

__all__ = [
    "NeighbourhoodSurfaceDotProductExtractor",
]


class NeighbourhoodSurfaceDotProductExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        directory = os.path.join(self.features_directory, neighbourhood_surface_dot_product_directory, self.get_directory(), "")
        if not os.path.exists(directory):
            os.makedirs(directory)
        dot_prod_filen = directory + protein_name + ".npy"
        results_filen = directory + protein_name + "_results.pkl"
        return dot_prod_filen, results_filen
    
    def get_directory(self):
        return "{0}-{1}-{2}".format(self.surface_neighbourhood_radius_inner, self.surface_neighbourhood_radius_outer, self.number_of_bins)
    
    def load(self, protein_name):
        filename, _ = self._get_file_name(protein_name)
        feature = np.load(filename)
        return normalize(feature)

    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(NeighbourhoodSurfaceDotProductExtractor, self).__init__(database_directory, protein_name, protein)
        if 'residue_neighbourhood_radius_inner' not in kwargs:
            kwargs['surface_neighbourhood_radius_inner'] = curv_def_dot_prod_neighbour_surface_radius_inner
        if 'residue_neighbourhood_radius_outer' not in kwargs:
            kwargs['surface_neighbourhood_radius_outer'] = curv_def_dot_prod_neighbour_surface_radius_outer
        if 'cone_angle' not in kwargs:
            kwargs['cone_angle'] = curv_def_dot_prod_cone_angle
        if 'number_of_bins' not in kwargs:
            kwargs['number_of_bins'] = curv_def_number_of_bins
        if 'seed' not in kwargs:
            kwargs['seed'] = curv_def_seed
        self.surface_neighbourhood_radius_inner = kwargs['surface_neighbourhood_radius_inner']
        self.surface_neighbourhood_radius_outer = kwargs['surface_neighbourhood_radius_outer']
        self.cone_angle = kwargs['cone_angle']
        self.number_of_bins = kwargs['number_of_bins']
        self.protein = protein
        self.seed = kwargs['seed']

    def extract(self):
        neighbourhood_res_dot_product_file, results_filen = self._get_file_name(self.protein.name)
        if not os.path.exists(neighbourhood_res_dot_product_file):
            seed(self.seed)
            logger.info("Computing neighborhood surface dot products for {0}".format(self.protein.name))
            # get surface mesh of protein (yes, the name is misleading)
            surface, normals = get_surface_atoms(self.protein.file_name)

            # for each residue...
            results = [None] * len(self.protein.residues)
            dp_distributions = np.zeros((len(self.protein.residues), self.number_of_bins))
            neighbourhood_array = np.zeros((len(self.protein.residues)))
            for i, query_residue in enumerate(self.protein.residues):
                query_residue_ov = self.get_ave_normal_vector(query_residue.center, surface, normals, self.surface_neighbourhood_radius_inner)
                #query_residue_ov = self.get_surface_vector(query_residue.center, surface)
                result = {}
                dp_tuples = []
                result["residue"] = {"loc":query_residue.center,"vector":query_residue_ov}
                # if this residue has no orientation vector then skip it. 
                distances = np.linalg.norm(surface - query_residue.center, None, 1)
                n_hood_indices = distances < self.surface_neighbourhood_radius_outer
                dot_products = []
                if np.linalg.norm(query_residue_ov)!=0:
                    # get surface points in neighbourhood
                    for surface_point, surface_norm in zip(surface[n_hood_indices], normals[n_hood_indices]):
#                        # exclude surface points which are behind the residue (not sure how the math works out in that case)
#                        if self.is_behind(query_residue.center, query_residue_ov, surface_point):
                        # include only surface points which fall within a a cone projected from the residue vector
                        if not self.is_in_cone(query_residue.center, query_residue_ov, surface_point, self.cone_angle):
                            continue
                        dp = self.get_dot_product(query_residue.center, surface_point, query_residue_ov, surface_norm)
                        dot_products.append(dp)
                        dp_tuples.append((surface_point, surface_norm, dp))
                    dot_products = np.array(dot_products)

                # generate a distribution of dot products
                bins, dp_distributions[i,:] = self.get_distribution(dot_products)
                result["distribution"] = dp_distributions[i,:]
                result["surface_points"] = dp_tuples
                # add result to the results list
                results[i] = result
            # save output files
            pickle.dump((self.surface_neighbourhood_radius_outer, bins, results), open(results_filen,'wb'))
            np.save(neighbourhood_res_dot_product_file, dp_distributions)

    def get_distribution(self, data):
        dp_distribution = np.zeros((self.number_of_bins,))
        # create bins
        bins = np.linspace(-1, 1, self.number_of_bins)
        if len(data)==0:
            return bins, dp_distribution
        if max(data) == 0 and min(data) == 0:
            return bins, dp_distribution
        data = [k-2 if k == 0 else k for k in data]
        # determine the bin of each dot product
        indices = np.digitize(data, bins)
        if len(indices)!=0:
            # NANs are given bin number one greater than the greatest index. If they exist, max(indices) will be out of range
            max_index = min(self.number_of_bins, max(indices))
            # generate dist by counting number of elements in a certain bin
            dp_distribution[:max_index] = np.bincount(indices)[1:max_index+1] / float(len(indices))
        return bins, dp_distribution

    def get_dot_product(self, query_residue_center, neighbour_residue_center, query_res_ov, neighbour_res_ov, mode="cos"):
        # get dot product of surface vectors
        if mode=="cos": 
            # if want just cos(angle)
            raw_dot_product = np.dot(query_res_ov, neighbour_res_ov) / (np.linalg.norm(query_res_ov) * np.linalg.norm(neighbour_res_ov)) # actually it's cos(angle) between vectors
        elif mode=="full":
            # full dot product
            raw_dot_product = np.dot(query_res_ov, neighbour_res_ov)
        elif mode=="sign":
            raw_dot_product = 1
            
        # determine "sense" of dot product. I.e are surface vectors pointing towards or away from each other?
        neighbour_displacement = neighbour_residue_center - query_residue_center
        perp_neighbour_displacement = neighbour_displacement - query_res_ov * np.dot(query_res_ov, neighbour_displacement)
        sense = (-1) * np.sign(np.dot(neighbour_res_ov, perp_neighbour_displacement))
        dot_product = sense * raw_dot_product
        return dot_product

    def is_behind(self, center_pos, center_vec, neighbour_pos):
        neighbour_displacement = neighbour_pos - center_pos
        return True if np.sign(np.dot(center_vec, neighbour_displacement)) < 0 else False

    def is_in_cone(self, center_pos, center_vec, neighbour_pos, cone_angle):
        neighbour_displacement = neighbour_pos - center_pos
        cos_angle = np.dot(center_vec, neighbour_displacement)/(np.linalg.norm(center_vec) * np.linalg.norm(neighbour_displacement))
        return True if cos_angle > np.cos(cone_angle) else False

    # return vector from center to nearest point on surface
    def get_surface_vector(self, center, surface):
        # find distances b/t all surface points and the center
        distances = np.linalg.norm(surface - center, None, 1)
        # TODO should maybe average over the k closest surfaces in case they are all similar (like a sphere around the residue)?
        surface_point = surface[np.argmin(distances)]
        surface_vector = surface_point - center
        return surface_vector

    # return vector which is the average of the surface normals which are in a neighbourhood of the residue
    # if there are no surface points in the neighbourhood returns (0,0,0)
    def get_ave_normal_vector(self, center, surface, normals, neighbourhood_radius):
        # find distances b/t all surface points and the center
        distances = np.linalg.norm(surface - center, None, 1)
        n_hood_indices = distances < neighbourhood_radius
        vec = np.zeros(3)
        if np.sum(n_hood_indices)!=0:
            vec = np.mean(normals[n_hood_indices,:], axis=0)
        return vec
