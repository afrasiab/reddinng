import os
from abc import ABCMeta, abstractmethod

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractFeatureExtractor(AbstractLogger):
    __metaclass__ = ABCMeta
    _protein = None

    def __init__(self, configuration):
        super(AbstractFeatureExtractor, self).__init__()
        self._configuration = configuration
        self._database_directory = os.path.join(self._configuration['dataset_directory'])
        features = self._configuration["reddinng"]["features_directory"]
        self._features_directory = os.path.join(self._database_directory, features)
        self._directory = os.path.join(self._features_directory, self._get_directory())
        try:
            os.mkdir(self._features_directory)
            os.makedirs(self._directory)
        except OSError:
            pass

    @abstractmethod
    def _get_directory(self):
        pass

    @abstractmethod
    def extract(self, protein, save=True):
        pass

    @abstractmethod
    def _get_file_name(self, protein_name):
        pass
