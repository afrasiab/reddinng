import numpy as np

from src.features.feature_extractors.profile_extractor import ProfileExtractor


class WPSFMExtractor(ProfileExtractor):

    def _get_file_name(self, protein_name):
        return super(WPSFMExtractor, self)._get_file_name(protein_name)

    def extract(self, protein, save=True):
        super(WPSFMExtractor, self).extract(protein, save=save)
        _, wpsfm_file = self._get_file_name(protein.name)
        wpsfm_array = np.load(wpsfm_file)
        mid = int(wpsfm_array.shape[1] / 2)
        hw = (self.window_size - 1) / 2
        wpsfm_array = wpsfm_array[:, (mid - 10 - hw * 20):(mid + 10 + hw * 20)]
        return wpsfm_array

