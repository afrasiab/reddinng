import os
import logging
import numpy as np

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import residue_full_id_directory
from data.feature_extractors import AbstractFeatureExtractor

__author__ = 'basir'

__all__ = [
    "ResidueFullIDExtractor",
]


class ResidueFullIDExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        return os.path.join(self.features_directory, residue_full_id_directory, protein_name + ".npy")

    def extract(self):
        id_file_name = self._get_file_name(self._protein.name)
        if not os.path.exists(id_file_name):
            logger.info("Computing residue IDs for {0}".format(self._protein.name))
            ids = []
            for residue in self._protein.biopython_residues:
                chain_id = residue.parent.id
                residue_id = residue.id
                full_id = "{0}-{1}-{2}".format(self._protein.name, chain_id, residue_id[1])
                has_insertion_code = residue_id[2].strip() != ''
                full_id = full_id + "-{}".format(residue_id[2]) if has_insertion_code else full_id
                ids.append(full_id)
            np.save(id_file_name, np.array(ids))
