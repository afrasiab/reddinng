import os

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class SequenceExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(SequenceExtractor, self).__init__(configuration)

    def _get_directory(self):
        return self._configuration["reddinng"]["sequence_directory"]

    def _get_file_name(self, protein_name):
        fasta_file = os.path.join(self._directory, protein_name + ".fasta")
        return fasta_file

    def extract(self, protein, save=True):
        fasta_file = self._get_file_name(protein.name)
        if not os.path.exists(fasta_file) and save:
            self._logger.info("Computing the sequence for {}".format(protein.name))
            sequence = protein.sequence
            f = open(fasta_file, "w+")
            f.write(">{0}\n".format(protein.name))
            f.write(sequence + "\n")
            f.close()
        return fasta_file
