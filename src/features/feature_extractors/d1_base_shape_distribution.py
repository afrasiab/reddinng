from numpy.random.mtrand import choice
import numpy as np
import logging
from scipy.spatial.distance import cdist

from configuration import sd_def_radius
from data.feature_extractors import BaseShapeDistributionExtractor
from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D1BaseShapeDistributionExtractor",
]


class D1BaseShapeDistributionExtractor(BaseShapeDistributionExtractor):
    def _get_file_name(self, protein_name):
        pass

    def extract(self):
        pass

    def _compute_distribution(self, nearby_res, center):
        atoms = []
        for r in nearby_res:
            atoms.extend(r.child_list)
        n = len(atoms)
        indices = range(n)
        if self.number_of_samples != -1 and self.number_of_samples < n:
            indices = choice(n, self.number_of_samples)

        coordinates = np.zeros((len(indices), 3))
        counter = 0
        for index in indices:
            coordinates[counter, :] = atoms[index].get_coord()
            counter += 1

        center = np.copy(center).reshape((1, center.size))
        dist = cdist(coordinates, center).reshape((coordinates.shape[0]))
        bins = np.linspace(0, sd_def_radius * 2, self.number_of_bins + 1)
        indices = np.digitize(dist, bins)
        distribution = np.bincount(indices, minlength=self.number_of_bins)
        distribution = distribution[:self.number_of_bins]
        distribution /= np.sum(distribution)
        return distribution

