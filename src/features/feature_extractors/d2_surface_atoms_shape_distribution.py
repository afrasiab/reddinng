import logging

import numpy as np
import os
from configuration import rNH, d2_surface_atoms_directory
from data.feature_extractors import D2BaseShapeDistributionExtractor
from data.tools.msms import get_surface_atoms
from numpy.random.mtrand import seed
from scipy.spatial.distance import cdist

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D2SurfaceAtomsShapeDistributionExtractor",
]


class D2SurfaceAtomsShapeDistributionExtractor(D2BaseShapeDistributionExtractor):

    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(D2SurfaceAtomsShapeDistributionExtractor, self).__init__(protein, database_directory)
        if 'rNH' not in kwargs:
            kwargs['rNH'] = rNH
        self.rNH = kwargs['rNH']

    def _get_file_name(self, protein_name):
        directory = os.path.join(self.features_directory, d2_surface_atoms_directory,
                                 self._get_directory() + "-{0}/".format(self.rNH))
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            logger.info("Computing D2 surface atom shape distribution for {0}".format(self._protein.name))
            seed(self.seed)
            surface, normals = get_surface_atoms(self._protein.file_name)
            distributions = np.zeros((len(self._protein.residues), 2*(self.number_of_bins + 1)))

            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                distributions[i, :] = self.get_distributions(residue.center, surface, normals)
            np.save(shape_dist_file, distributions)

    def get_distributions(self, center, surface, normals):
        distances = cdist(surface, center.reshape(1, center.size))
        indices = np.where(distances < self.rNH)
        selected_normals = normals[indices[0], :]
        selected_coordinates = surface[indices[0], :]
        n = selected_coordinates.shape[0]
        dist = cdist(selected_coordinates, selected_coordinates).reshape((n*n,))
        dist /= np.max(dist)
        bins = np.linspace(0, 1, self.number_of_bins)
        indices = np.digitize(dist, bins)

        dist_distribution = np.bincount(indices)
        dist_distribution = self._normalize(dist_distribution)

        dist = selected_normals.dot(selected_normals.T).reshape((n*n,))
        dist /= np.max(dist)
        bins = np.linspace(0, 1, self.number_of_bins)
        indices = np.digitize(dist, bins)

        norm_distribution = np.bincount(indices)
        norm_distribution = self._normalize(norm_distribution)
        distribution = np.hstack((dist_distribution, norm_distribution))
        return distribution
