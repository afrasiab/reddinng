import os
import logging
import numpy as np
from numpy.random.mtrand import seed

from Bio.PDB import NeighborSearch

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import sd_def_levels, shape_dist_directory, d1_directory
from data.feature_extractors import D1BaseShapeDistributionExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D1MultiLevelShapeDistributionExtractor",
]


class D1MultiLevelShapeDistributionExtractor(D1BaseShapeDistributionExtractor):
    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(D1MultiLevelShapeDistributionExtractor, self).__init__(database_directory, protein_name, protein, **kwargs)
        if 'number_of_levels' not in kwargs:
            kwargs['number_of_levels'] = sd_def_levels
        self.number_of_levels = kwargs['number_of_levels']

    def _get_file_name(self, protein_name):
        return os.path.join(self.features_directory, shape_dist_directory, d1_directory, protein_name + ".npy")

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            seed(self.seed)
            logger.info("Computing Multi-level D1 shape distribution for {0}".format(self._protein.name))
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_levels * self.number_of_bins))
            k = self.number_of_bins
            previous_residues = set()
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                for j in range(self.number_of_levels):
                    nearby_residues = neighbour_search.search(residue.center, (self.radius * 3) / (5 - j), "R")
                    new_nearby_residues = list(set(nearby_residues) - set(previous_residues))
                    distributions[i, j * k:(j + 1) * k] = self._compute_distribution(nearby_residues, residue.center)
                    previous_residues.update(nearby_residues)
            np.save(shape_dist_file, distributions)
