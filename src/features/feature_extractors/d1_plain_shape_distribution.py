import os
import logging
import numpy as np
from numpy.random.mtrand import seed

from Bio.PDB import NeighborSearch

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import d1_directory
from data.feature_extractors import D1BaseShapeDistributionExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D1PlainShapeDistributionExtractor",
]


class D1PlainShapeDistributionExtractor(D1BaseShapeDistributionExtractor):

    def _get_file_name(self, protein_name):
        directory = os.path.join(self.features_directory, d1_directory, self._get_directory(), "")
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            seed(self.seed)
            logger.info("Computing plain D1 shape distribution for {0}".format(self._protein.name))
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins))
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                nearby_residues = neighbour_search.search(residue.center, self.radius, "R")
                distributions[i, :] = self._compute_distribution(nearby_residues, residue.center)
            np.save(shape_dist_file, distributions)


