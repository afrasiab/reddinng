import logging
import numpy as np
import os
from scipy.spatial.distance import cdist
from utils import IndentLogger
from configuration import interaction_thr, example_id_separator, contact_examples_directory
from data.feature_extractors import AbstractFeatureExtractor, ResidueFullIDExtractor, TrueInterfaceExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

logger = IndentLogger(logging.getLogger(''), {})


class ContactExampleExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, complex_name):
        name = os.path.join(self.features_directory, contact_examples_directory, complex_name)
        return name + "_pos_l.npy", name + "_neg_l.npy", name + "_pos_dist_l.npy", name + "_neg_dist_l.npy", \
               name + "_pos_r.npy", name + "_neg_r.npy", name + "_pos_dist_r.npy", name + "_neg_dist_r.npy"

    def __init__(self, database_directory, complex_code=None, complex_object_model=None, **kwargs):
        super(ContactExampleExtractor, self).__init__(database_directory, complex_object_model.complex_code, None,
                                                      **kwargs)
        self.complex_code = complex_object_model.name if complex_code is None else complex_code
        self.complex = complex_object_model

    def set_protein(self, complex_object_model):
        self.complex_code = complex_object_model.name
        self.complex = complex_object_model

    def load(self, complex_name):
        p_f_l, n_f_l, p_d_f_l, n_d_f_l, p_f_r, n_f_r, p_d_f_r, n_d_f_r = self._get_file_name(complex_name)
        p_l = np.load(p_f_l)
        n_l = np.load(n_f_l)
        p_d_l = np.load(p_d_f_l)
        n_d_l = np.load(n_d_f_l)
        p_r = np.load(p_f_r)
        n_r = np.load(n_f_r)
        p_d_r = np.load(p_d_f_r)
        n_d_r = np.load(n_d_f_r)
        return p_l, n_l, p_d_l, n_d_l, p_r, n_r, p_d_r, n_d_r

    def extract(self):
        code = self.complex.complex_code
        files = self._get_file_name(code)
        [p_f_l, n_f_l, p_d_f_l, n_d_f_l, p_f_r, n_f_r, p_d_f_r, n_d_f_r] = files
        all_exist = True
        for f in files:
            if not os.path.exists(f):
                all_exist = False
                break
        if not all_exist:
            logger.info("Extracting interacting/non-interacting residues for {0}".format(self.complex.complex_code))
            ligand_u = self.complex.unbound_formation.ligand
            receptor_u = self.complex.unbound_formation.receptor
            unbound_ligand_residues = ligand_u.residues
            unbound_receptor_residues = receptor_u.residues
            pos_l = []
            neg_l = []
            pos_d_l = []
            neg_d_l = []
            pos_r = []
            neg_r = []
            pos_d_r = []
            neg_d_r = []
            l_full_ids = ResidueFullIDExtractor(self.database_directory).load(ligand_u.name)
            r_full_ids = ResidueFullIDExtractor(self.database_directory).load(receptor_u.name)
            l = (unbound_ligand_residues, l_full_ids, pos_l, neg_l, pos_d_l, neg_d_l)
            r = (unbound_receptor_residues, r_full_ids, pos_r, neg_r, pos_d_r, neg_d_r)
            for residues, ids, p, n, p_d, n_d in [l, r]:
                for i, b_r in enumerate(residues):
                    for j, b_l in enumerate(residues):
                        if i == j:
                            continue
                        e_id = "{}{}{}".format(ids[i], example_id_separator, ids[j])
                        min_distance = cdist(b_l.get_coordinates(), b_r.get_coordinates()).min()
                        if min_distance < interaction_thr:
                            p_d.append(min_distance)
                            p.append((e_id, '+1'))
                        else:
                            n_d.append(min_distance)
                            n.append((e_id, '-1'))
            np.save(p_f_l, np.array(pos_l))
            np.save(n_f_l, np.array(neg_l))
            np.save(p_d_f_l, np.array(pos_d_l))
            np.save(n_d_f_l, np.array(neg_d_l))
            np.save(p_f_r, np.array(pos_r))
            np.save(n_f_r, np.array(neg_r))
            np.save(p_d_f_r, np.array(pos_d_r))
            np.save(n_d_f_r, np.array(neg_d_r))

