import os
import logging
import numpy as np
from numpy.random.mtrand import seed

from Bio.PDB import NeighborSearch

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import d1_surface_directory, sd_def_rASA_thresh
from data.feature_extractors import D1BaseShapeDistributionExtractor, StrideSecondaryStructureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D1SurfaceShapeDistributionExtractor",
]


class D1SurfaceShapeDistributionExtractor(D1BaseShapeDistributionExtractor):

    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(D1SurfaceShapeDistributionExtractor, self).__init__(database_directory, protein_name, protein, **kwargs)
        if 'rASA' not in kwargs:
            kwargs['rASA'] = sd_def_rASA_thresh
        self.rASA_threshold = kwargs['rASA']

    def _get_file_name(self, protein_name):
        directory = os.path.join(d1_surface_directory, self._get_directory() + "-{0}/".format(self.rASA_threshold))
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            seed(self.seed)
            logger.info("Computing D1 surface residue shape distribution for {0}".format(self._protein.name))
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins))
            rsa = StrideSecondaryStructureExtractor(None).load(self._protein.name)

            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                nearby_residues = [self._protein.biopython_residues[i]]
                temp_nearby_residues = neighbour_search.search(residue.center, self.radius, "R")
                for nearby_residue in temp_nearby_residues:
                    if nearby_residue not in self._protein.biopython_residues:
                        continue
                    residues_index = self._protein.biopython_residues.index(nearby_residue)
                    if rsa[residues_index] >= self.rASA_threshold:
                        nearby_residues.append(nearby_residue)
                distributions[i, :] = self._compute_distribution(nearby_residues, residue.center)
            np.save(shape_dist_file, distributions)
