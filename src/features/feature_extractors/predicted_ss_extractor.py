import numpy as np
import os

from src.data.tools.spinex import SPINEX
from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor
from src.features.feature_extractors.profile_extractor import ProfileExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class PredictedSSExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(PredictedSSExtractor, self).__init__(configuration)
        self.pred_ss_file = None
        self.pred_ss_directory = self._configuration["reddinng"]["pred_ss_directory"]
        self.sequence_directory = self._configuration["reddinng"]["sequence_directory"]
        self.profile_directory = self._configuration["reddinng"]["profile_directory"]

    def _get_file_name(self, protein_name):
        ss_file = os.path.join(self._features_directory, self.pred_ss_directory, protein_name + "_ss.npy")
        return ss_file

    def load(self, protein_name):
        _, ss_file, _ = self._get_file_name(protein_name)
        pred_ss = np.load(ss_file)
        return pred_ss

    def extract(self, protein, save=True):
        pred_ss_file = self._get_file_name(self._protein.name)
        if not os.path.exists(pred_ss_file):
            self._logger.info("Computing predicted ss for {0}".format(self._protein.name))
            pssm_file = os.path.join(self._features_directory, self.profile_directory, protein.name + ".mat")
            if not os.path.exists(pssm_file):
                ProfileExtractor(self._configuration).extract(protein)
            (asa, rasa, _, _, _) = SPINEX(self._configuration).give_ss_predictions(pssm_file)
            rasa_array = rasa.reshape((rasa.shape[0], 1))

            if save:
                np.save(pred_ss_file, rasa_array)
            return rasa_array
        else:
            np.load(rasa_array)

