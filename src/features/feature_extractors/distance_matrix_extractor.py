import numpy as np
import os

from scipy.spatial.distance import cdist
from Bio.PDB import is_aa

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'


class DistanceMatrixExtractor(AbstractFeatureExtractor):
    def extract(self, protein, save):
        pass

    def _get_file_name(self, protein_name):
        pass

    def __init__(self, configuration):
        super(DistanceMatrixExtractor, self).__init__(configuration)
        myclass = type(self).__name__
        if "n_bins" not in self._configuration:
            self.n_bins = 10
        if "distance_matrix" in self._configuration:
            if "preprocess" in self._configuration["distance_matrix"]:
                self.preprocess = self._configuration["distance_matrix"]["preprocess"]
        elif myclass in self._configuration:
            if "preprocess" in self._configuration[myclass]:
                self.preprocess = self._configuration[myclass]["preprocess"]
        else:
            self.preprocess = None

    def _get_directory(self):
        return self._configuration["reddinng"]["distance_matrix_directory"]

    def _load(self, file_name):
        data = np.load(file_name)
        if self.preprocess is None:
            return data
        elif self.preprocess == "normalize":
            minn = np.min(data)
            maxx = np.max(data)
            data = (data - minn) / (maxx - minn)
            transform_info = {"type": "normalize", "min": minn, "max": maxx}
        elif self.preprocess == "standardize":
            meann = np.mean(data)
            stdd = np.std(data)
            data = (data - meann) / stdd
            transform_info = {"type": "standardize", "mean": meann, "std": stdd}
        elif "gaussian_kernel" in self.preprocess:
            std = self.preprocess[1]
            data = np.exp(-data ** 2 / float(std) ** 2)
            transform_info = {'type': "gaussian_kernel", "std": std}
        elif "binary_upper_bound" in self.preprocess:
            threshold = self.preprocess[1]
            data = np.where(data <= threshold, np.ones(data.shape), np.zeros(data.shape))
            transform_info = {'type': "binary_upper_bound", "threshold": threshold}
        elif "binary_lower_bound" in self.preprocess:
            threshold = self.preprocess[1]
            data = np.where(data >= threshold, np.ones(data.shape), np.zeros(data.shape))
            transform_info = {'type': "binary_lower_bound", "threshold": threshold}
        else:
            raise ValueError("unrecognized preprocessing: {}".format(self.preprocess))
        return data, transform_info

    def load(self, protein_name, protein=None):
        file_name = self._get_file_name(protein_name)
        return self._load(file_name)


class DistanceMatrixMaxExtractor(DistanceMatrixExtractor):
    def extract(self, protein, save=True):
        file_name = self._get_file_name(protein.name)
        if not os.path.exists(file_name):
            self._logger.info("Computing maximum atomic distances for {0}".format(protein.name))
            max_dist = np.zeros((len(protein.residues), len(protein.residues)))
            for i in range(len(protein.residues)):
                for j in range(i):
                    atom_dists = cdist(protein.residues[i].get_coordinates(),
                                       protein.residues[j].get_coordinates())
                    max_dist[i][j] = max_dist[j][i] = atom_dists.max()
            if save:
                np.save(file_name, max_dist)
            return max_dist
        else:
            return np.load(file_name)

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, "{}_max.npy".format(protein_name))


class DistanceMatrixMinExtractor(DistanceMatrixExtractor):
    def extract(self, protein, save=True):
        file_name = self._get_file_name(protein.name)
        if not os.path.exists(file_name):
            self._logger.info("Computing minimum atomic distances for {0}".format(protein.name))
            min_dist = np.zeros((len(protein.residues), len(protein.residues)))
            for i in range(len(protein.residues)):
                for j in range(i):
                    atom_dists = cdist(protein.residues[i].get_coordinates(), protein.residues[j].get_coordinates())
                    min_dist[i][j] = min_dist[j][i] = atom_dists.min()
            if save:
                np.save(file_name, min_dist)
            return min_dist
        else:
            return np.load(file_name)

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, "{}_min.npy".format(protein_name))


class DistanceMatrixAveExtractor(DistanceMatrixExtractor):
    def extract(self, protein, save=True):
        file_name = self._get_file_name(protein.name)
        if not os.path.exists(file_name):
            self._logger.info("Computing average atomic distances for {0}".format(protein.name))
            ave_dist = np.zeros((len(protein.residues), len(protein.residues)))
            for i in range(len(protein.residues)):
                for j in range(i):
                    ave_dists = cdist(protein.residues[i].get_coordinates(), protein.residues[j].get_coordinates())
                    ave_dist[i][j] = ave_dist[j][i] = ave_dists.mean()
            if save:
                np.save(file_name, ave_dist)
            return ave_dist
        else:
            return np.load(file_name)

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, "{}_ave.npy".format(protein_name))


class DistanceMatrixStdExtractor(DistanceMatrixExtractor):
    def extract(self, protein, save=True):
        file_name = self._get_file_name(protein.name)
        if not os.path.exists(file_name):
            self._logger.info("Computing std atomic distances for {0}".format(protein.name))
            std_dist = np.zeros((len(protein.residues), len(protein.residues)))
            for i in range(len(protein.residues)):
                for j in range(i):
                    atom_dists = cdist(protein.residues[i].get_coordinates(), protein.residues[j].get_coordinates())
                    std_dist[i][j] = std_dist[j][i] = atom_dists.std()
            if save:
                np.save(file_name, std_dist)
            return std_dist
        else:
            return np.load(file_name)

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, "{}_std.npy".format(protein_name))


class DistanceMatrixCAExtractor(DistanceMatrixExtractor):
    def extract(self, protein, save=True):
        file_name = self._get_file_name(protein.name)
        if not os.path.exists(file_name):
            self._logger.info("Computing alpha carbon distances for {0}".format(protein.name))
            ca_dist = np.zeros((len(protein.residues), len(protein.residues)))
            alpha_carbons = [get_alpha_carbon(res) for res in protein.biopython_residues]
            for i in range(len(protein.residues)):
                for j in range(i):
                    if alpha_carbons[i] is not None and alpha_carbons[j] is not None:
                        ca_dist[i][j] = ca_dist[j][i] = np.linalg.norm(alpha_carbons[i] - alpha_carbons[j])
                    else:
                        ca_dist[i][j] = ca_dist[j][i] = np.nan
            if save:
                np.save(file_name, ca_dist)
            return ca_dist
        else:
            return np.load(file_name)


    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, "{}_ca.npy".format(protein_name))


class DistanceMatrixSeqExtractor(DistanceMatrixExtractor):
    def extract(self, protein, save=True):
        file_name = self._get_file_name(protein.name)
        if not os.path.exists(file_name):
            self._logger.info("Computing sequence distances for {0}".format(protein.name))
            seq_dist = np.zeros((len(protein.residues), len(protein.residues)))
            for i in range(len(protein.residues)):
                for j in range(i):
                    distance = abs(protein.residues[i].residue.id[1] - protein.residues[j].residue.id[1])
                    seq_dist[i][j] = seq_dist[j][i] = 1 if distance == 1 else 0
            if save:
                np.save(file_name, seq_dist)
            return seq_dist
        else:
            return np.load(file_name)

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, "{}_seq.npy".format(protein_name))


def get_alpha_carbon(residue):
    if is_aa(residue) and residue.has_id('CA'):
        return residue['CA'].get_coord()
    return None
