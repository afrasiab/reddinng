import os
import numpy as np

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor
from src.features.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AdjacencyMatrixExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(AdjacencyMatrixExtractor, self).__init__(configuration)

    def _get_directory(self):
        return self._configuration["reddinng"]["adjacency_matrix_directory"]

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        if not os.path.exists(directory):
            os.makedirs(directory)
        return os.path.join(directory, protein_name + ".npy")

    def extract(self, protein, save=True):
        adjacency_matrix_file = self._get_file_name(protein.name)
        if not os.path.exists(adjacency_matrix_file):
            self._logger.info("Computing Adjacency Matrix for {0}".format(protein.name))
            n = len(protein.residues)
            adjacency_matrix = np.zeros((n, n))
            extractor = ResidueNeighbourhoodExtractor(self._configuration)
            adjacency_list = extractor.extract(protein).astype(int)
            for i, neighbours in enumerate(adjacency_list):
                if np.any(np.where(neighbours == -1)):
                    end = np.min(np.where(neighbours == -1))
                    adjacency_matrix[i, tuple(neighbours[:end])] = 1
                else:
                    adjacency_matrix[i, tuple(neighbours)] = 1
            if save:
                np.save(adjacency_matrix_file, adjacency_matrix)
            return adjacency_matrix
        return np.load(adjacency_matrix_file)
