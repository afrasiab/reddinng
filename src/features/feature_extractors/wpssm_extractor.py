import numpy as np
from sklearn.preprocessing import normalize

from src.features.feature_extractors.profile_extractor import ProfileExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class WPSSMExtractor(ProfileExtractor):

    def extract(self, protein, save=True):
        super(WPSSMExtractor, self).extract(protein, save=save)
        wpssm_file, _ = self._get_file_name(protein.name)
        wpssm_array = np.load(wpssm_file)
        mid = int(wpssm_array.shape[1] / 2)
        hw = (self.window_size - 1) / 2
        wpssm_array = wpssm_array[:, (mid - 10 - hw * 20):(mid + 10 + hw * 20)]
        return wpssm_array
