import os
import socket
from multiprocessing import cpu_count
import numpy as np
from sklearn.preprocessing import normalize

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor
from src.features.feature_extractors.sequence_extractor import SequenceExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ProfileExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        self.window_size = configuration["window_size"] if "window_size" in configuration else 11
        assert (self.window_size % 2 == 1)
        self._n_iterations = configuration["n_iterations"] if "n_iterations" in configuration else 3
        self._db = configuration["db"] if "db" in configuration else "uniref"

        super(ProfileExtractor, self).__init__(configuration)
        if "blast-database" in self._configuration:
            os.environ["BLASTDB"] = self._configuration["blast-database"].replace("<host>", socket.gethostname())
        self.psiblast_exe = self._configuration["reddinng"]["psiblast_executable"]

    def _get_directory(self):
        return os.path.join(self._configuration["reddinng"]["profile_directory"], self._db)

    def _get_file_name(self, protein_name):
        base_name = os.path.join(self._directory, protein_name)
        return base_name + "_wpssm.npy", base_name + "_wpsfm.npy"

    def extract(self, protein, save=True):
        wpssm_file, wpsfm_file = self._get_file_name(protein.name)
        if not os.path.exists(wpssm_file) or not os.path.exists(wpsfm_file):
            fasta_file = SequenceExtractor(self._configuration).extract(protein)
            self._logger.info("Computing profile features for {0}".format(protein.name))
            matrix_file = wpsfm_file[:-10]
            if not os.path.exists(matrix_file + ".mat"):
                command = "{4} " \
                          "-query {0} " \
                          "-db {1} " \
                          "-out {2}.psi.txt " \
                          "-num_iterations {3} " \
                          "-num_threads {5} " \
                          "-out_ascii_pssm {2}.mat" \
                    .format(fasta_file, self._db, matrix_file, self._n_iterations, self.psiblast_exe, cpu_count())
                error_code = os.system(command)
                if error_code != 0:
                    self._logger.error('Failed with error code {0}'.format(error_code))
                    raise ValueError('Failed with error code {0}'.format(error_code))
            pssm, psfm, info = ProfileExtractor.__parse_pssm_file(matrix_file + ".mat")
            wpssm = self.__get_wpsm(pssm)
            wpsfm = self.__get_wpsm(psfm)
            if save:
                np.save(wpssm_file, wpssm)
                np.save(wpsfm_file, wpsfm)
            os.remove(matrix_file + ".psi.txt")
            mid_s = int(wpssm.shape[1] / 2)
            mid_f = int(wpsfm.shape[1] / 2)
            hw = (self.window_size - 1) / 2
            wpssm = wpssm[:, (mid_s - 10 - hw * 20):(mid_s + 10 + hw * 20)]
            wpsfm = wpsfm[:, (mid_f - 10 - hw * 20):(mid_f + 10 + hw * 20)]
            return np.hstack((wpssm, wpsfm))

        else:
            wpssm = np.load(wpssm_file)
            wpsfm = np.load(wpsfm_file)
            mid_s = int(wpssm.shape[1] / 2)
            mid_f = int(wpsfm.shape[1] / 2)
            hw = (self.window_size - 1) / 2
            wpssm = wpssm[:, (mid_s - 10 - hw * 20):(mid_s + 10 + hw * 20)]
            wpsfm = wpsfm[:, (mid_f - 10 - hw * 20):(mid_f + 10 + hw * 20)]
            return np.hstack((wpssm, wpsfm))

    @staticmethod
    def __get_wpsm(psm, window_size=5):
        window_size = int(window_size)
        (sequence_length, dimension) = psm.shape
        padded_psm = np.vstack((np.zeros((window_size, dimension)), psm, np.zeros((window_size, dimension))))
        ws = 2 * window_size + 1
        wpsm = np.zeros((sequence_length, ws * dimension))
        for i in range(sequence_length):
            wpsm[i, :] = padded_psm[i:i + ws, :].flatten()
        return wpsm

    @staticmethod
    def __parse_pssm_file(file_name):
        pssm = []
        psfm = []
        info = []
        try:
            for line in open(file_name, 'r'):
                record = line.split()
                if len(record) and record[0].isdigit():  # the first character must be a position
                    pssm.append([float(i) for i in record[2:22]])
                    psfm.append([float(i) / 100 for i in record[22:42]])
                    if len(record) >= 42:
                        info.append(float(record[42]))
                    else:
                        info.append(-1)
            profiles = (np.array(pssm), np.array(psfm), np.array(info))
        except IOError as e:
            print e
            profiles = None
        return profiles
