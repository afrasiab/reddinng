import os

from configuration import interface_directory
from data.feature_extractors import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "TrueInterfaceExtractor",
]


# noinspection PyMethodMayBeStatic
class TrueInterfaceExtractor(AbstractFeatureExtractor):
    def __init__(self, database_directory, complex_code=None, complex_object_model=None, **kwargs):
        super(TrueInterfaceExtractor, self).__init__(database_directory, None)
        self.features_directory = None
        self.complex_code = complex_object_model.name if complex_code is None else complex_code
        self.complex = complex_object_model

    def extract(self):
        # Example extractor also extracts the true interface
        pass

    def _get_file_name(self, protein_name):
        return os.path.join(self.database_directory, interface_directory, protein_name + ".npy")

    def get_file_names(self, complex_name):
        base_name = os.path.join(self.database_directory, interface_directory, complex_name)
        return base_name + "_l_b.npy", base_name + "_r_b.npy", base_name + "_l_u.npy", base_name + "_r_u.npy"
