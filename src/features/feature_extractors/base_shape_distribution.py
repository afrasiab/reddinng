from enum import Enum
from abc import abstractmethod

from configuration import sd_def_number_of_bins, sd_def_radius, sd_def_seed, sd_def_number_of_samples
from . import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "BaseShapeDistributionExtractor",
]


class ResidueCategories(Enum):
    Polar = 0
    Charged = 1
    Hydrophobic = 2


categories = dict({
    'CYS': ResidueCategories.Polar,
    'GLN': ResidueCategories.Polar,
    'ASP': ResidueCategories.Charged,
    'SER': ResidueCategories.Polar,
    'VAL': ResidueCategories.Hydrophobic,
    'LYS': ResidueCategories.Charged,
    'PRO': ResidueCategories.Hydrophobic,
    'THR': ResidueCategories.Polar,
    'TRP': ResidueCategories.Polar,
    'PHE': ResidueCategories.Hydrophobic,
    'GLU': ResidueCategories.Charged,
    'HIS': ResidueCategories.Polar,
    'GLY': ResidueCategories.Hydrophobic,
    'ILE': ResidueCategories.Hydrophobic,
    'LEU': ResidueCategories.Hydrophobic,
    'ARG': ResidueCategories.Charged,
    'ALA': ResidueCategories.Hydrophobic,
    'ASN': ResidueCategories.Polar,
    'TYR': ResidueCategories.Polar,
    'MET': ResidueCategories.Polar,
})


class BaseShapeDistributionExtractor(AbstractFeatureExtractor):
    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(BaseShapeDistributionExtractor, self).__init__(database_directory, protein_name, protein, **kwargs)
        if 'number_of_bins' not in kwargs:
            kwargs['number_of_bins'] = sd_def_number_of_bins
        if 'radius' not in kwargs:
            kwargs['radius'] = sd_def_radius
        if 'seed' not in kwargs:
            kwargs['seed'] = sd_def_seed
        if 'number_of_samples' not in kwargs:
            kwargs['number_of_samples'] = sd_def_number_of_samples

        self.radius = kwargs['radius']
        self.number_of_bins = kwargs['number_of_bins']
        self.number_of_samples = kwargs['number_of_samples']
        self.seed = kwargs['seed']

    @abstractmethod
    def extract(self):
        pass

    def _get_directory(self):
        return "{0}-{1}-{2}-{3}".format(self.radius, self.number_of_bins, self.number_of_samples, self.seed)
