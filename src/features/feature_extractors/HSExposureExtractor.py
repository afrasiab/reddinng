import os
import numpy as np
from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor
from Bio.PDB import HSExposure

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class HSExposureExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(HSExposureExtractor, self).__init__(configuration)

    def _get_file_name(self, protein_name):
        return os.path.join(self._directory, "{}.npy".format(protein_name))

    def _get_directory(self):
        return self._configuration["reddinng"]["hse_directory"]

    def extract(self, protein, save):
        hse_file = self._get_file_name(protein.name)
        if not os.path.exists(hse_file):
            n = len(protein.residues)
            hse_array = np.zeros((n, 10))
            hse_ca = HSExposure.HSExposureCA(protein.structure[0])
            hse_cb = HSExposure.HSExposureCB(protein.structure[0])

