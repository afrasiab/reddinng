import os
import numpy as np

from Bio.PDB.Polypeptide import standard_aa_names
from Bio.PDB import is_aa, Vector
from sklearn import preprocessing

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor
from src.features.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class HalfSphereAminoAcidCompositionExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(HalfSphereAminoAcidCompositionExtractor, self).__init__(configuration)

    def _get_directory(self):
        return self._configuration["reddinng"]["hse_directory"]

    def _get_file_name(self, protein_name):
        return os.path.join(self._directory, protein_name + ".npy")

    def extract(self, protein, save=True):
        hse_file = self._get_file_name(protein.name)
        if not os.path.exists(hse_file):
            aa_table = dict(zip(standard_aa_names, range(len(standard_aa_names))))
            n_aa = len(standard_aa_names)
            self._logger.info("Computing half sphere amino acid composition for {0}".format(protein.name))
            neighbourhood = ResidueNeighbourhoodExtractor(self._configuration).extract(protein)
            n = len(protein.biopython_residues)
            up_direction_n = np.zeros(n)
            down_direction_n = np.zeros(n)
            up_direction_composition = np.zeros((n_aa, n))
            down_direction_composition = np.zeros((n_aa, n))
            for i, residue in enumerate(protein.residues):
                u = self._get_side_chain_vector(residue)
                if u is None:
                    up_direction_n[i] = np.nan
                    down_direction_n[i] = np.nan
                    up_direction_composition[:, i] = np.nan
                    down_direction_composition[:, i] = np.nan
                else:
                    residue_index = aa_table[residue.residue.get_resname()]
                    up_direction_composition[residue_index, i] += 1
                    down_direction_composition[residue_index, i] += 1
                    neighbours_indices = neighbourhood[i]
                    # print neighbours_indices
                    for neighbour_index in neighbours_indices:
                        if neighbour_index == -1:
                            break
                        neighbour_residue = protein.biopython_residues[int(neighbour_index)]
                        if is_aa(neighbour_residue) and neighbour_residue.has_id('CA'):
                            neighbour_vector = neighbour_residue['CA'].get_vector()
                            residue_index = aa_table[neighbour_residue.get_resname()]
                            if u[1].angle((neighbour_vector - u[0])) < np.pi / 2.0:
                                up_direction_n[i] += 1
                                up_direction_composition[residue_index, i] += 1
                            else:
                                down_direction_n[i] += 1
                                down_direction_composition[residue_index, i] += 1
            up_direction_composition = (up_direction_composition / (1.0 + up_direction_n)).T
            down_direction_composition = (down_direction_composition / (1.0 + down_direction_n)).T
            hse_array = np.hstack((up_direction_composition, down_direction_composition))
            if save:
                np.save(hse_file, hse_array)
            return hse_array
        else:
            return np.load(hse_file)

    def _get_side_chain_vector(self, residue):
        """
        Find the average of the unit vectors to different atoms in the side chain
        from the c-alpha atom.
        Returns (C-alpha coordinate vector, side chain unit vector) for residue r
        """
        u = None
        if is_aa(residue.residue) and residue.residue.has_id('CA'):
            ca = residue.residue['CA'].get_coord()
            side_chain_atoms = residue.get_side_chain_coordinates()
            if side_chain_atoms is not None:
                differences = preprocessing.normalize(side_chain_atoms - ca, norm='l2')
                u = (Vector(ca), Vector(differences.mean(axis=0)))
            else:
                return None
        elif is_aa(residue.residue):
            self._logger.info("{} is missing CA atom".format(residue))
        else:
            self._logger.info("{} is not an amino acid".format(residue))
        return u