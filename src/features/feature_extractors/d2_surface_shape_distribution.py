import os
import logging
import numpy as np
from numpy.random.mtrand import seed

from Bio.PDB import NeighborSearch

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import sd_def_rASA_thresh, d2_surface_directory
from data.feature_extractors import D2BaseShapeDistributionExtractor, StrideSecondaryStructureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D2SurfaceShapeDistributionExtractor",
]


class D2SurfaceShapeDistributionExtractor(D2BaseShapeDistributionExtractor):
    def _get_file_name(self, protein_name):
        return os.path.join(self.features_directory, d2_surface_directory, protein_name + ".npy")

    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(D2SurfaceShapeDistributionExtractor, self).__init__(database_directory, protein_name, protein, **kwargs)
        if 'rASA' not in kwargs:
            kwargs['rASA'] = sd_def_rASA_thresh
        self.rASA_threshold = kwargs['rASA']

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            seed(self.seed)
            logger.info("Computing D2 surface residue shape distribution for {0}".format(self._protein.name))
            StrideSecondaryStructureExtractor(self._protein).extract()
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins))
            rsa = StrideSecondaryStructureExtractor(None).load(self._protein.name)
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                nearby_residues = [self._protein.biopython_residues[i]]
                temp_nearby_residues = neighbour_search.search(residue.center, self.radius, "R")
                for nearby_residue in temp_nearby_residues:
                    if nearby_residue not in self._protein.biopython_residues:
                        continue
                    residues_index = self._protein.biopython_residues.index(nearby_residue)
                    if rsa[residues_index] >= self.rASA_threshold:
                        nearby_residues.append(nearby_residue)
                distributions[i, :] = self._compute_distribution(nearby_residues)
            np.save(shape_dist_file, distributions)
