import os
import numpy as np
from Bio.PDB import is_aa

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'Basir Shariat (basir@rams.colostate.edu)'


class AngleMatrixExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(AngleMatrixExtractor, self).__init__(configuration)
        if "angle_matrix" in configuration:
            if "preprocess" in configuration["angle_matrix"]:
                self.preprocess = configuration["angle_matrix"]["preprocess"]

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._features_directory, angle_matrix_directory, "")
        if not os.path.exists(directory):
            os.makedirs(directory)
        filens = []
        for sufx in ["ccao", "ccao_cos", "canc", "canc_cos", "hsaa", "hsaa_cos", "surf", "surf_cos"]:
            filens.append(directory + protein_name + "_" + sufx + ".npy")
        return filens
    
    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        if self.preprocess == "divide_pi":
            return np.load(filen)/np.pi
        elif self.preprocess == "divide_2pi":
            return np.load(filen)/np.pi/2.
        return np.load(filen)

    def get_vectors(self):
        pass

    def extract(self, save):
        filen, filen_cos = self._get_file_name(self._protein.name)
        if not os.path.exists(filen):
            logger.info("Computing angles for {0}".format(self._protein.name))
            # for each residue...
            angles_cos = np.zeros((len(self._protein.residues), len(self._protein.residues), 1))
            vectors = self.get_vectors()
            for i in range(len(self._protein.residues)):
                for j in range(i, len(self._protein.residues)):
                    if vectors[i] is not None and vectors[j] is not None:
                        angles_cos[i][j][0] = angles_cos[j][i][0] = np.dot(vectors[i], vectors[j])
                    else:
                        angles_cos[i][j][0] = angles_cos[j][i][0] = np.nan
            angles_cos = np.clip(angles_cos, -1, 1)
            # save output files
            np.save(filen, np.arccos(angles_cos))
            np.save(filen_cos, angles_cos)


class AngleMatrixCCAOExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixCCAOExtractor, self)._get_file_name(protein_name)[0:2]

    def get_vectors(self):
            return np.array([get_ccao_plane_normal(res) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[0])


class AngleMatrixCCAOCosExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixCCAOCosExtractor, self)._get_file_name(protein_name)[0:2]

    def get_vectors(self):
        return np.array([get_ccao_plane_normal(res) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[1])


class AngleMatrixCANCExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixCANCExtractor, self)._get_file_name(protein_name)[2:4]

    def get_vectors(self):
        return np.array([get_canc_plane_normal(res) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[0])


class AngleMatrixCANCCosExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixCANCCosExtractor, self)._get_file_name(protein_name)[2:4]

    def get_vectors(self):
        return np.array([get_canc_plane_normal(res) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[1])


class AngleMatrixHSAAExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixHSAAExtractor, self)._get_file_name(protein_name)[4:6]

    def get_vectors(self):
        return np.array([get_hsaa_vector(res) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[0])


class AngleMatrixHSAACosExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixHSAACosExtractor, self)._get_file_name(protein_name)[4:6]

    def get_vectors(self):
        return np.array([get_hsaa_vector(res) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[1])


class AngleMatrixSURFExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixSURFExtractor, self)._get_file_name(protein_name)[6:8]

    def get_vectors(self):
        surface, _ = get_surface_atoms(self._protein.file_name)
        return np.array([get_vector_to_surface(res, surface) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[0])


class AngleMatrixSURFCosExtractor(AngleMatrixExtractor):
    def _get_file_name(self, protein_name):
        return super(AngleMatrixSURFCosExtractor, self)._get_file_name(protein_name)[6:8]

    def get_vectors(self):
        surface, _ = get_surface_atoms(self._protein.file_name)
        return np.array([get_vector_to_surface(res, surface) for res in self._protein.biopython_residues])

    def load(self, protein_name):
        filen = self._get_file_name(protein_name)
        return np.load(filen[1])


def get_alpha_carbon(residue):
    ca = None
    if is_aa(residue) and residue.has_id('CA'):
        ca = residue['CA'].get_coord()
    return ca


def ccao_cos_angle(res1, res2):
    norm1 = get_ccao_plane_normal(res1)
    norm2 = get_ccao_plane_normal(res2)
    return np.dot(norm1, norm2)


def canc_cos_angle(res1, res2):
    norm1 = get_canc_plane_normal(res1)
    norm2 = get_canc_plane_normal(res2)
    return np.dot(norm1, norm2)


def hsaa_cos_angle(res1, res2):
    vec1 = HalfSphereAminoAcidCompositionExtractor._get_side_chain_vector(res1)
    vec2 = HalfSphereAminoAcidCompositionExtractor._get_side_chain_vector(res2)
    norm1 = vec1/np.linalg.norm(vec1)
    norm2 = vec2/np.linalg.norm(vec2)
    return np.dot(norm1, norm2)


def surf_cos_angle(res1, res2, surface):
    norm1 = get_vector_to_surface(res1, surface)
    norm2 = get_vector_to_surface(res2, surface)
    return np.dot(norm1, norm2)


# this is the peptide plane
def get_ccao_plane_normal(residue):
    # get alpha carbon coords
    norm = None
    if is_aa(residue) and all([residue.has_id(id) for id in ['CA', 'C', 'O']]):
        ca = residue['CA'].get_coord()
        c = residue['C'].get_coord()
        o = residue['O'].get_coord()
        direction = np.cross(ca-c, o-c)
        norm = direction/np.linalg.norm(direction)
    elif is_aa(residue):
        msg = "{} is missing atoms, atoms present: {}".format(residue, [id + ":" + str(residue.has_id(id)) for id in ['CA', 'C', 'O']])
        logger.debug(msg)
    else:
        logger.debug("{} is not an amino acid")

    return norm


# this plane centers on the alpha carbon and perhaps better represents the orientation of the residue
def get_canc_plane_normal(residue):
    # get alpha carbon coords
    norm = None
    if is_aa(residue) and all([residue.has_id(id) for id in ['CA', 'C', 'N']]):
        ca = residue['CA'].get_coord()
        c = residue['C'].get_coord()
        n = residue['N'].get_coord()
        direction = np.cross(n-ca, c-ca)
        norm = direction/np.linalg.norm(direction)
    elif is_aa(residue):
        msg = "{} is missing atoms, atoms present: {}".format(residue, [id + ":" + str(residue.has_id(id)) for id in ['CA', 'C', 'N']])
        logger.debug(msg)
    else:
        logger.debug("{} is not an amino acid")
    return norm


def get_hsaa_vector(residue):
    vec = HalfSphereAminoAcidCompositionExtractor._get_side_chain_vector(residue)
    if vec is not None:
        vec /= np.linalg.norm(vec)
    return vec


def get_vector_to_surface(residue, surface):
    # return vector from center to nearest point on surface
    vec = None
    if is_aa(residue) and residue.has_id('CA'):
        center = get_alpha_carbon(residue)
        distances = np.linalg.norm(surface - center, None, 1)
        surface_point = surface[np.argmin(distances)]
        surface_vector = surface_point - center
        vec = surface_vector/np.linalg.norm(surface_vector)
    elif is_aa(residue):
        msg = "{} is missing CA atom".format(residue)
        logger.debug(msg)
    else:
        logger.debug("{} is not an amino acid")
    return vec
