import numpy as np
import logging
from sklearn.preprocessing import normalize

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from data.feature_extractors import ProfileExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "ShortWindowProfileExtractor",
]


class ShortWindowProfileExtractor(ProfileExtractor):
    def load(self, protein_name):
        _, wpssm_file, wpsfm_file = self._get_file_name(protein_name)
        wpssm = np.load(wpssm_file)
        wpsfm = np.load(wpsfm_file)
        middle = int(wpssm.shape[1] / 2) + 1
        return normalize(np.hstack((wpssm[:, middle - 5:middle + 5], wpsfm[:, middle - 3:middle + 3])))
