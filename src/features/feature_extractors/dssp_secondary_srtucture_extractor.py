import os
import numpy as np
from Bio.PDB import DSSP
from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class DSSPSecondaryStructureExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(DSSPSecondaryStructureExtractor, self).__init__(configuration)
        self.ss_abbreviations = self._configuration["reddinng"]["dssp_ss_abbreviations"]

    def _get_directory(self):
        return self._configuration["reddinng"]["dssp_directory"]

    def _get_file_name(self, protein_name):
        return os.path.join(self._directory, protein_name + ".npy")

    def extract(self, protein, save=True):
        dssp_file = self._get_file_name(protein.name)
        if not os.path.exists(dssp_file):
            self._logger.info("running DSSP for protein " + protein.name)
            n_ss = len(self.ss_abbreviations)
            n = len(protein.residues)
            secondary_structure_dict = dict(zip(self.ss_abbreviations, range(n_ss)))
            dssp = DSSP(protein.structure[0], protein.file_name)
            np.zeros((n, n_ss + 4))
            # ss, asa, rasa, phi, psi
            values = []
            for (i, res) in enumerate(protein.biopython_residues):
                (_, _, cid, rid) = res.get_full_id()
                key = (cid, rid)
                if key in dssp:
                    values.append(dssp[key])
                else:
                    ss_l.append('-')
                    asa_l.append(np.nan)
                    rasa_l.append(np.nan)
                    phi_l.append(np.nan)
                    psi_l.append(np.nan)
            dssp_array = np.hstack((np.array(ss_l), np.array(asa_l), np.array(rasa_l), np.array(phi_l), np.array(psi_l)))
            for i, array in enumerate(arrays):
                np.save(names[i], array)

    def load(self, protein_name):
        all_arrays = []
        for name in self._get_file_name(protein_name):
            all_arrays.append(np.load(name))
        return all_arrays
