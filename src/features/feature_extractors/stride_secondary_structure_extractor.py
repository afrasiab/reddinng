import os
import numpy as np

from src.data.tools.stride import Stride
from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class StrideSecondaryStructureExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(StrideSecondaryStructureExtractor, self).__init__(configuration)
        self.ss_abbreviations = self._configuration["reddinng"]["stride_ss_abbreviations"]

    def _get_directory(self):
        return self._configuration["reddinng"]["stride_directory"]

    def _get_file_name(self, protein_name):
        return os.path.join(self._directory, protein_name + ".npy")

    def extract(self, protein, save=True):
        stride_file = self._get_file_name(protein.name)
        if not os.path.exists(stride_file):
            self._logger.info("Computing secondary structural features for {0} using stride".format(protein.name))
            n_ss = len(self.ss_abbreviations)
            secondary_structure_dict = dict(zip(self.ss_abbreviations, range(n_ss)))
            n = len(protein.residues)
            stride_dictionary = Stride(self._configuration).get_predictions(protein.file_name)
            stride_array = np.zeros((n, n_ss+4))
            for i, residue in enumerate(protein.biopython_residues):
                (_, _, cid, (_, residue_index, ri_num)) = residue.get_full_id()
                key = cid, str(residue_index) + ri_num.strip()
                if key in stride_dictionary:
                    (_, s, phi, psi, asa, r_asa) = stride_dictionary[key]
                    if s not in secondary_structure_dict:
                        message = "unknown secondary structure! Add to dictionary!"
                        self._logger.error(message)
                        raise ValueError(message)
                    # one-hot encoding of the secondary structures
                    ss = np.zeros(len(secondary_structure_dict))
                    ss[secondary_structure_dict[s]] = 1
                    stride_array[i, :n_ss] = ss
                    stride_array[i, n_ss+0] = phi/180.
                    stride_array[i, n_ss+1] = psi/180.
                    stride_array[i, n_ss+2] = r_asa
                else:
                    stride_array[i, :] = np.nan
            if save:
                np.save(stride_file, stride_array)
            return stride_array
        else:
            return np.load(stride_file)
