import logging
import numpy as np
import os
from Bio.PDB import is_aa

from configuration import side_chain_direction_directory
from utils import IndentLogger
from data.feature_extractors import AbstractFeatureExtractor

logger = IndentLogger(logging.getLogger(''), {})


__author__ = 'basir shariat (basir@rams.colostate.edu)'


class SideChainDirectionExtractor(AbstractFeatureExtractor):
    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(SideChainDirectionExtractor, self).__init__(database_directory, protein_name, protein)
        myclass = type(self).__name__
        if "n_bins" not in kwargs:
            self.n_bins = 10
        if "distance_matrix" in kwargs:
            if "preprocess" in kwargs["distance_matrix"]:
                self.preprocess = kwargs["distance_matrix"]["preprocess"]
        elif myclass in kwargs:
            if "preprocess" in kwargs[myclass]:
                self.preprocess = kwargs[myclass]["preprocess"]
        else:
            self.preprocess = None

    def extract(self):
        file_names = self._get_file_name(self._protein.name)
        if not all([os.path.exists(file_name) for file_name in file_names]):
            logger.info("Computing side chain directions for {0}".format(self._protein.name))
            # for each residue...
            directions_dot = np.zeros((len(self._protein.residues), len(self._protein.residues), 1))
            directions_cross = np.zeros((len(self._protein.residues), len(self._protein.residues), 3))
            directions = self.get_residue_directions()
            for i in range(len(self._protein.residues)):
                for j in range(i):
                    directions_dot[i, j, :] = directions_dot[j, i, :] = np.dot(directions[i], directions[j])
                    directions_cross[i, j, :] = directions_cross[j, i, :] = np.cross(directions[i], directions[j])

            np.save(file_names[0], directions_dot)
            np.save(file_names[1], directions_cross)

    def _get_file_name(self, protein_name):
        directory = os.path.join(self.features_directory, side_chain_direction_directory)
        if not os.path.exists(directory):
            os.makedirs(directory)
        return os.path.join(directory, protein_name + "_dot.npy"), os.path.join(directory, protein_name + "_cross.npy")

    def get_residue_directions(self):
        directions = []
        for residue in self._protein.residues:
            if is_aa(residue.residue):
                vectors = []
                for atom in residue:
                    if atom.id == "CA":
                        continue
                    else:
                        vectors.append(residue["CA"].get_coordinates() - atom.get_coordinates())
                directions.append(np.mean(vectors))
            else:
                directions.append(np.mean(residue.get_coordinates() - residue.center))
        return directions

