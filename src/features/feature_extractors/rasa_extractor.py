from src.features.feature_extractors.stride_secondary_structure_extractor import StrideSecondaryStructureExtractor
import numpy as np

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class RASAExtractor(StrideSecondaryStructureExtractor):
    def extract(self, protein, save=True):
        stride_array = super(StrideSecondaryStructureExtractor, self).extract(protein, save=save)
        return stride_array[:, -1]
