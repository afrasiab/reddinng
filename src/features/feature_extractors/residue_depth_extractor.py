import os
import numpy as np
from Bio.PDB import is_aa
from scipy.linalg import norm
from scipy.spatial.distance import cdist

from src.data.tools.msms import MSMS

from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ResidueDepthExtractor(AbstractFeatureExtractor):
    def __init__(self, configuration):
        super(ResidueDepthExtractor, self).__init__(configuration)

    def _get_directory(self):
        return self._configuration["reddinng"]["residue_depth_directory"]

    def _get_file_name(self, protein_name):
        return os.path.join(self._directory, protein_name + ".npy")

    def extract(self, protein, save=True):
        residue_depth_file = self._get_file_name(protein.name)
        if not os.path.exists(residue_depth_file):
            self._logger.info("Computing residue depth for {0}".format(protein.name))
            surface_coordinates = MSMS(self._configuration).get_surface(protein.file_name)[0]
            residue_depth_array = np.zeros((len(protein.residues), 2))
            for (i, residue) in enumerate(protein.residues):
                residue_depth_array[i, :] = self._get_depth(residue, surface_coordinates)
            if save:
                np.save(residue_depth_file, residue_depth_array)
            return residue_depth_array
        else:
            return np.load(residue_depth_file)

    @staticmethod
    def _get_depth(residue, surface):
        depth = cdist(residue.get_coordinates(), surface).min()
        if not residue.residue.has_id("CA"):
            return depth, -1
        else:
            ca = residue.residue["CA"]
            ca_depth = cdist(surface, [ca.get_coord()]).min()
            return depth, ca_depth

