import os
import logging
from random import seed
import numpy as np

from Bio.PDB import NeighborSearch

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import d2_category_directory
from data.feature_extractors.base_shape_distribution import ResidueCategories, categories
from data.feature_extractors import D2BaseShapeDistributionExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D2CategoryShapeDistributionExtractor",
]


class D2CategoryShapeDistributionExtractor(D2BaseShapeDistributionExtractor):
    def _get_file_name(self, protein_name, **kwargs):
        directory = os.path.join(self.features_directory, d2_category_directory, self._get_directory(), "")
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            seed(self.seed)
            logger.info("Computing D2 category shape distribution for {0}".format(self._protein.name))
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins * 3))
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                nearby_residues = neighbour_search.search(residue.center, self.radius, "R")
                distributions[i, :] = self._compute_distribution_category(nearby_residues)
            np.save(shape_dist_file, distributions)

    def _compute_distribution_category(self, nearby_res):
        polar_charged = []
        hydrophobic_charged = []
        hydrophobic_polar = []
        all_cat = [polar_charged, hydrophobic_charged, hydrophobic_polar]
        for res in nearby_res:
            if res.resname not in categories:
                continue
            if categories[res.resname] == ResidueCategories.Polar:
                polar_charged.append(res)
                hydrophobic_polar.append(res)
            if categories[res.resname] == ResidueCategories.Hydrophobic:
                hydrophobic_charged.append(res)
                hydrophobic_polar.append(res)
            if categories[res.resname] == ResidueCategories.Charged:
                polar_charged.append(res)
                hydrophobic_charged.append(res)
        final_distribution = []
        for category in all_cat:
            final_distribution.extend(list(self._compute_distribution(category)))
        return self._normalize(np.array(final_distribution))
