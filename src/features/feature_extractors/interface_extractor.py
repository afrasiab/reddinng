import os
import numpy as np
import copy
import cPickle as cp
from src.features.feature_extractors.abstract_feature_extractor import AbstractFeatureExtractor
from src.features.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class InterfaceExtractor(AbstractFeatureExtractor):
    # todo!!! very important this extractor assumes that the ligand and receptor have only on chain and matching ids
    def __init__(self, configuration):
        self.radius = configuration["reddinng"]["interaction_thr"]
        super(InterfaceExtractor, self).__init__(configuration)

    def _get_directory(self):
        return os.path.join(self._configuration["reddinng"]["interface_directory"], str(self.radius))

    def _get_file_name(self, protein_name):
        directory = os.path.join(self._directory, protein_name[:4])
        try:
            os.makedirs(directory)
        except OSError:
            pass
        return os.path.join(directory, protein_name + ".npy")

    def extract(self, protein, save=True):
        interface_file = self._get_file_name(protein.name)
        if not os.path.exists(interface_file):
            self._logger.info("Computing interface for {0} (read the comments in the class!!!)".format(protein.name))
            conf = copy.copy(self._configuration)
            conf["neighborhood_r"] = self.radius
            residue_neighbour_array = ResidueNeighbourhoodExtractor(conf).extract(protein)
            interface_set = set()
            for i in range(len(protein.biopython_residues)):
                res1 = protein.biopython_residues[i]
                for j in residue_neighbour_array[i]:
                    if j == -1:
                        break
                    res2 = protein.biopython_residues[j]
                    if res1.parent.id != res2.parent.id:
                        if res1.parent.id < res2.parent.id:
                            c1_id, r1_id, c2_id, r2_id = res1.parent.id, res1.id[1], res2.parent.id, res2.id[1]
                        else:
                            c1_id, r1_id, c2_id, r2_id = res2.parent.id, res2.id[1], res1.parent.id, res1.id[1]
                        interface_set.add("{}_{}_{}_{}".format(c1_id, r1_id, c2_id, r2_id))
            if save:
                cp.dump(interface_set, open(interface_file, "w"))
            return interface_set
        else:
            return cp.load(open(interface_file))
