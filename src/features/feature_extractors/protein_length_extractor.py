import os
import logging
import numpy as np

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import length_directory
from data.feature_extractors import AbstractFeatureExtractor

__author__ = 'basir'

__all__ = [
    "ProteinLengthExtractor",
]


class ProteinLengthExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        return os.path.join(self.features_directory, length_directory, protein_name + ".npy")

    def load(self, protein_name):
        return int(np.load(self._get_file_name(protein_name)))

    def extract(self):
        size_file_name = self._get_file_name(self._protein.name)
        if not os.path.exists(size_file_name):
            logger.info("Computing protein length for {0}".format(self._protein.name))
            np.save(size_file_name, len(self._protein.residues))
