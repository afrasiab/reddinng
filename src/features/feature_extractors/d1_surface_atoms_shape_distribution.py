import logging

import numpy as np
import os
from configuration import d1_surface_atoms_directory, rNH
from data.feature_extractors import D1BaseShapeDistributionExtractor
from data.tools.msms import get_surface_atoms
from numpy.random.mtrand import seed
from scipy.spatial.distance import cdist

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D1SurfaceAtomsShapeDistributionExtractor",
]


class D1SurfaceAtomsShapeDistributionExtractor(D1BaseShapeDistributionExtractor):
    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(D1SurfaceAtomsShapeDistributionExtractor, self).__init__(database_directory, protein_name, protein)
        if 'rNH' not in kwargs:
            kwargs['rNH'] = rNH
        self.rNH = kwargs['rNH']

    def _get_file_name(self, protein_name):
        directory = os.path.join(self.features_directory, d1_surface_atoms_directory,
                                 self._get_directory() + "-{0}/".format(self.rNH))
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            logger.info("Computing D1 surface atom shape distribution for {0}".format(self._protein.name))
            seed(self.seed)
            surface, normals = get_surface_atoms(self._protein.file_name)
            distributions = np.zeros((len(self._protein.residues), 2 * self.number_of_bins))

            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                distributions[i, :] = self.get_distributions(residue.center, surface, normals)
            np.save(shape_dist_file, distributions)

    def get_distributions(self, center, surface, normals):
        c = np.copy(center).reshape((1, center.size))
        distances = cdist(surface, c).reshape((surface.shape[0]))
        neighbourhood = np.where(distances < self.rNH)[0]
        neighbourhood_normals = normals[neighbourhood, :]

        dist = distances[neighbourhood]
        bins = np.linspace(0, self.rNH, self.number_of_bins + 1)
        indices = np.digitize(dist, bins)
        dist_distribution = np.zeros((self.number_of_bins,))
        dist_distribution[:max(indices)] = np.bincount(indices)[1:] / float(len(neighbourhood))

        mean_norm = np.mean(neighbourhood_normals, axis=0)
        dots = neighbourhood_normals.dot(mean_norm.T)
        bins = np.linspace(-1, 1, self.number_of_bins + 1)
        indices = np.digitize(dots, bins)
        norm_distribution = np.zeros((self.number_of_bins,))
        norm_distribution[:max(indices)] = np.bincount(indices)[1:] / float(len(neighbourhood))

        return np.hstack((dist_distribution, norm_distribution))
