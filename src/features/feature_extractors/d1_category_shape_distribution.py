import os
import logging
from random import seed
import numpy as np
from Bio.PDB import NeighborSearch

from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})
from configuration import shape_dist_directory, d1_category_directory
from data.feature_extractors.base_shape_distribution import ResidueCategories, categories
from data.feature_extractors import D1BaseShapeDistributionExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "D1CategoryShapeDistributionExtractor",
]


class D1CategoryShapeDistributionExtractor(D1BaseShapeDistributionExtractor):
    def __init__(self, database_directory, protein_name=None, protein=None, **kwargs):
        super(D1CategoryShapeDistributionExtractor, self).__init__(database_directory, protein_name, protein, **kwargs)
        self.radius = 25

    def _get_file_name(self, protein_name):
        directory = os.path.join(self.features_directory, shape_dist_directory, d1_category_directory, self._get_directory(), "")
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            seed(self.seed)
            logger.info("Computing plain D1 category shape distribution for {0}".format(self._protein.name))
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins * 3))
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                nearby_residues = neighbour_search.search(residue.center, self.radius, "R")
                distributions[i, :] = self._compute_category_distribution(nearby_residues, residue.center)
            np.save(shape_dist_file, distributions)

    def _compute_category_distribution(self, nearby_res, center):
        polar_charged = []
        hydrophobic_charged = []
        hydrophobic_polar = []
        all_cat = [polar_charged, hydrophobic_charged, hydrophobic_polar]
        for res in nearby_res:
            if res.resname not in categories:
                continue
            if categories[res.resname] == ResidueCategories.Polar:
                polar_charged.append(res)
                hydrophobic_polar.append(res)
            if categories[res.resname] == ResidueCategories.Hydrophobic:
                hydrophobic_charged.append(res)
                hydrophobic_polar.append(res)
            if categories[res.resname] == ResidueCategories.Charged:
                polar_charged.append(res)
                hydrophobic_charged.append(res)
        final_distribution = []
        for category in all_cat:
            final_distribution.extend(list(self._compute_distribution(category, center)))
        return np.array(final_distribution) / len(all_cat)

