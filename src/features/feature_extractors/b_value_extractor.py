import os
import logging
import numpy as np

from configuration import b_value_directory
from data.feature_extractors import AbstractFeatureExtractor
from utils import IndentLogger
logger = IndentLogger(logging.getLogger(''), {})

__author__ = 'basir shariat (basir@rams.colostate.edu)'

__all__ = [
    "BValueExtractor",
]


class BValueExtractor(AbstractFeatureExtractor):

    def _get_file_name(self, protein_name):
        return os.path.join(self.features_directory, b_value_directory, protein_name + ".npy")

    def extract(self):
        if not os.path.exists(self._get_file_name(self._protein.name)):
            logger.info("Computing B-factor for {0}".format(self._protein.name))
            b_factor_array = np.zeros(len(self._protein.residues))
            for (index, residue) in enumerate(self._protein.biopython_residues):
                b_factor_array[index] = max([atom.get_bfactor() for atom in residue])
            np.save(self._get_file_name(self._protein.name), self._normalize(b_factor_array))

