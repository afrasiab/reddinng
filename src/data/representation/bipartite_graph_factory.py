import glob

import traceback

import sys
from random import shuffle

from os.path import basename

from src.data.dataobjects.bipartite_graph import BipartiteGraph
from src.data.pdb.pdb_reader import PDBReader
from src.data.proteins.protein import Protein
from src.data.representation.abstract_representation_factory import AbstractRepresentation
import numpy as np
import os
import copy
from src.features.feature_extractors.interface_array_extractor import InterfaceArrayExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def extract_rep(args):
    try:
        import os
        conf, object_id = args
        os.chdir(conf["reddinng"]["reddinng_home"])
        import numpy
        import copy
        from src.features.feature_extractors.adjacency_matrix_extractor import AdjacencyMatrixExtractor
        from src.features.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor
        from src.data.pdb.pdb_reader import PDBReader
        from src.features import FeatureExtractors
        from src.data.proteins.protein import Protein
        from os.path import basename
        import cPickle as cP
        import sklearn
        decoy = Protein(*PDBReader().read(object_id))
        index_map = conf["index_map"]
        r_file = conf["file"]
        v_l = conf["v_l"]
        v_r = conf["v_r"]
        interface_residues = conf["interface"]
        n = len(interface_residues)
        v = numpy.zeros((len(interface_residues), v_l.shape[1]))
        # setting up the masks for inter protein and between protein edges
        adj = numpy.zeros((len(interface_residues), len(interface_residues)))

        l_n = len(index_map["ligand"])
        decoy_adj = AdjacencyMatrixExtractor(conf).extract(decoy)
        adj[0:l_n, l_n:n] = decoy_adj[numpy.ix_(index_map["ligand"].keys(), index_map["receptor"].keys())]
        adj[l_n:n, 0:l_n] = decoy_adj[numpy.ix_(index_map["receptor"].keys(), index_map["ligand"].keys())]

        v[0:l_n, :] = v_l[index_map["ligand"].values()]
        v[l_n:, :] = v_r[index_map["receptor"].values()]
        configs = copy.copy(conf)
        e = [adj]
        for feature in conf["features"]:
            if isinstance(feature, dict):
                params = feature.values()[0]
                feature = feature.keys()[0]
                for param in params:
                    configs[param] = params[param]
            extractor, _, f_type = FeatureExtractors[feature]

            if f_type == "edge":
                f = extractor(configs).extract(decoy, save=False)
                if len(f.shape) == 2:
                    np.expand_dims(f, 2)
                d = f.shape[2]
                f_i = numpy.zeros((adj.shape[0], adj.shape[1], d))
                f_i[0:l_n, 0:l_n, :] = f[numpy.ix_(index_map["ligand"].keys(), index_map["ligand"].keys(), range(d))]
                f_i[0:l_n, l_n:n, :] = f[numpy.ix_(index_map["ligand"].keys(), index_map["receptor"].keys(), range(d))]
                f_i[l_n:n, l_n:n, :] = f[
                    numpy.ix_(index_map["receptor"].keys(), index_map["receptor"].keys(), range(d))]
                f_i[l_n:n, 0:l_n, :] = f[numpy.ix_(index_map["receptor"].keys(), index_map["ligand"].keys(), range(d))]
                e.append(numpy.nan_to_num(f_i))
        e = numpy.dstack(e)
        label_file = os.path.join(conf["labels_directory"], basename(object_id)[:4], "{}.lbl.cp".format(decoy.name))
        label = cP.load(open(label_file))
        v = sklearn.preprocessing.normalize(numpy.nan_to_num(v))
        f = open(r_file, "w")
        numpy.savez(f, v=v, e=e, lbl=label)
        f.close()
        return "successful"
    except Exception:
        traceback.print_exc(file=sys.stdout)
        return traceback.format_exc()


class BipartiteGraphFactory(AbstractRepresentation):
    def __init__(self, configuration):
        super(BipartiteGraphFactory, self).__init__(configuration)
        imd = self._configuration["reddinng"]["decoy_index_maps"]
        self.index_map = os.path.join(self._configuration["dataset_directory"], imd)

    def _get_directory(self):
        from src.features import Features
        feature_set = [Features[feature.keys()[0]] if isinstance(feature, dict) else Features[feature]
                       for feature in self._configuration["features"]]
        feature_set.sort()
        feature_set = [str(r) for r in feature_set]
        features = "-".join(feature_set)
        return os.path.join(self._configuration["reddinng"]["bipartite_graph_directory"], features)

    def _get_file_name(self, protein_name):
        if not os.path.exists(os.path.join(self._directory, protein_name[:4])):
            os.mkdir(os.path.join(self._directory, protein_name[:4]))
        return os.path.join(self._directory, protein_name[:4], protein_name + ".npy")

    def get_object_ids(self):
        pass

    def extract(self, object_ids):
        parallel = "parallel" in self._configuration and self._configuration["parallel"]
        graphs = {}

        if parallel:
            import dispy
            import dispy.httpd
            cluster = dispy.JobCluster(extract_rep)
            http_server = dispy.httpd.DispyHTTPServer(cluster)
            node_cores = [(line.strip().split()[0], line.strip().split()[1]) for line in
                          open(os.path.join(os.getenv("RD_CONF"), "dispy_machines_with_cores"))]
            nodes = [node for node, cores in node_cores for _ in range(int(cores))]
            shuffle(nodes)
            i = 0
            for a_complex in object_ids:
                graphs[a_complex] = []
                jobs = []
                v_l, v_r, ligand, receptor = self.get_vertex_matrix(a_complex)
                self._logger.info("extracting graph representations for decoys of {}".format(a_complex))
                for decoy_file in object_ids[a_complex]:
                    rep_file = self._get_file_name(basename(decoy_file).split(".")[0])
                    if not os.path.exists(rep_file):
                        conf = copy.copy(self._configuration)
                        decoy = Protein(*PDBReader().read(decoy_file))
                        conf['v_l'] = v_l
                        conf['v_r'] = v_r
                        conf['index_map'], conf['interface'] = self._get_index_map(ligand, receptor, decoy)
                        if len(conf['interface']) == 0:
                            self._logger.info("Decoy {} has no interface! removing it.".format(decoy.name))
                            continue
                        conf['file'] = rep_file
                        job = cluster.submit_node(nodes[i % len(nodes)], [conf, decoy_file])
                        while job is None:
                            nodes.remove(nodes[i % len(nodes)])
                            i += 1
                            job = cluster.submit_node(nodes[i % len(nodes)])
                        job.id = decoy_file
                        jobs.append(job)
                        i += 1
                    else:
                        graphs[a_complex].append(BipartiteGraph(rep_file))
                cluster.wait()
                for job in jobs:
                    r = job()
                    if r == "successful":
                        graphs[a_complex].append(BipartiteGraph(job.id))
                    else:
                        self._logger.error(r)
            http_server.shutdown()
            cluster.close()
        else:
            for a_complex in object_ids:
                graphs[a_complex] = []
                self._logger.info("extracting graph representations for decoys of {}".format(a_complex))
                v_l, v_r, ligand, receptor = self.get_vertex_matrix(a_complex)
                for decoy_file in object_ids[a_complex]:
                    rep_file = self._get_file_name(basename(decoy_file).split(".")[0])
                    if not os.path.exists(rep_file):
                        conf = copy.copy(self._configuration)
                        decoy = Protein(*PDBReader().read(decoy_file))
                        conf['v_l'] = v_l
                        conf['v_r'] = v_r
                        conf['index_map'], conf['interface'] = self._get_index_map(ligand, receptor, decoy)
                        if len(conf['interface']) == 0:
                            self._logger.info("Decoy {} has no interface! removing it.".format(decoy.name))
                            continue
                        conf['file'] = rep_file
                        r = extract_rep([conf, decoy_file])
                        if r == "successful":
                            graphs[a_complex].append(BipartiteGraph(conf['file']))
                        else:
                            self._logger.error(r)
                    else:
                        graphs[a_complex].append(BipartiteGraph(rep_file))
        return graphs

    def get_vertex_matrix(self, a_complex):
        conf = copy.copy(self._configuration)
        ligand = os.path.join(self._configuration["vertex_features_folder"], "{}_l_u.*".format(a_complex))
        receptor = os.path.join(self._configuration["vertex_features_folder"], "{}_r_u.*".format(a_complex))
        ligand = Protein(*PDBReader().read(glob.glob(ligand)[0]))
        receptor = Protein(*PDBReader().read(glob.glob(receptor)[0]))
        v_l = []
        v_r = []
        from src.features import FeatureExtractors
        for feature in conf["features"]:
            if isinstance(feature, dict):
                params = feature.values()[0]
                feature = feature.keys()[0]
                for param in params:
                    conf[param] = params[param]

            extractor, _, f_type = FeatureExtractors[feature]
            for p, v in [(ligand, v_l), (receptor, v_r)]:
                if f_type == "vertex":
                    f = extractor(conf).extract(p)
                    v.append(f)
        return np.hstack(tuple(v_l)), np.hstack(tuple(v_r)), ligand, receptor

    def load(self, object_ids):
        graphs_files = self.extract(object_ids)
        return graphs_files

    def _get_index_map(self, ligand, receptor, decoy):
        index_map = {"ligand": {}, "receptor": {}}
        ligand_res_index_map = {}
        receptor_res_index_map = {}
        interface_residues_indices = np.where(InterfaceArrayExtractor(self._configuration).extract(decoy))[0]
        for protein, res_id in [(ligand, ligand_res_index_map), (receptor, receptor_res_index_map)]:
            for i, residue in enumerate(protein.biopython_residues):
                chain_id = residue.parent.id
                res_id["{}_{}".format(chain_id, residue.id[1])] = i
        l_ids = set(ligand_res_index_map.keys())
        r_ids = set(receptor_res_index_map.keys())
        for i, residue in enumerate(decoy.biopython_residues):
            if i not in interface_residues_indices:
                continue
            chain_id = residue.parent.id
            res_id = "{}_{}".format(chain_id, residue.id[1])
            if res_id in l_ids:
                index_map["ligand"][i] = ligand_res_index_map[res_id]
            elif res_id in r_ids:
                index_map["receptor"][i] = receptor_res_index_map[res_id]
            else:
                raise ValueError("WTF!")
        return index_map, interface_residues_indices
