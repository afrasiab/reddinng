import glob

import traceback

import sys

from os.path import basename
from src.data.pdb.pdb_reader import PDBReader
from src.data.proteins.protein import Protein
from src.data.representation.abstract_representation_factory import AbstractRepresentation
import numpy as np
import os
import copy
from src.data.dataobjects.bicolored_graph import BicoloredGraph

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def extract_rep(args):
    try:
        import os
        conf, object_id = args
        os.chdir(conf["reddinng"]["reddinng_home"])
        import numpy
        import copy
        from src.features.feature_extractors.adjacency_matrix_extractor import AdjacencyMatrixExtractor
        from src.features.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor
        from src.data.pdb.pdb_reader import PDBReader
        from src.features import FeatureExtractors
        from src.data.proteins.protein import Protein
        from os.path import basename
        import cPickle as cP
        import sklearn
        decoy = Protein(*PDBReader().read(object_id))
        index_map = conf["index_map"]

        def get_interface_indices(decoy, index_map):
            interface_set = set()
            color = numpy.zeros((len(decoy.biopython_residues), 1))
            color[index_map["ligand"], 0] = 0
            color[index_map["receptor"], 0] = 1
            n_res = ResidueNeighbourhoodExtractor(conf).extract(decoy)
            for i in range(n_res.shape[0]):
                for j in n_res[i, :]:
                    if j == -1:
                        continue
                    if color[i] != color[j]:
                        interface_set.add(i)
                        interface_set.add(j)
            return list(interface_set)

        representation_file = conf["file"]
        if not os.path.exists(representation_file):
            v_l = conf["v_l"]
            v_r = conf["v_r"]
            v = numpy.zeros((len(decoy.biopython_residues), v_l.shape[1] + 1))
            v[index_map["ligand"], 0] = 0
            v[index_map["receptor"], 0] = 1
            # setting up the masks for inter protein and between protein edges
            colors = v[:, 0].reshape((-1, 1))
            n = v.shape[0]
            adj = AdjacencyMatrixExtractor(conf).extract(decoy)
            inner_mask = numpy.abs(numpy.tile(colors, n) - numpy.tile(colors, n).T)
            outer_mask = 1 - inner_mask
            v[index_map["ligand"], 1:] = v_l
            v[index_map["receptor"], 1:] = v_r
            configs = copy.copy(conf)
            e = [numpy.multiply(adj, inner_mask), numpy.multiply(adj, outer_mask)]
            for feature in conf["features"]:
                if isinstance(feature, dict):
                    params = feature.values()[0]
                    feature = feature.keys()[0]
                    for param in params:
                        configs[param] = params[param]
                extractor, _, f_type = FeatureExtractors[feature]

                if f_type == "edge":
                    f = extractor(configs).extract(decoy, save=False)
                    e.append(sklearn.preprocessing.normalize(numpy.nan_to_num(f)))
            e = numpy.dstack(e)
            # extracting the label
            label_file = os.path.join(conf["labels_directory"], basename(object_id)[:4], "{}.lbl.cp".format(decoy.name))
            label = cP.load(open(label_file))['dockq']
            v[:, 1:] = sklearn.preprocessing.normalize(numpy.nan_to_num(v[:, 1:]))
            if conf['interface_only']:
                interface_residues = get_interface_indices(decoy, index_map)
                v = v[interface_residues, :]
                e = [numpy.ix_(interface_residues, interface_residues)]
            f = open(representation_file, "w")
            numpy.savez(f, v=v, e=e, lbl=label)
            f.close()
        return "successful"
    except Exception:
        traceback.print_exc(file=sys.stdout)
        return traceback.format_exc()


class BiColoredGraphFactory(AbstractRepresentation):
    def __init__(self, configuration):
        super(BiColoredGraphFactory, self).__init__(configuration)
        imd = self._configuration["reddinng"]["decoy_index_maps"]
        self.index_map = os.path.join(self._configuration["dataset_directory"], imd)
        if "args" in self._configuration["representation"]:
            if "interface_only" in self._configuration["representation"]["args"]:
                self._interface_only = self._configuration["representation"]["args"]["interface_only"]
            else:
                self._interface_only = False
        else:
            self._interface_only = False

    def _get_directory(self):
        from src.features import Features
        feature_set = [Features[feature.keys()[0]] if isinstance(feature, dict) else Features[feature]
                       for feature in self._configuration["features"]]
        feature_set.sort()
        feature_set = [str(r) for r in feature_set]
        features = "-".join(feature_set)
        return os.path.join(self._configuration["reddinng"]["bicolored_graph_directory"], features)

    def _get_file_name(self, protein_name):
        if not os.path.exists(os.path.join(self._directory, protein_name[:4])):
            os.mkdir(os.path.join(self._directory, protein_name[:4]))
        return os.path.join(self._directory, protein_name[:4], protein_name + ".npy")

    def get_object_ids(self):
        pass

    def extract(self, object_ids):
        parallel = "parallel" in self._configuration and self._configuration["parallel"]
        graphs = {}

        if parallel:
            import dispy
            import dispy.httpd
            cluster = dispy.JobCluster(extract_rep)
            http_server = dispy.httpd.DispyHTTPServer(cluster)
            for a_complex in object_ids:
                graphs[a_complex] = []
                jobs = []
                v_l, v_r, ligand, receptor = self.get_vertex_matrix(a_complex)
                self._logger.info("extracting graph representations for decoys of {}".format(a_complex))
                for decoy_file in object_ids[a_complex]:
                    rep_file = self._get_file_name(basename(decoy_file).split(".")[0])
                    if not os.path.exists(rep_file):
                        conf = copy.copy(self._configuration)
                        decoy = Protein(*PDBReader().read(decoy_file))
                        conf['v_l'] = v_l
                        conf['v_r'] = v_r
                        conf['index_map'] = self._get_index_map(ligand, receptor, decoy)
                        conf['file'] = rep_file
                        conf['interface_only'] = self._interface_only
                        job = cluster.submit([conf, decoy_file])
                        job.id = decoy_file
                        jobs.append(job)
                    else:
                        graphs[a_complex].append(BicoloredGraph(rep_file))
                cluster.wait()
                for job in jobs:
                    r = job()
                    if r == "successful":
                        graphs[a_complex].append(BicoloredGraph(job.id))
                    else:
                        self._logger.error(r)
            http_server.shutdown()
            cluster.close()
        else:
            for a_complex in object_ids:
                graphs[a_complex] = []
                self._logger.info("extracting graph representations for decoys of {}".format(a_complex))
                v_l, v_r, ligand, receptor = self.get_vertex_matrix(a_complex)
                for decoy_file in object_ids[a_complex]:
                    rep_file = self._get_file_name(basename(decoy_file).split(".")[0])
                    if not os.path.exists(rep_file):
                        conf = copy.copy(self._configuration)
                        decoy = Protein(*PDBReader().read(decoy_file))
                        conf['v_l'] = v_l
                        conf['v_r'] = v_r
                        conf['index_map'] = self._get_index_map(ligand, receptor, decoy)
                        conf['file'] = rep_file
                        conf['interface_only'] = self._interface_only
                        r = extract_rep([conf, decoy_file])
                        if r == "successful":
                            graphs[a_complex].append(BicoloredGraph(conf['file']))
                        else:
                            self._logger.error(r)
                    else:
                        graphs[a_complex].append(BicoloredGraph(rep_file))
        return graphs

    def get_vertex_matrix(self, a_complex):
        conf = copy.copy(self._configuration)
        ligand = os.path.join(self._configuration["vertex_features_folder"], "{}_l_u.*".format(a_complex))
        receptor = os.path.join(self._configuration["vertex_features_folder"], "{}_r_u.*".format(a_complex))
        ligand = Protein(*PDBReader().read(glob.glob(ligand)[0]))
        receptor = Protein(*PDBReader().read(glob.glob(receptor)[0]))
        v_l = []
        v_r = []
        from src.features import FeatureExtractors
        for feature in conf["features"]:
            if isinstance(feature, dict):
                params = feature.values()[0]
                feature = feature.keys()[0]
                for param in params:
                    conf[param] = params[param]

            extractor, _, f_type = FeatureExtractors[feature]
            for p, v in [(ligand, v_l), (receptor, v_r)]:
                if f_type == "vertex":
                    f = extractor(conf).extract(p)
                    v.append(f)
        return np.hstack(tuple(v_l)), np.hstack(tuple(v_r)), ligand, receptor

    def load(self, object_ids):
        graphs_files = self.extract(object_ids)
        return graphs_files

    @staticmethod
    def _get_index_map(ligand, receptor, decoy):
        index_map = {"ligand": [0] * len(ligand.residues), "receptor": [0] * len(receptor.residues)}
        ligand_res_index_map = {}
        receptor_res_index_map = {}
        for protein, res_id in [(ligand, ligand_res_index_map), (receptor, receptor_res_index_map)]:
            for i, residue in enumerate(protein.biopython_residues):
                full_id = residue.get_full_id()
                res_id["{}_{}".format(full_id[2], full_id[3][1])] = i
        l_ids = set(ligand_res_index_map.keys())
        r_ids = set(receptor_res_index_map.keys())
        for i, residue in enumerate(decoy.biopython_residues):
            full_id = residue.get_full_id()
            res_id = "{}_{}".format(full_id[2], full_id[3][1])
            if res_id in l_ids:
                index_map["ligand"][ligand_res_index_map[res_id]] = i
            elif res_id in r_ids:
                index_map["receptor"][receptor_res_index_map[res_id]] = i
            else:
                raise ValueError("WTF!")
        return index_map
