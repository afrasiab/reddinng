from src.data.representation.bicolored_graph_factory import BiColoredGraphFactory
from src.data.representation.bipartite_graph_factory import BipartiteGraphFactory

__author__ = 'basir shariat (basir@rams.colostate.edu)'


RepresentationFactories = {
    'bicolored_graph': BiColoredGraphFactory,
    'bipartite_graph': BipartiteGraphFactory
}
