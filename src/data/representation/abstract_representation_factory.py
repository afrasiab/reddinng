import os

from abc import ABCMeta, abstractmethod
import numpy as np
from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractRepresentation(AbstractLogger):
    __metaclass__ = ABCMeta

    def __init__(self, configuration):
        super(AbstractRepresentation, self).__init__()
        self._configuration = configuration
        self._database_directory = os.path.join(self._configuration["dataset_directory"])
        representations = self._configuration["reddinng"]["representations_directory"]
        self._representations_directory = os.path.join(self._database_directory, representations)
        self._directory = os.path.join(self._representations_directory, self._get_directory())
        if not os.path.exists(self._representations_directory):
            os.mkdir(self._representations_directory)
        if not os.path.exists(self._directory):
            os.makedirs(self._directory)

    @abstractmethod
    def _get_directory(self):
        pass

    @abstractmethod
    def extract(self, protein):
        pass

    def load(self, protein_name):
        return np.load(self._get_file_name(protein_name))

    @abstractmethod
    def _get_file_name(self, protein_name):
        pass