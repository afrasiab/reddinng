import os
import tempfile

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Stride(AbstractLogger):
    """
        From the documentation:
        STRIDE [1] is a program to recognize secondary structural elements  in
        proteins	 from  their atomic coordinates. It performs the same task as
        DSSP by Kabsch and Sander [2] but utilizes both hydrogen	 bond  energy
        and  main chain  dihedral angles	 rather	than hydrogen bonds alone.
        It relies on  database-derived recognition parameters with the
        crystallographers' secondary  structure definitions as a standard-of-
        truth. Please see Frishman and Argos [1] for detailed  description  of
        the algorithm.
        8.  References
            1.  Frishman,D	& Argos,P. (1995) Knowledge-based secondary structure
             assignment.  Proteins:  structure,	function and genetics, 23,
             566-579.

            2.  Kabsch,W. & Sander,C. (1983)  Dictionary  of  protein  secondary
             structure:	   pattern   recognition   of	hydrogen-bonded	  and
             geometrical features. Biopolymers,	22: 2577-2637.
    """

    def __init__(self, configuration):
        super(Stride, self).__init__()
        self._home = configuration["reddinng"]["stride_home"]
        self._executable = configuration["reddinng"]["stride_exe"]
        self.max_asa = configuration["reddinng"]["max_asa"]

    def _parse_stride_output(self, stride_output):
        max_acc = self.max_asa
        stride_out = {}
        handle = open(stride_output, "r")
        try:
            for line in handle.readlines():
                record_elements = line.split()
                if record_elements[0] != "ASG":  # if not detailed secondary structure record
                    continue
                # REM  |---Residue---|    |--Structure--|   |-Phi-|   |-Psi-|  |-Area-|      ~~~~
                # ASG  ALA A    1    1    C          Coil    360.00    -35.26     120.7      ~~~~
                # 0      1 2    3    4    5           6       7          8         9          10
                res_id = (record_elements[2], record_elements[3])
                aa = record_elements[1]
                ss = record_elements[5].upper()  # There was b and B both from Bridge
                phi, psi, asa = float(record_elements[7]), float(record_elements[8]), float(record_elements[9])
                rasa = asa / max_acc[aa] if asa / max_acc[aa] < 1 else 1.0
                stride_out[res_id] = (aa, ss, phi, psi, asa, rasa)
        finally:
            handle.close()
        return stride_out

    def get_predictions(self, pdb_file):
        temp_file = self.get_output_file()
        os.chdir(self._home)
        exit_status = os.system("{0} {1} > {2}".format(self._executable, pdb_file, temp_file))
        if exit_status == 0:
            # successful execution
            self._logger.info("Stride successfully ran on {}".format(pdb_file))
            out_dict = self._parse_stride_output(temp_file)
            os.remove(temp_file)
            return out_dict
        else:
            message = "Stride didn't run correctly, exit status {0}".format(exit_status)
            self._logger.error(message)
            raise ValueError(message)

    @staticmethod
    def get_output_file():
        out_file = tempfile.NamedTemporaryFile(suffix='.stride')
        out_file.flush()
        out_file.close()
        temp_file = out_file.name
        return temp_file
