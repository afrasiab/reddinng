import os
import tempfile
import numpy as np

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class SPINEX(AbstractLogger):

    def __init__(self, configuration):
        super(SPINEX, self).__init__()
        self._spinex_exe = configuration["reddinng"]["spinex_exe"]
        self._max_asa = configuration["reddinng"]["max_asa"]

    def parse_spinex_output(self, output_file_name):
        secondary_structure = []
        phi = []
        psi = []
        asa = []
        rasa = []
        for f in open(output_file_name, 'r'):
            f = f.split()
            if f[0] == '#':
                continue
            phi.append([float(f[8]), float(f[3])])
            psi.append([float(f[9]), float(f[4])])
            secondary_structure.append([float(i) for i in f[5:8]])
            asa.append(float(f[10]))
            try:
                m = self._max_asa[f[1]]
            except KeyError as e:
                self._logger.error(e)
                m = np.nan
            rasa.append(float(f[10]) / m)
        return np.array(asa), np.array(rasa), np.array(secondary_structure), np.array(phi), np.array(psi)

    def give_ss_predictions(self, profile_file):
        name = os.path.basename(profile_file).split(".")[0]
        directory = os.path.dirname(profile_file)
        out_file = tempfile.NamedTemporaryFile(suffix='.spx')
        out_file.close()
        list_file = out_file.name
        (list_file_directory, _, _) = os.path.dirname(list_file)
        output_file_name = os.path.join(list_file_directory, name + '.spinex')
        lfh = open(list_file, 'w+')
        lfh.write(name)
        lfh.close()
        if os.system("%s %s %s %s" % (self._spinex_exe, list_file, directory, list_file_directory)) == 0:
            z = self.parse_spinex_output(output_file_name)
            os.remove(output_file_name)
        else:
            self._logger.error("Error using spinex!")
            z = None
        os.remove(list_file)
        return z
