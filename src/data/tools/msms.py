import numpy as np
import os

from os.path import basename

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class MSMS(AbstractLogger):
    def __init__(self, configuration):
        super(MSMS, self).__init__()
        self._configuration = configuration

    def get_surface(self, pdb_file):
        temp_dir = self._configuration["reddinng"]["temp_directory"]
        extension = ".pdb.ms" if basename(pdb_file).endswith(".pdb.ms") else ".pdb"
        xyz_file = os.path.join(temp_dir, basename(pdb_file).replace(extension, ".xyzr"))
        face_file = os.path.join(temp_dir, basename(pdb_file).replace(extension, ".face"))
        os.chdir(self._configuration["reddinng"]["msms_home"])
        make_xyz_command = self._coordinate_generation_command(pdb_file, xyz_file)
        os.system(make_xyz_command)
        if not os.path.exists(xyz_file):
            message = "Failed to generate XYZR file using command: {0}".format(make_xyz_command)
            self._logger.error(message)
            raise RuntimeError(message)

        msms_output_file = os.path.join(temp_dir, basename(pdb_file).replace(extension, ""))
        vertex_file_command = self._vertex_generation_command(msms_output_file, xyz_file)
        os.system(vertex_file_command)
        surface_file = msms_output_file + ".vert"
        if not os.path.exists(surface_file):
            message = "Failed to generate surface file using command: {0}".format(vertex_file_command)
            self._logger.error(message)
            raise RuntimeError(message)

        normal_list, vertex_list = self.parse_results(surface_file)
        os.remove(xyz_file)
        os.remove(face_file)
        os.remove(surface_file)
        return np.array(vertex_list), np.array(normal_list)

    @staticmethod
    def parse_results(surface_file):
        with open(surface_file, "r") as file_pointer:
            vertex_list = []
            normal_list = []
            for line in file_pointer.readlines():
                sl = line.split()
                if not len(sl) == 9:
                    # skipping the header
                    continue
                vl = [float(x) for x in sl[0:3]]
                nl = [float(x) for x in sl[3:6]]
                vertex_list.append(vl)
                normal_list.append(nl)
        return normal_list, vertex_list

    def _coordinate_generation_command(self, pdb_file, xyz_file):
        return "{0} {1} > {2}".format(
            self._configuration["reddinng"]["msms_pdb2xyz"],
            pdb_file,
            xyz_file)

    def _vertex_generation_command(self, msms_output_file, xyz_file):
        return "{0} -probe_radius 1.5 -if {1} -of {2} > /dev/null".format(
            self._configuration["reddinng"]["msms_exe"],
            xyz_file,
            msms_output_file)
