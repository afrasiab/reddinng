import tempfile
import os
import glob
import numpy as np
import yaml
from os.path import basename

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class PSAIA(AbstractLogger):
    def __init__(self, configuration):
        super(PSAIA, self).__init__()
        self._home = configuration["reddinng"]["psaia_home"]
        self._conf = configuration["reddinng"]["psaia_conf"]
        self._temp = configuration["reddinng"]["temp_directory"]

    def _get_config(self, name):
        config = yaml.load(open(self._conf))
        for key in config:
            if isinstance(config[key], str) and "__home__" in config[key]:
                config[key] = config[key].replace("__home__", self._home)
            if isinstance(config[key], str) and "__temp__" in config[key]:
                config[key] = config[key].replace("__temp__", self._temp)
        config_file_name = os.path.join(self._temp, "{}.psaia.config".format(name))
        config_file = open(config_file_name, "w")
        yaml.dump(config, config_file, default_flow_style=False)
        config_file.flush()
        config_file.close()
        return config_file_name

    def run_psaia(self, pdb_file):
        config = self._get_config(basename(pdb_file))
        inputs = self._get_input(pdb_file)
        try:
            command = 'echo y | {}/psa {} {} > /dev/null'.format(self._home, config, inputs)
            exit_code = os.system(command)
            if exit_code != 0 and exit_code != 256:
                message = "psa did not run properly; exit code {0}".format(exit_code)
                self._logger.error(message)
                raise ValueError(message)
            result = self._get_dictionary(pdb_file)
        finally:
            os.remove(config)
            os.remove(inputs)
        return result

    def _get_dictionary(self, pdb_file):
        protein_name = os.path.splitext(os.path.split(pdb_file)[1])[0]
        psaia_files = glob.glob('{}/{}*bound.tbl'.format(self._temp, protein_name.split(".")[0]))
        try:
            if len(psaia_files) > 0:
                psaia_file = psaia_files[0]
                psaia_dictionary = self.parse_results(psaia_file)
            else:
                psaia_dictionary = None
        finally:
            for temporary_file in psaia_files:
                os.remove(temporary_file)
        return psaia_dictionary

    def _get_input(self, pdb_file):
        input_pdbs_file_name = os.path.join(self._temp, "{}.psaia.input".format(basename(pdb_file)))
        input_pdbs = open(input_pdbs_file_name, "w")
        input_pdbs.write(os.path.abspath(pdb_file))
        input_pdbs.flush()
        input_pdbs.close()
        return input_pdbs_file_name

    def parse_results(self, filename):
        psaia = {}
        line_number = 0
        try:
            for line in open(filename, "r"):
                line_number += 1
                line_parts = line.split()
                # the line containing 'chain' is the last line before real data starts
                if len(line_parts) < 5 or line_parts[0] == 'chain':
                    continue
                protein_id = line_parts[0]
                if protein_id == '*':
                    protein_id = ' '
                residue_id = (protein_id, line_parts[6])
                casa = np.array(map(float, line_parts[1:6]))
                rasa = np.array(map(float, line_parts[8:13]))
                rrasa = np.array(map(float, line_parts[13:18]))
                rdpx = np.array(map(float, line_parts[18:24]))
                rcx = np.array(map(float, line_parts[24:30]))
                rhph = float(line_parts[-1])
                psaia[residue_id] = (casa, rasa, rrasa, rdpx, rcx, rhph)
        except Exception as e:
            self._logger.error('Error Processing psaia file {0}: {1}'.format(filename, e))
            self._logger.error('Error occurred while processing line: {0}'.format(line_number))
            raise e
        return psaia
