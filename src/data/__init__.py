from src.data.datasets.zdock import ZDock


__author__ = 'basir shariat (basir@rams.colostate.edu)'

Datasets = {
    'zdock_irad_3.0.2': ZDock,
    'decoys_bm4_zd3.0.2_6deg': ZDock,
    'decoys_bm4_zd3.0.2_15deg': ZDock,
}


OutputRegex = {
    'zdock_irad_3.0.2': 'zdock_irad_decoy_files_regex',
    'decoys_bm4_zd3.0.2_6deg': 'zdock_302_6deg_bm4_decoy_files_regex',
    'decoys_bm4_zd3.0.2_15deg': 'zdock_302_15deg_bm4_decoy_files_regex',
}


