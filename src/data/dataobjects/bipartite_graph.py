from src.data.dataobjects.abstract_dataobject import AbstractDataObject
import tensorflow as tf
import numpy as np

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class BipartiteGraph(AbstractDataObject):
    def __init__(self, object_file):
        super(BipartiteGraph, self).__init__(object_file)

    def get_label(self):
        f = open(self.object_file)
        graph = np.load(f)
        lbl = graph['lbl']['dockq']
        f.close()
        return lbl

    def get(self):
        f = open(self.object_file)
        graph = np.load(f)
        v, e, lbl = graph['v'], graph['e'], graph['lbl'].tolist()['dockq']
        f.close()
        adj = np.expand_dims(e[:, :, 0], 2)
        e = np.multiply(e[:, :, 1:], adj)
        return v, e, lbl

    def tf_labels_tensor(self, batch_size):
        return tf.placeholder(tf.float32, [batch_size, 1], "labels")

    def tf_inputs_tensor(self, batch_size):
        f = open(self.object_file)
        graph = np.load(f)
        v, e = graph['v'], graph['e']
        f.close()
        vertex_features = tf.placeholder(tf.float32, [None, v.shape[1], batch_size], "vertices")  # n x d_v x b
        edge_features = tf.placeholder(tf.float32, [None, None, e.shape[2]-1, batch_size], "edges")  # n x n x d_e x b
        return vertex_features, edge_features
