from abc import ABCMeta, abstractmethod
import numpy as np

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractDataObject:
    __metaclass__ = ABCMeta

    def __init__(self, object_file):
        super(AbstractDataObject, self).__init__()
        self.object_file = object_file

    @abstractmethod
    def get_label(self):
        pass

    @abstractmethod
    def get(self):
        pass

    @abstractmethod
    def tf_inputs_tensor(self, batch_size):
        pass

    @abstractmethod
    def tf_labels_tensor(self, batch_size):
        pass
