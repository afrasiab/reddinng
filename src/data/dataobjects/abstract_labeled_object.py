from abc import abstractmethod, ABCMeta


class AbstractLabeledObject:
    __metaclass__ = ABCMeta

    def __init__(self, object_id, label):
        self._object_id = object_id
        self._label = label

    @abstractmethod
    def label(self):
        pass

    def get_id(self):
        return self._object_id
