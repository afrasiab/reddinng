import cPickle

from src.data.dataobjects.abstract_labeled_object import AbstractLabeledObject


class DecoyDockQ(AbstractLabeledObject):
    def __init__(self, object_id, label):
        AbstractLabeledObject.__init__(self, object_id, label)

    def label(self):
        return cPickle.load(open(self._label))["dockq"]

