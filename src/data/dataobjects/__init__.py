from src.data.dataobjects.bicolored_graph import BicoloredGraph
from src.data.dataobjects.bipartite_graph import BipartiteGraph

__author__ = 'basir shariat (basir@rams.colostate.edu)'

DataObjects = {
    "bicolored_graph": BicoloredGraph,
    "bipartite_graph": BipartiteGraph
}
