from src.data.dataobjects.abstract_dataobject import AbstractDataObject
import tensorflow as tf
import numpy as np

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class BicoloredGraph(AbstractDataObject):
    def __init__(self, object_file):
        super(BicoloredGraph, self).__init__(object_file)

    def get_label(self):
        f = open(self.object_file)
        graph = np.load(f)
        lbl = graph['lbl']
        f.close()
        return lbl

    def get(self):
        f = open(self.object_file)
        graph = np.load(f)
        v, e, lbl = graph['v'], graph['e'], graph['lbl']
        f.close()
        return v, e, lbl

    def tf_labels_tensor(self, batch_size):
        return tf.placeholder(tf.float32, [1, batch_size], "labels")

    def tf_inputs_tensor(self, batch_size):
        f = open(self.object_file)
        graph = np.load(f)
        v, e = graph['v'], graph['e']
        f.close()
        vertex_features = tf.placeholder(tf.float32, [None, v.shape[1], batch_size], "vertices")  # n x d_v x b
        edge_features = tf.placeholder(tf.float32, [None, None, e.shape[2]-2, batch_size], "edges")  # n x n x d_e x b
        masks = tf.placeholder(tf.float32, [None, None, 2, batch_size], "edge_masks")  # n x n x 2 x b
        return vertex_features, edge_features, masks
