from abc import ABCMeta, abstractmethod

import os

from src.utils.abstract_logger import AbstractLogger


class AbstractSampler(AbstractLogger):
    __metaclass__ = ABCMeta

    def __init__(self, config):
        super(AbstractSampler, self).__init__()
        self._config = config
        self.seed = self._config["seed"]

    @abstractmethod
    def sample(self, objects_id, objects_list):
        pass

    def get_file_name(self, object_id):
        out_dir = os.path.join(self._config["sampling_dir"], str(self.seed))
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        return os.path.join(out_dir, "{}.cp".format(object_id))
