import cPickle

from src.data.samplers.abstract_sampler import AbstractSampler
import numpy as np
import os


class InverseRegressionScoreSampler(AbstractSampler):
    def sample(self, objects_id, objects_list):
        sampled_objects_file = self.get_file_name(objects_id)
        if os.path.exists(sampled_objects_file):
            return cPickle.load(open(sampled_objects_file))
        ratio = self._config["sampling_ratio"]
        n_samples = len(objects_list) * ratio
        chosen_objects = []
        self._logger.info("Sampling {} with inverse of the score".format(objects_id))
        while len(chosen_objects) != n_samples:
            for an_object in objects_list:
                if an_object.label() > 1 or an_object.label() < 0:
                    raise ValueError("labels must be between 0 and 1")
                probability = an_object.label()
                chosen = True if np.random.binomial(1, probability, 1)[0] == 1 else False
                if chosen:
                    chosen_objects.append(an_object.get_id())
                    if len(chosen_objects) == n_samples:
                        break
        cPickle.dump(chosen_objects, open(sampled_objects_file, "w"))
        return chosen_objects
