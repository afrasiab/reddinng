from src.data.metrics.abstract_metric import AbstractMetric
from src.features.feature_extractors.interface_extractor import InterfaceExtractor


class FNATCalculator(AbstractMetric):

    def __init__(self, config):
        super(FNATCalculator, self).__init__(config)

    def get(self, native, decoy):
        native_interface = InterfaceExtractor(self._config).extract(native)
        decoy_interface = InterfaceExtractor(self._config).extract(decoy)
        if len(decoy_interface) == 0:
            return 0., 1.
        f_nat = float(len(native_interface.intersection(decoy_interface))) / len(native_interface)
        f_non_nat = float(len(decoy_interface.difference(native_interface))) / len(decoy_interface)
        return f_nat, f_non_nat
