from Bio.PDB import Superimposer

from src.data.metrics.abstract_metric import AbstractMetric
from src.features.feature_extractors.interface_array_extractor import InterfaceArrayExtractor
import numpy as np


class IRMSDCalculator(AbstractMetric):

    def __init__(self, config):
        super(IRMSDCalculator, self).__init__(config)

    def get(self, native, decoy):
        # this method superimposes the decoy interface on the corresponding native conformation atoms
        decoy_interface = np.where(InterfaceArrayExtractor(self._config).extract(decoy))[0]
        # if no residues fall in the 6 Angstrom threshold return a big number
        if len(decoy_interface) == 0:
            self._logger.error("No interface residues in decoy {}".format(decoy.name))
            return 1000

        native_atoms = {}
        for residue in native.residues:
            b_res = residue.residue
            for atom in residue.get_backbone(ca_only=True):
                atom_key = "{}_{}_{}".format(b_res.parent.id, b_res.id[1], atom.id)
                native_atoms[atom_key] = atom

        decoy_atoms = {}
        for index in decoy_interface:
            residue = decoy.residues[index]
            b_res = residue.residue
            for atom in residue.get_backbone(ca_only=True):
                atom_key = "{}_{}_{}".format(b_res.parent.id, b_res.id[1], atom.id)
                decoy_atoms[atom_key] = atom

        decoy_atoms_list = []
        native_atoms_list = []
        for atom_key in decoy_atoms:
            if atom_key not in native_atoms:
                continue
            decoy_atoms_list.append(decoy_atoms[atom_key])
            native_atoms_list.append(native_atoms[atom_key])
        if len(decoy_atoms_list) == 0:
            return 1000
        super_imposer = Superimposer()
        super_imposer.set_atoms(native_atoms_list, decoy_atoms_list)
        super_imposer.apply(decoy.model.get_atoms())
        irmsd = super_imposer.rms
        return irmsd

    # def get1(self, native, decoy):
    #     # this method superimposes the native interface to the decoy interface
    #     native_interface = np.where(InterfaceArrayExtractor(self._config).extract(native))[0]
    #     decoy_atoms = {}
    #     for residue in decoy.residues:
    #         b_res = residue.residue
    #         for atom in residue.get_backbone(ca_only=True):
    #             atom_key = "{}_{}_{}".format(b_res.parent.id, b_res.id[1], atom.id)
    #             decoy_atoms[atom_key] = atom
    #
    #     native_atoms = {}
    #     for index in native_interface:
    #         residue = native.residues[index]
    #         b_res = residue.residue
    #         for atom in residue.get_backbone(ca_only=True):
    #             atom_key = "{}_{}_{}".format(b_res.parent.id, b_res.id[1], atom.id)
    #             native_atoms[atom_key] = atom
    #
    #     # common_interface = list(set(native_atoms).intersection(set(decoy_atoms)))
    #     decoy_atoms_list = []
    #     native_atoms_list = []
    #     for atom_key in native_atoms:
    #         if atom_key not in decoy_atoms:
    #             continue
    #         decoy_atoms_list.append(decoy_atoms[atom_key])
    #         native_atoms_list.append(native_atoms[atom_key])
    #     if len(decoy_atoms_list) == 0:
    #         return 1000
    #     super_imposer = Superimposer()
    #     super_imposer.set_atoms(native_atoms_list, decoy_atoms_list)
    #     super_imposer.apply(decoy.model.get_atoms())
    #     irmsd = super_imposer.rms
    #     return irmsd
