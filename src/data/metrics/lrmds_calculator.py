from Bio.PDB import Superimposer
from Bio.SVDSuperimposer import SVDSuperimposer

from src.data.metrics.abstract_metric import AbstractMetric
from src.data.pdb.pdb_reader import PDBReader
import numpy as np

from src.data.proteins.protein import Protein


class LRMSDCalculator(AbstractMetric):

    def __init__(self, config):
        super(LRMSDCalculator, self).__init__(config)

    @staticmethod
    def _rms(coords1, coords2):
        diff = coords1 - coords2
        return np.sqrt(sum(sum(diff * diff)) / coords1.shape[0])

    def get(self, native, decoy):
        receptor_u_chains = Protein(*PDBReader().read(native.file_name.replace("_b", "_r_u") + ".ms")).chains.keys()
        receptor_b_chains = Protein(*PDBReader().read(native.file_name.replace("_b", "_r_b") + ".ms")).chains.keys()
        ligand_u_chains = Protein(*PDBReader().read(native.file_name.replace("_b", "_l_u") + ".ms")).chains.keys()
        ligand_b_chains = Protein(*PDBReader().read(native.file_name.replace("_b", "_l_b") + ".ms")).chains.keys()
        r_b_dict = {}
        r_u_dict = {}
        for chain in receptor_b_chains:
            for residue in native.residues:
                if residue.residue.parent.id != chain:
                    continue
                for atom in residue.get_backbone():
                    atom_key = "{}_{}_{}".format(residue.residue.parent.id, residue.residue.id[1], atom.id)
                    r_b_dict[atom_key] = atom

        for chain in receptor_u_chains:
            for residue in decoy.residues:
                if residue.residue.parent.id != chain:
                    continue
                for atom in residue.get_backbone():
                    atom_key = "{}_{}_{}".format(residue.residue.parent.id, residue.residue.id[1], atom.id)
                    r_u_dict[atom_key] = atom

        native_atoms_list = []
        decoy_atoms_list = []
        for atom_key in r_u_dict:
            if atom_key not in r_b_dict:
                continue
            native_atoms_list.append(r_b_dict[atom_key])
            decoy_atoms_list.append(r_u_dict[atom_key])
        super_imposer = Superimposer()
        super_imposer.set_atoms(native_atoms_list, decoy_atoms_list)
        super_imposer.apply(decoy.model.get_atoms())

        coord1 = []
        coord2 = []
        l_b_dict = {}
        l_u_dict = {}
        for chain in ligand_b_chains:
            for residue in native.residues:
                if residue.residue.parent.id != chain:
                    continue
                for atom in residue.get_backbone():
                    atom_key = "{}_{}_{}".format(residue.residue.parent.id, residue.residue.id[1], atom.id)
                    l_b_dict[atom_key] = atom

        for chain in ligand_u_chains:
            for residue in decoy.residues:
                if residue.residue.parent.id != chain:
                    continue
                for atom in residue.get_backbone():
                    atom_key = "{}_{}_{}".format(residue.residue.parent.id, residue.residue.id[1], atom.id)
                    l_u_dict[atom_key] = atom

        for atom_key in l_u_dict:
            if atom_key not in l_b_dict:
                continue
            coord1.append(l_b_dict[atom_key].coord)
            coord2.append(l_u_dict[atom_key].coord)

        lrmsd = self._rms(np.array(coord1), np.array(coord2))
        return lrmsd
