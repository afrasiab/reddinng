from abc import ABCMeta, abstractmethod

from src.utils.abstract_logger import AbstractLogger


class AbstractMetric(AbstractLogger):
    __metaclass__ = ABCMeta

    def __init__(self, config):
        super(AbstractMetric, self).__init__()
        self._config = config

    @abstractmethod
    def get(self, native, decoy):
        pass
