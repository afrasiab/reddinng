from src.data.metrics.abstract_metric import AbstractMetric
from src.data.metrics.fnat_calculator import FNATCalculator
from src.data.metrics.irmsd_calculator import IRMSDCalculator
from src.data.metrics.lrmds_calculator import LRMSDCalculator
from src.data.pdb.pdb_reader import PDBReader
from src.data.proteins.protein import Protein


class DockQCalculator(AbstractMetric):

    def __init__(self, config):
        super(DockQCalculator, self).__init__(config)

    def get(self, native_pdb, decoy_pdb):
        native = Protein(*PDBReader().read(native_pdb))
        decoy = Protein(*PDBReader().read(decoy_pdb))
        fnat, f_non_nat = FNATCalculator(self._config).get(native, decoy)
        irmsd = IRMSDCalculator(self._config).get(native, decoy)
        lrmsd = LRMSDCalculator(self._config).get(native, decoy)
        dockq = (float(fnat) + (1. / (1 + (irmsd / 1.5) ** 2)) + (1. / (1 + (lrmsd / 8.5) ** 2))) / 3
        return dockq, fnat, f_non_nat, irmsd, lrmsd
