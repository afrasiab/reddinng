import numpy as np
from Bio.PDB import CaPPBuilder, CaPPBuilder

from .residue import Residue
from scipy.spatial.distance import cdist
from scipy.spatial.qhull import ConvexHull


__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Protein:

    def __init__(self, file_name, name, structure, residues, sequence, atoms):
        self.file_name = file_name

        self.residues = []
        self.name = name
        self.residues_index = {}
        for i, residue in enumerate(residues):
            self.residues.append(Residue(residue))
            self.residues_index[residue] = i
        self.biopython_residues = residues
        self.sequence = sequence
        self.atoms = atoms
        self.structure = structure
        self.model = self.structure[0]
        aa_chain_list = set([res.parent for res in residues])
        self.chains = self.model.child_dict
        # self.chains = {chain.id: chain for chain in aa_chain_list}
        # self.model.child_di
        self.polypeptides = CaPPBuilder().build_peptides(structure)

    def compute_diameter(self):
        coordinates = np.ndarray((0, 3))
        for residue in self.residues:
            coordinates = np.vstack((coordinates, residue.get_coordinates()))
        hull = ConvexHull(coordinates)
        boundary = coordinates[hull.vertices]
        diam = np.max(cdist(boundary, boundary))
        return diam

    def get_atoms(self):
        return self.structure[0].get_atoms()

    def get_atoms_coordinates(self):
        coordinates = np.ndarray((0, 3))
        for residue in self.residues:
            coordinates = np.vstack((coordinates, residue.get_coordinates()))
        return coordinates

    def is_ligand(self):
        return "_l_" in self.name

    def get_chains_ids(self):
        chains_ids = []
        for chain in self.structure[0]:
            chains_ids.append(chain.id)
        return chains_ids

    def get_chain(self, chain_id):
        return self.structure[0][chain_id]

    def get_chain_ids(self):
        return self.structure[0].keys()
