import os

from abc import ABCMeta, abstractmethod

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractDataset(AbstractLogger):
    __metaclass__ = ABCMeta

    def __init__(self, configuration):
        super(AbstractDataset, self).__init__()
        self._config = configuration
        if "features_dataset" not in self._config:
            self._config["features_dataset"] = self._config["dataset"]
        data_directory = self._config["reddinng"]["data_directory"]
        self._config["dataset_directory"] = os.path.join(data_directory, self._config["dataset"])
        self._directory = self._config["dataset_directory"]

    @abstractmethod
    def get_complexes(self):
        pass

    @abstractmethod
    def get_object_ids(self):
        pass

    @abstractmethod
    def extract(self, object_ids):
        pass

    def load(self, object_ids=None):
        if object_ids is None:
            object_ids = self.get_object_ids()
        self.extract(object_ids)
