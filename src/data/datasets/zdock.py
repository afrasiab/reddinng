import glob

import copy

import os
import sys

import cPickle
from Bio.PDB import PDBIO
import traceback

from os.path import basename
import subprocess

from src.data.dataobjects.decoy_dockq import DecoyDockQ
from src.data.datasets.abstract_dataset import AbstractDataset
from src.data.pdb.pdb_reader import PDBReader
from src.data.proteins.protein import Protein
from src.data.representation import RepresentationFactories
from src.data.samplers import Samplers
from src.features.feature_extractors.interface_array_extractor import InterfaceArrayExtractor
from src.features.feature_extractors.interface_extractor import InterfaceExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def generate_decoys_scores(args):
    [config, decoy] = args
    import os
    decompressed = False
    decoy_file = None
    try:
        import cPickle
        from os.path import basename, split
        os.chdir(config["reddinng"]["reddinng_home"])

        the_complex = basename(decoy)[:4]
        complex_dir = os.path.join(config["labels_directory"], the_complex)
        label_file = os.path.join(complex_dir, basename(decoy).split(".")[0] + ".lbl.cp")
        pdbs = os.path.join(config["reddinng"]["dbd4_directory"], config["reddinng"]["dbd_cleaned_dir"])
        native_pdb = os.path.join(pdbs, "{}_b.pdb".format(the_complex))
        decompressed = False
        if decoy.endswith(".gz"):
            decoy_file = os.path.join(config["reddinng"]["temp_directory"], basename(decoy[:-3]))
            os.system("gunzip -c {} > {}".format(decoy, decoy_file))
            decompressed = True
        else:
            decoy_file = decoy
        from src.data.metrics.dockq_calculator import DockQCalculator
        dockq, fnat, f_non_nat, irmsd, lrmsd = DockQCalculator(config).get(native_pdb, decoy_file)
        metrics = {
            'dockq': dockq,
            'fnat': fnat,
            'f_non_nat': f_non_nat,
            'lrmsd': lrmsd,
            'irmsd': irmsd,
        }
        cPickle.dump(metrics, open(label_file, "w"))
        if decompressed and os.path.exists(decoy_file):
            os.remove(decoy_file)
        return "successful"
    except Exception:
        if decompressed and os.path.exists(decoy_file):
            os.remove(decoy_file)
        traceback.print_exc(file=sys.stdout)
        return traceback.format_exc()


def generate_decoy_pdbs(args):
    """"
    worker function for ZDock.extract_vertex_features and ZDock.extract_edge_features we need to keep this
    as a function not a method since dispy only recognizes functions as tasks!
    """
    try:
        conf, the_complex = args
        import os
        import glob
        from os.path import basename
        os.chdir(conf["reddinng"]["reddinng_home"])
        from src.data import OutputRegex

        regex = conf["reddinng"][OutputRegex[conf["dataset"]]]
        z_out = conf["reddinng"]["zdock_output_directory"]
        zdock_output = os.path.join(conf["dataset_directory"], z_out, regex.format(the_complex))
        pdbs_directory = os.path.join(conf["dataset_directory"], conf["reddinng"]["zdock_natives_directory"])
        os.chdir(pdbs_directory)
        decoys_dir = os.path.join(conf["dataset_directory"], conf["reddinng"]["zdock_pdb_decoys_directory"])
        # number_of_decoys = 1
        number_of_decoys = sum(1 for _ in open(zdock_output)) - 5
        last_decoy = os.path.join(decoys_dir, the_complex, "{}_decoy_{}.pdb.gz".format(the_complex, number_of_decoys))
        if not os.path.exists(last_decoy):
            subprocess.check_call(["{}/create.pl".format(pdbs_directory), zdock_output, str(number_of_decoys)])
            os.system('find {}/{}/ -type f -exec gzip -q {{}} + '.format(decoys_dir, the_complex))
        decoy_files = []
        for i in range(number_of_decoys):
            decoy_files.append(os.path.join(decoys_dir, the_complex, "{}_decoy_{}.pdb.gz".format(the_complex, i + 1)))
        return decoy_files
    except Exception:
        traceback.print_exc(file=sys.stdout)
        return traceback.format_exc()


def extract_features(args):
    """"
    worker function for ZDock._generate_decoys we need to keep this as a function not a method since dispy
    only recognizes functions as tasks!
    """
    try:
        import os
        object_id, conf, feature_type = args
        os.chdir(conf["reddinng"]["reddinng_home"])
        from src.data.pdb.pdb_reader import PDBReader
        from src.features import FeatureExtractors
        from src.data.proteins.protein import Protein
        protein = Protein(*PDBReader().read(object_id))
        configs = copy.copy(conf)
        for feature in conf["features"]:
            if isinstance(feature, dict):
                params = feature.values()[0]
                feature = feature.keys()[0]
                for param in params:
                    configs[param] = params[param]
            extractor, _, f_type = FeatureExtractors[feature]
            if f_type == feature_type:
                extractor(configs).extract(protein)
        return "successful"
    except Exception:
        traceback.print_exc(file=sys.stdout)
        return traceback.format_exc()


class ZDock(AbstractDataset):
    def __init__(self, configuration):
        super(ZDock, self).__init__(configuration)
        labels = self._config["reddinng"]["zdock_labels_directory"]
        self._config["labels_directory"] = os.path.join(self._config["dataset_directory"], labels)
        pdbs = self._config["reddinng"]["zdock_natives_directory"]
        self.pdbs_directory = os.path.join(self._config["dataset_directory"], pdbs)
        self._config["vertex_features_folder"] = self.pdbs_directory

    def get_object_ids(self, the_complex=""):
        object_ids = {}
        complexes = self.get_complexes()
        for a_complex in complexes:
            object_ids[a_complex] = 0.05
        return object_ids

    def extract_vertex_features(self, proteins):
        if "features" not in self._config:
            message = "Features are not specified in the configuration file"
            self._logger.error(message)
            raise ValueError(message)
        # doing profile features in parallel will bring the network down
        # so i am doing vertex feature extraction sequentially
        # if "WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX" in self._config["features"]:
        if self._config["parallel"]:
            import dispy
            import dispy.httpd
            cluster = dispy.JobCluster(extract_features, loglevel=dispy.logger.debug)
            http_server = dispy.httpd.DispyHTTPServer(cluster)
            jobs = []
            for protein in proteins:
                self._logger.info("extracting vertex features for {}".format(basename(protein)))
                job = cluster.submit([protein, self._config, "vertex"])
                job.id = protein
                jobs.append(job)
            for job in jobs:
                r = job()
                if isinstance(r, str) and r != "successful":
                    self._logger.error(r)
            cluster.wait()
            http_server.shutdown()
            cluster.close()
        else:
            for protein in proteins:
                self._logger.info("extracting vertex features for {}".format(basename(protein)))
                r = extract_features([protein, self._config, "vertex"])
                if isinstance(r, str) and r != "successful":
                    self._logger.error(r)

    def extract_edge_features(self, decoys):
        if "features" not in self._config:
            message = "Features are not specified in the configuration file"
            self._logger.error(message)
            raise ValueError(message)
        if self._config["parallel"]:
            import dispy
            import dispy.httpd
            cluster = dispy.JobCluster(extract_features, loglevel=dispy.logger.debug)
            http_server = dispy.httpd.DispyHTTPServer(cluster)
            jobs = []
            for a_complex in decoys:
                self._logger.info("extracting edge features for {}".format(a_complex))
                for decoy in decoys[a_complex]:
                    job = cluster.submit([decoy, self._config, "edge"])
                    job.id = decoy
                    jobs.append(job)
                for job in jobs:
                    r = job()
                    if isinstance(r, str) and r != "successful":
                        self._logger.error(r)
            cluster.wait()
            http_server.shutdown()
            cluster.close()
        else:
            for a_complex in decoys:
                self._logger.info("extracting edge features for {}".format(a_complex))
                for decoy in decoys[a_complex]:
                    r = extract_features([decoy, self._config, "edge"])
                    if isinstance(r, str) and r != "successful":
                        self._logger.error(r)

    def extract(self, object_ids):
        decoys_pdbs = self._generate_decoy_pdbs(object_ids)
        # self._generate_labels(decoys_pdbs)
        # complexes = object_ids.keys()
        # self.extract_vertex_features(self._get_proteins(complexes))
        return decoys_pdbs

    def _get_label_files(self, decoy_pdbs):
        label_files = {}
        for a_complex in decoy_pdbs:
            label_files[a_complex] = []
            complex_dir = os.path.join(self._config["labels_directory"], a_complex)
            if not os.path.exists(complex_dir):
                os.makedirs(complex_dir)
            for decoy_pdb in decoy_pdbs[a_complex]:
                label_file = os.path.join(complex_dir, basename(decoy_pdb).split(".")[0] + ".lbl.cp")
                label_files[a_complex].append(label_file)
        return label_files

    def _generate_decoy_pdbs(self, object_ids):
        decoys = {}
        if isinstance(object_ids, dict):
            if self._config["parallel"]:
                import dispy
                import dispy.httpd
                cluster = dispy.JobCluster(generate_decoy_pdbs, loglevel=dispy.logger.debug)
                http_server = dispy.httpd.DispyHTTPServer(cluster)
                jobs = []
                for a_complex in object_ids:
                    self._logger.info("generating decoys for {} if necessary".format(a_complex))
                    job = cluster.submit([self._config, a_complex])
                    job.id = a_complex
                    jobs.append(job)
                cluster.wait()
                for job in jobs:
                    r = job()
                    if isinstance(r, list):
                        decoys[job.id] = r
                    else:
                        self._logger.error(r)
                http_server.shutdown()
                cluster.close()
            else:
                for a_complex in object_ids:
                    self._logger.info("generating decoys for {} if necessary".format(a_complex))
                    decoys[a_complex] = generate_decoy_pdbs([self._config, a_complex])
        return decoys

    def load(self, object_ids=None):
        """
        Gets a dictionary of complexes and the ratio of decoys to sample from the pool of decoys for that complex
         and outputs the the representation specified in the configuration.

        :param object_ids: if not provided it will output first 5% decoys of all the complexes in the dataset.
        complex_code->ratio will output top n decoys of that complex specified by the ratio in the experiment\'s
        configuration file.
        :return:
        The representation of the objects by the structure of the object_ids. if object_ids is none it will return
        first 5% decoys of all the complexes in a dictionary of complex code to decoy representation.
        """
        if object_ids is None:
            object_ids = self.get_object_ids()
        decoys_pdbs = self.extract(object_ids)
        sampling_conf = self._config["sampling"]
        if sampling_conf["sampler"] is not None:
            label_files = self._get_label_files(decoys_pdbs)
            decoys = {}
            for a_complex in decoys_pdbs:
                decoys[a_complex] = []
                for decoys_pdb, label in zip(decoys_pdbs[a_complex], label_files[a_complex]):
                    decoys[a_complex].append(DecoyDockQ(decoys_pdb, label))
            conf = copy.copy(sampling_conf)
            conf["seed"] = self._config["seed"]
            conf["sampling_dir"] = os.path.join(self._directory, self._config["reddinng"]["sampling_directory"])
            sampler = Samplers[sampling_conf["sampler"]](conf)
            sampled_decoys = {}
            for a_complex in decoys:
                sampled_decoys[a_complex] = sampler.sample(a_complex, decoys[a_complex])
            decoys = sampled_decoys
        else:
            decoys = decoys_pdbs
        representation_factory = RepresentationFactories[self._config["representation"]["type"]](self._config)
        return representation_factory.load(decoys)

    def get_complexes(self):
        zdock_decoys_dir = self._config["reddinng"]["zdock_output_directory"]
        results_directory = os.path.join(self._config["dataset_directory"], zdock_decoys_dir)
        complexes = {basename(a_file)[:4] for a_file in glob.glob(os.path.join(results_directory, "*.out"))}
        problematic_complexes = set(self._config["reddinng"]["problematic_complexes"])
        complexes = complexes.difference(problematic_complexes)
        return list(complexes)

    def _get_proteins(self, complexes):
        proteins = []
        benchmark4_pdbs = os.path.join(self._config["reddinng"]["dbd4_directory"],
                                       self._config["reddinng"]["dbd_cleaned_dir"])
        for the_complex in complexes:
            proteins.extend(glob.glob(os.path.join(benchmark4_pdbs, "{}_?_u*".format(the_complex))))
        return proteins

    def _generate_labels(self, decoys):
        rd = self._config["reddinng"]
        if self._config["parallel"]:
            import dispy
            import dispy.httpd
            dispy.MsgTimeout = 30
            cluster = dispy.JobCluster(generate_decoys_scores)
            http_server = dispy.httpd.DispyHTTPServer(cluster)
            jobs = []
            for a_complex in decoys:
                self._logger.info("Computing scores for decoys of {}".format(a_complex))
                complex_dir = os.path.join(self._config["labels_directory"], a_complex)
                if not os.path.exists(complex_dir):
                    os.makedirs(complex_dir)
                native_pdb = os.path.join(rd["dbd4_directory"], rd["dbd_cleaned_dir"], "{}_b.pdb".format(a_complex))
                native = Protein(*PDBReader().read(native_pdb))
                InterfaceExtractor(self._config).extract(native)
                InterfaceArrayExtractor(self._config).extract(native)
                for decoy in decoys[a_complex]:
                    label_file = os.path.join(complex_dir, basename(decoy).split(".")[0] + ".lbl.cp")
                    if not os.path.exists(label_file):
                        job = cluster.submit([self._config, decoy])
                        job.id = decoy
                        jobs.append(job)
                for job in jobs:
                    r = job()
                    if isinstance(r, str) and r != "successful":
                        self._logger.error(r)
            cluster.wait()
            http_server.shutdown()
            cluster.close()
        else:
            for a_complex in decoys:
                self._logger.info("Computing scores for decoys of {}".format(a_complex))
                complex_dir = os.path.join(self._config["labels_directory"], a_complex)
                if not os.path.exists(complex_dir):
                    os.makedirs(complex_dir)
                native_pdb = os.path.join(rd["dbd4_directory"], rd["dbd_cleaned_dir"], "{}_b.pdb".format(a_complex))
                native = Protein(*PDBReader().read(native_pdb))
                InterfaceExtractor(self._config).extract(native)
                InterfaceArrayExtractor(self._config).extract(native)
                for decoy in decoys[a_complex]:
                    label_file = os.path.join(complex_dir, basename(decoy).split(".")[0] + ".lbl.cp")
                    if not os.path.exists(label_file):
                        r = generate_decoys_scores([self._config, decoy])
                        if isinstance(r, str) and r != "successful":
                            self._logger.error(r)
