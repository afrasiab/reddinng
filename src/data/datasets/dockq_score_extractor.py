import glob
import os
import warnings
from Bio.PDB import PDBParser
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from os.path import basename
import subprocess
import cPickle as pickle

from configuration import data_directory, zdock_irad_3_0_2, zdock_pdb_decoys_directory, zdock_natives_directory, \
    reddinng_home

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def get_chains(natives_directory, complex_code):
    ligand_pdb = os.path.join(natives_directory, complex_code + "_l_u.pdb.ms")
    receptor_pdb = os.path.join(natives_directory, complex_code + "_r_u.pdb.ms")
    chains = []
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", PDBConstructionWarning)
        ligand = PDBParser().get_structure("ligand", ligand_pdb)[0]
        receptor = PDBParser().get_structure("receptor", receptor_pdb)[0]
        for protein in [ligand, receptor]:
            chains.append(protein.child_dict.keys())
    return ' '.join(chains[0]), ' '.join(chains[1])


def generate_decoy_scores(args):
    [decoy, native_pdb, ligand_chains, receptor_chains] = args

    label_file = decoy[:-4] + ".labels.cp"
    if os.path.exists(label_file):
        return "label file already exists: {}".format(label_file)
    result = subprocess.check_output("python /s/jawar/j/nobackup/basir/reddinng/data/tools/dockq/DockQ.py {} {}"
                                     " -native_chain1 {} -model_chain1 {} "
                                     "-native_chain2 {} -model_chain2 {} "
                                     "-short".format(native_pdb,
                                                     decoy,
                                                     ligand_chains,
                                                     ligand_chains,
                                                     receptor_chains,
                                                     receptor_chains), shell=True, stderr=subprocess.STDOUT)
    parts = result.split()
    metrics = {
        'dockq': float(parts[1]),
        'fnat': float(parts[3]),
        'lrmsd': float(parts[5]),
        'irmsd': float(parts[7]),
    }
    pickle.dump(metrics, open(label_file, "w"))
    return result


def main():
    import dispy
    cluster = dispy.JobCluster(generate_decoy_scores)
    decoys_directory = os.path.join(data_directory, zdock_irad_3_0_2, zdock_pdb_decoys_directory)
    natives_directory = os.path.join(data_directory, zdock_irad_3_0_2, zdock_natives_directory)
    native_pdbs = glob.glob(natives_directory + "/????_b.pdb.ms")
    native_pdbs.sort()
    os.chdir("{}data/tools/dockq/".format(reddinng_home))
    for native_pdb in native_pdbs:
        complex_code = basename(native_pdb)[:4]
        decoys = os.path.join(decoys_directory, complex_code + ".*.pdb")
        decoys = glob.glob(decoys)
        decoys.sort()
        ligand_chains, receptor_chains = get_chains(natives_directory, complex_code)
        jobs = []
        for decoy in decoys:
            job = cluster.submit([decoy, native_pdb, ligand_chains, receptor_chains])
            job.id = decoy
            jobs.append(job)
        print native_pdb
        # for job in jobs:
        #     print job()
    cluster.print_status()

if __name__ == '__main__':
    main()

