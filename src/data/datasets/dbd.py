import glob
import os
import yaml
from Bio import pairwise2
from Bio.PDB import CaPPBuilder, PDBIO
import copy
import numpy as np
from os.path import basename
from src.data.proteins.protein import Protein

from src.data.pdb.pdb_reader import PDBReader

from src.data.datasets.abstract_dataset import AbstractDataset
from src.data.proteins.protein_complex import ProteinComplex
from src.data.proteins.protein_pair import ProteinPair
import cPickle as cP

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class DBD(AbstractDataset):
    def __init__(self, configuration):
        super(DBD, self).__init__(configuration)
        self.pdbs_directory = os.path.join(self._directory, self._config["reddinng"]["dbd_pdb_directory"])

    def get_complexes(self):
        complexes = set()
        for ligand_pdb in glob.glob(os.path.join(self.pdbs_directory, "*_l_b*")):
            complexes.add(basename(ligand_pdb)[:4])
        complexes = list(complexes)
        complexes.sort()
        return complexes

    def extract(self, object_ids):
        pass

    def get_object_ids(self):
        pass

    def match_bound_unbound_ids(self, complexes=None, assemble_bound=True):
        if complexes is None:
            complexes = self.get_complexes()
        out_dir = os.path.join(self._directory, self._config["reddinng"]["dbd_cleaned_dir"])
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        for a_complex in complexes:
            l_b = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_l_b.pdb")))
            l_u = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_l_u.pdb")))
            r_b = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_r_b.pdb")))
            r_u = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_r_u.pdb")))
            renamed_chains = []
            if len(l_b.chains) != len(l_u.chains) or \
                    len(r_b.chains) != len(r_u.chains) or \
                    len(l_b.chains) > 1 or \
                    len(l_u.chains) > 1 or \
                    len(r_b.chains) > 1 or \
                    len(r_u.chains) > 1:
                continue
            print a_complex

            for b, u, chain_ids in [(l_b, l_u, map(chr, range(65, 91, 1))), (r_b, r_u, map(chr, range(90, 64, -1)))]:
                alignments, scores = self.collect_scores(b, u)
                matched_chains = self.match_chains(scores)
                renamed_chains.extend(self.rename_matched_chains(b, u, alignments, chain_ids, matched_chains))
                # for p in [l_b, l_u, r_b, r_u]:
                #     io = PDBIO()
                #     io.set_structure(p.structure)
                #     io.save(os.path.join(out_dir, basename(p.file_name)+".ms"), write_end=False)
                # if assemble_bound:
                #     file_name = os.path.join(out_dir, basename(l_b.file_name).replace("_l_b", "_b"))
                #     handle = open(file_name, "w")
                #     io = PDBIO()
                #     io.set_structure(l_b.structure)
                #     io.save(handle, write_end=False)
                #     handle.close()
                #     handle = open(file_name, "a")
                #     io = PDBIO()
                #     io.set_structure(r_b.structure)
                #     io.save(handle)
                #     handle.close()
            # self.save_matched_chains(a_complex, renamed_chains)

    def save_matched_chains(self, a_complex, renamed_chains):
        match_dir = os.path.join(self._directory, self._config["reddinng"]["dbd_chain_match_directory"])
        if not os.path.exists(match_dir):
            os.mkdir(match_dir)
        match_file = os.path.join(match_dir, "{}.cp".format(a_complex))
        if not os.path.exists(match_file):
            cP.dump(renamed_chains, open(match_file, "w"))

    @staticmethod
    def collect_scores(b, u):
        alignments = {}
        scores = {}
        ppb = CaPPBuilder()
        b_chains = b.chains.keys()
        u_chains = u.chains.keys()
        b_chains.sort()
        u_chains.sort()
        for i, b_chain in enumerate(b_chains):
            for j, u_chain in enumerate(u_chains):
                b_chain_seq = ''.join([str(p.get_sequence()) for p in ppb.build_peptides(b.chains[b_chain])])
                u_chain_seq = ''.join([str(p.get_sequence()) for p in ppb.build_peptides(u.chains[u_chain])])
                if len(b_chain_seq) > 0 and len(u_chain_seq) > 0:
                    aln = pairwise2.align.localxs(b_chain_seq, u_chain_seq, -1, -0.1, one_alignment_only=True)[0]
                    b_aln, u_aln, score, start, end = aln
                    alignments[b_chain, u_chain] = [b_aln, u_aln, score, start, end]
                    scores[b_chain, u_chain] = score / (end - start)
                else:
                    scores[b_chain, u_chain] = -np.inf
        return alignments, scores

    @staticmethod
    def _get_aln_residue_map(aln_seq, chain):
        aln_res_map = {}
        ppb = CaPPBuilder()
        b_pp = []
        for p in ppb.build_peptides(chain):
            b_pp.extend(p)
        pp_index = 0
        for j, c in enumerate(aln_seq):
            if c != "-":
                aln_res_map[j] = b_pp[pp_index]
                pp_index += 1
        return aln_res_map

    @staticmethod
    def rename_matched_chains(b, u, alignments, chain_ids, matched_chains, rename_residues=True):
        chain_ids_c = copy.copy(chain_ids)
        b_chains = b.chains.keys()
        u_chains = u.chains.keys()
        b_chains.sort()
        u_chains.sort()
        both_chains_ids = set(b_chains).union(set(u_chains))
        usable_temp_ids = list(set(chain_ids_c[len(matched_chains):]).difference(both_chains_ids))
        b_chains_map = {}
        u_chains_map = {}
        # assigning fake ids to chains in order to avoid conflict with new chain ids
        for i, (b_c, u_c) in enumerate(matched_chains):
            b_chains_map[b_c] = b.chains[b_c]
            u_chains_map[u_c] = u.chains[u_c]
            b.chains[b_c].id = u.chains[u_c].id = usable_temp_ids[i]
        # removing unmatched chain ids from the pool of available ids
        b_unmatched_chains = copy.copy(b_chains)
        u_unmatched_chains = copy.copy(u_chains)
        for b_c, u_c in matched_chains:
            b_unmatched_chains.remove(b_c)
            u_unmatched_chains.remove(u_c)
        for c in b_unmatched_chains+u_unmatched_chains:
            if c in chain_ids_c:
                chain_ids_c.remove(c)
        # rename the chains and residues ids
        renamed_chains = []
        for i, (b_c, u_c) in enumerate(matched_chains):
            b_chains_map[b_c].id = u_chains_map[u_c].id = chain_ids_c[i]
            renamed_chains.append(chain_ids_c[i])
            if rename_residues:
                aln = alignments[b_c, u_c]
                b_aln_res_map = DBD._get_aln_residue_map(aln[0], b_chains_map[b_c])
                u_aln_res_map = DBD._get_aln_residue_map(aln[1], u_chains_map[u_c])
                b_aln_seq = aln[0]
                u_aln_seq = aln[1]
                aln_length = range(len(u_aln_seq))
                # assign unused ids temporarily in order to avoid conflict with new ids
                for chain in [b_chains_map[b_c], u_chains_map[u_c]]:
                    current_ids = [r.id[1] for r in chain.child_list]
                    unused_ids = set(range(max(len(b_chains_map[b_c]), len(u_chains_map[u_c])), 10000))
                    available_ids = list(unused_ids.difference(set(current_ids)))
                    for j, res in enumerate(chain.child_list):
                        res_id = res.id
                        res.id = (res_id[0], available_ids[j], res_id[2])
                # match the id of matched residues
                for j in aln_length:
                    if b_aln_seq[j] != '-':
                        res_id = b_aln_res_map[j].id
                        b_aln_res_map[j].id = (res_id[0], j, res_id[2])
                    if u_aln_seq[j] != '-':
                        res_id = u_aln_res_map[j].id
                        u_aln_res_map[j].id = (res_id[0], j, res_id[2])
                # set the id of unmatched residues
                for chain in [b_chains_map[b_c], u_chains_map[u_c]]:
                    j = 0
                    for res in chain.child_list:
                        res_id = res.id
                        if res.id[1] < 0:
                            res.id = (res_id[0], len(aln_length) + j, res_id[2])
                            j += 1
        for p, unmatched_chains in [(b, b_unmatched_chains), (u, u_unmatched_chains)]:
            if ' ' in unmatched_chains:
                cc = copy.copy(chain_ids)
                for c_id in p.chains.keys():
                    if c_id in cc:
                        cc.remove(c_id)
                p.chains[' '].id = cc[0]
        return renamed_chains

    @staticmethod
    def match_chains(scores):
        matches = []
        chain_pairs = scores.keys()
        bound_chains = list({a for (a, b) in chain_pairs})
        unbound_chains = list({b for (a, b) in chain_pairs})
        bound_chains.sort()
        unbound_chains.sort()
        unmatched_bound_chains = copy.copy(bound_chains)
        unmatched_unbound_chains = copy.copy(unbound_chains)
        for bound_chain in bound_chains:
            max_score = -np.inf
            match = None
            if bound_chain not in unmatched_bound_chains:
                continue
            for unbound_chain in unmatched_unbound_chains:
                score = scores[bound_chain, unbound_chain]
                if score > max_score and score > 0.6:
                    match = unbound_chain
                    max_score = score
            if match is not None:
                unmatched_bound_chains.remove(bound_chain)
                unmatched_unbound_chains.remove(match)
                matches.append((bound_chain, match))
        return matches

    def rename_blank_chains(self, merge_bound=True):
        complexes = self.get_complexes()
        renamed_chains_directory = os.path.join(self._directory, self._config["reddinng"]["dbd_renamed_chains_directory"])
        bound_folder = os.path.join(self._directory, self._config["reddinng"]["dbd_bound_folder"])
        if not os.path.exists(bound_folder):
            os.mkdir(bound_folder)
        if not os.path.exists(renamed_chains_directory):
            os.mkdir(renamed_chains_directory)
        for a_complex in complexes:
            print("Renaming blank chains in {} ".format(a_complex))
            l_b = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_l_b.pdb")))
            r_b = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_r_b.pdb")))
            l_u = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_l_u.pdb")))
            r_u = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_r_u.pdb")))
            for l, r in [(l_b, r_b), (l_u, r_u)]:
                l_chain_ids = set(l.chains.keys())
                r_chain_ids = set(r.chains.keys())
                available_ids = list(set(map(chr, range(65, 91, 1))).difference(l_chain_ids.union(r_chain_ids)))
                all_chains = l.chains.values()
                all_chains.extend(r.chains.values())
                for i, chain in enumerate(all_chains):
                    chain.id = available_ids[i]
                for protein in [l,r]:
                    file_name = os.path.join(self._directory, self._config["reddinng"]["dbd_renamed_chains_directory"], basename(protein.file_name))
                    handle = open(file_name, "w")
                    io = PDBIO()
                    io.set_structure(protein.structure)
                    io.save(handle)
                    handle.close()
            if merge_bound:
                file_name = os.path.join(bound_folder, basename(l_b.file_name).replace("_l_b", "_b"))
                handle = open(file_name, "w")
                io = PDBIO()
                io.set_structure(l_b.structure)
                io.save(handle, write_end=False)
                handle.close()
                handle = open(file_name, "a")
                io = PDBIO()
                io.set_structure(r_b.structure)
                io.save(handle)
                handle.close()


    def merge_bound(self):
        complexes = self.get_complexes()
        bound_folder = os.path.join(self._directory, self._config["reddinng"]["dbd_bound_folder"])
        if not os.path.exists(bound_folder):
            os.mkdir(bound_folder)
        for a_complex in complexes:
            print("Merging {} ligand and receptor in the bound formation into a single file".format(a_complex))
            l_b = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_l_b.pdb")))
            r_b = Protein(*PDBReader().read(os.path.join(self.pdbs_directory, a_complex + "_r_b.pdb")))
            file_name = os.path.join(bound_folder, basename(l_b.file_name).replace("_l_b", "_b") + ".ms")
            handle = open(file_name, "w")
            io = PDBIO()
            io.set_structure(l_b.structure)
            io.save(handle, write_end=False)
            handle.close()
            handle = open(file_name, "a")
            io = PDBIO()
            io.set_structure(r_b.structure)
            io.save(handle)
            handle.close()


def main():
    config_file = os.path.join(os.getenv("RD_CONF"), "reddinng.yaml")
    redd_conf = yaml.load(open(config_file))
    config = {"reddinng": redd_conf, "dataset": "benchmark4"}
    DBD(config).match_bound_unbound_ids()
    # DBD(config).rename_blank_chains()


if __name__ == '__main__':
    main()
