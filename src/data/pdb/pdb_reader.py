import gzip
import os
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from os.path import splitext, basename
from Bio.PDB import PDBParser, Selection, PPBuilder, CaPPBuilder
import warnings

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class PDBReader(AbstractLogger):
    def __init__(self):
        super(PDBReader, self).__init__()

    def read(self, pdb_file_path, name=None):
        """
        Extract info from a PDB file
            file_name: path of pdb file
            name: name of the structure (default name of the file without extension)
            return:: (structure,R,polypeptides,sequence,seq_res_dict)

                structure: structure object
                residues: list of residues
                polypeptides: list of polypeptides in the structure
                sequence: combined sequence (for all polypeptides)
                seq_res_dict: Sequence to residues mapping index list, sequence[i] corresponds to
                    residues[seq_res_dict[i]]
        """
        if pdb_file_path.endswith(".gz"):
            f = gzip.open(pdb_file_path, 'rb')
        else:
            f = open(pdb_file_path)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", PDBConstructionWarning)
            if name is None:
                name = splitext(basename(pdb_file_path))[0]

            structure = PDBParser().get_structure(name, f)
            if len(structure) > 1:
                message = "more than one MODEL present, taking first entry"
                self._logger.error(message)
                raise RuntimeError(message)
            elif len(structure) == 0:
                message = "Zero structures found in " + name
                self._logger.error(message)
                raise ValueError(message)

            atoms = Selection.unfold_entities(structure, 'A')
            polypeptides = CaPPBuilder().build_peptides(structure)
            sequence = ''.join([str(p.get_sequence()) for p in polypeptides])
            residues = [residue for polypeptide in polypeptides for residue in polypeptide]
            protein_name = os.path.basename(pdb_file_path).split(".")[0]
            return pdb_file_path, protein_name, structure, residues, sequence, atoms
