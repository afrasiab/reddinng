from os.path import basename

import yaml

#from src.utils.abstract_logger import AbstractLogger
import matplotlib.pyplot as plt
import numpy as np


class ZDockAnalyzer:
    def __init__(self):
        super(ZDockAnalyzer, self).__init__()

    def get_labels_distribution(self, dataset):
        # self._logger.info("Computing the distribution of the labels")
        labels = []
        for a_complex in dataset:
            #self._logger.info(a_complex)
            for representation in dataset[a_complex]:
                labels.append(representation.label())
        # plt.hist(labels)
        # self._logger.info(np.histogram(labels))
        # plt.show()


def main():
    import glob
    import cPickle
    import sys
    bins = np.linspace(0, 1, 11)

    # for i in range(100):
    #     if i < 99:
    #         sys.stdout.write("[{} {}], ".format(bins[i], bins[i+1]))
    #     else:
    #         sys.stdout.write("[{} {}]\n".format(bins[i], bins[i+1]))
    sys.stdout.flush()
    max_values = []
    for i, c_f in enumerate(glob.glob("/s/jawar/k/nobackup/basir/data/decoys_bm4_zd3.0.2_6deg/labels/*")):
        scores = []
        # raw_values = []
        the_complex = basename(c_f)
        max_decoy = ""
        max_value = -np.inf
        # print "computing scores for {}".format(the_complex)
        for f in glob.glob("/s/jawar/k/nobackup/basir/data/decoys_bm4_zd3.0.2_6deg/labels/{}/*".format(the_complex)):
            score = cPickle.load(open(f))
            irmsd = 1 / (1 + (score["irmsd"] / 6) ** 2)
            lrmsd = 1 / (1 + (score["lrmsd"] / 16) ** 2)
            fnat = score["fnat"]
            dockq = np.mean([irmsd, lrmsd, fnat])
            # raw_values.append(irmsd)
            scores.append(dockq)
            if dockq > max_value:
                max_value = dockq
                max_decoy = f
            # scores.append(1 / (1 + (irmsd / 18.5) ** 2))
        histogram = np.histogram(scores, bins=bins)
        hist = ""
        total = float(np.sum(histogram[0]))
        for count in histogram[0]:
            hist += "{:.5f}, ".format(count / total)
        print "{}, {}".format(the_complex, hist[:-2])
        print "{} - {}".format(max_value, basename(max_decoy))
        max_values.append(max_value)

    print np.mean(max_values)
    print np.max(max_values)
    print np.min(max_values)
    print np.std(max_values)


if __name__ == '__main__':
    main()
