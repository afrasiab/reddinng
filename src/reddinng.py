import sys
import time
import os
import yaml

from src.experiment import ExperimentDict
from src.utils.abstract_logger import AbstractLogger
from src.utils.dict_utils import DictionaryUtils

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Reddinng(AbstractLogger):
    experiments = []

    def __init__(self):
        super(Reddinng, self).__init__()
        self._validate_environment_variables()
        self._read_configuration_yaml()
        self._setup_environment_variables()
        self._setup()
        self._run_experiments()
        self._clean_up()

    def _setup(self):
        self._create_experiment_dirs()
        self._build_experiment_dicts()

    def _create_experiment_dirs(self):
        if not os.path.exists(self._configuration["temp_directory"]):
            os.makedirs(os.path.exists(self._configuration["temp_directory"]))
        output_dir = os.path.join(self._configuration["output_directory"], sys.argv[1].split(".")[0])
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        self.this_run_dir = os.path.join(output_dir, time.strftime("%Y%m%d-%H%M%S"))
        if not os.path.exists(self.this_run_dir):
            os.mkdir(self.this_run_dir)

    def _run_experiments(self):
        for experiment_conf in self.experiments:
            experiment_conf["reddinng"] = self._configuration
            experiment = ExperimentDict[experiment_conf["type"]](experiment_conf)
            experiment.run()
            return "{} Done!".format(experiment_conf["name"])

    def _clean_up(self):
        pass

    def _build_experiment_dicts(self):
        experiment_file = os.path.join(self._configuration["experiment_directory"], sys.argv[1])
        self.experiments_conf = yaml.load(open(experiment_file))
        if "parallel" in self.experiments_conf:
            DictionaryUtils.get_node(self.experiments_conf, "data")["parallel"] = self.experiments_conf["parallel"]
        for experiment_conf in self.experiments_conf["experiments"]:
            self.experiments.append(DictionaryUtils.merge_dict(self.experiments_conf["common"], experiment_conf))
            self.experiments[-1]["output"] = self.this_run_dir

    def _read_configuration_yaml(self):
        reddinng_home = os.path.dirname(os.path.dirname(__file__)) + "/"
        config_file = os.path.join(os.getenv("RD_CONF"), "reddinng.yaml")
        if not os.path.exists(config_file):
            raise RuntimeError("Could not find the config file in {}".format(os.getenv("RD_CONF")))
        self._configuration = yaml.load(open(config_file))
        # todo: use yamllint for validation
        self._configuration["reddinng_home"] = reddinng_home

    def _run_experiment(self, experiment_conf):
        pass

    @staticmethod
    def _validate_environment_variables():
        reddinng_home = os.path.dirname(os.path.dirname(__file__)) + "/"
        variables = [("RD_CONF", "configs"),
                     ("RD_DATA", "../data"),
                     ("RD_OUT", "outputs"),
                     ("RD_EXPERIMENTS", "experiments"),
                     ("RD_TOOLS", "../tools"),
                     ("RD_VENV", "/home/basir/virtualenvs/reddinng"),
                     ("RD_TEMP", "temp")]
        for variable, default_folder in variables:
            if os.getenv(variable) is None:
                if variable != "RD_VENV":
                    default_path = os.path.join(reddinng_home, default_folder)
                else:
                    default_path = default_folder
                os.environ[variable] = default_path
                if not os.path.exists(default_path):
                    message = "Please set all the following environment " \
                              "variables to a valid path before running or " \
                              "keep the default folder structure. Consult the " \
                              "README file for meaning of each variable" \
                              "and how they must be set:\n{}".format("\t\n".join(variables))
                    print(message)
                    raise RuntimeError()

    def _setup_environment_variables(self):
        os.environ["PATH"] = "{}:{}".format(os.getenv("PATH"), self._configuration["msms_home"])
        os.environ["PATH"] = "{}:{}".format(os.getenv("PATH"), self._configuration["stride_home"])
        os.environ["PATH"] = "{}:{}".format(os.getenv("PATH"), self._configuration["dssp_home"])
        os.environ["PATH"] = "{}:{}".format(os.getenv("PATH"), self._configuration["psaia_home"])
        os.environ["PATH"] = "{}:{}".format(os.getenv("PATH"), self._configuration["zdock_pdbs_scripts_directory"])


def main():
    Reddinng()


if __name__ == '__main__':
    main()
