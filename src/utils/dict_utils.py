import copy

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class DictionaryUtils:
    def __init__(self):
        pass

    @staticmethod
    def get_node(dictionary, query_key):
        if query_key in dictionary:
            return dictionary[query_key]
        else:
            for key in dictionary:
                if isinstance(dictionary[key], dict):
                    value = DictionaryUtils.get_node(dictionary[key], query_key)
                    if value is not None:
                        return value
                    else:
                        continue
        return None

    @staticmethod
    def merge_dict(dict1, dict2):
        """ recursively merges two dictionaries"""
        new_dict = copy.copy(dict2)
        for key in dict1:
            if key in dict2:
                # merge the two values
                if type(dict1[key]) != type(dict2[key]):
                    raise TypeError
                else:
                    if type(dict1[key]) is list:
                        new_dict[key].extend(dict1[key])
                    elif type(dict1[key]) is dict:
                        new_dict[key] = DictionaryUtils.merge_dict(dict1[key], dict2[key])
            else:
                new_dict[key] = dict1[key]
        return new_dict
