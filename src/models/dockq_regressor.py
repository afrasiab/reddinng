import sklearn

from src.models.abstract_tensorflow_model import AbstractTensorFlowModel
import numpy as np
import tensorflow as tf

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class DockQRegressor(AbstractTensorFlowModel):
    def __init__(self, conf, inputs, labels, outputs):
        super(DockQRegressor, self).__init__(conf, inputs, labels, outputs)
        self.vertex, self.edges = self.inputs
        self.train_op = self._get_train_op()

    def train(self, dataset):
        v, e, lbl = dataset
        _, losses, predictions = self.sess.run(
            [self.train_op, self.loss, self.outputs],
            feed_dict=
            {
                self.vertex: v,
                self.edges: e,
                self.labels: lbl
            }
        )
        self._logger.info("loss ={}".format(losses))
        self.losses.append(losses)
        self.predictions.append(predictions)

    def predict(self, dataset):
        p = {}
        for a_complex in dataset:
            p[a_complex] = []
            for a_decoy in dataset[a_complex]:
                v, e, lbl = a_decoy.get()
                lbl = np.array(lbl).reshape((-1, 1))
                _, predictions = self.sess.run(
                    [self.train_op, self.outputs],
                    feed_dict=
                    {
                        self.vertex: v,
                        self.edges: e,
                        self.labels: lbl
                    }
                )
                self._logger.info("label ={}, prediction={}".format(lbl, predictions))
                p[a_complex].append(predictions[0][0])
