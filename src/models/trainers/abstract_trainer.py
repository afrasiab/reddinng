from abc import ABCMeta, abstractmethod

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractTrainer(AbstractLogger):
    __metaclass__ = ABCMeta

    def __init__(self, configuration):
        super(AbstractTrainer, self).__init__()
        self._configuration = configuration
        self.epochs = self._configuration["num_epochs"]

    @abstractmethod
    def train(self, model, training_data, validation_data):
        pass
