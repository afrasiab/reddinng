from tensorflow.python.training.adam import AdamOptimizer
from tensorflow.python.training.gradient_descent import GradientDescentOptimizer
from tensorflow.python.training.momentum import MomentumOptimizer
from tensorflow.python.training.proximal_adagrad import ProximalAdagradOptimizer
from tensorflow.python.training.rmsprop import RMSPropOptimizer

__author__ = 'basir shariat (basir@rams.colostate.edu)'


Optimizers = {
    "sgd": GradientDescentOptimizer,
    "tf_rmsprop": RMSPropOptimizer,
    "tf_adam": AdamOptimizer,
    "tf_prox_adagrad": ProximalAdagradOptimizer,
    "tf_momentum": MomentumOptimizer,
}
