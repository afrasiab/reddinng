from src.models.trainers.abstract_trainer import AbstractTrainer
import numpy as np

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class BatchTrainer(AbstractTrainer):

    def __init__(self, configuration):
        super(BatchTrainer, self).__init__(configuration)
        self.batch_size = self._configuration["minibatch_size"]

    def train(self, model, training_data, validation_data):
        for e in range(self.epochs):
            self._logger.info("epoch {} ".format(e))

            batch_v = []
            batch_e = []
            batch_lbl = []
            for a_complex in training_data:
                self._logger.info("feeding {} decoys of {} ".format(len(training_data[a_complex]), a_complex))
                for an_object in training_data[a_complex]:
                    v, e, lbl = an_object.get()
                    batch_v.append(v)
                    batch_e.append(e)
                    batch_lbl.append(lbl)
                    # if batch size is correct
                    if len(batch_v) == self.batch_size:
                        # padding the batches with zeros to the size of the biggest graph in the batch
                        max_len = np.max([v.shape[0] for v in batch_v])
                        padded_v_batch = []
                        for vv in batch_v:
                            m = max_len - vv.shape[0]
                            pad = ((0, m), (0, 0))
                            values = ((0, 0), (0, 0))
                            padded_v_batch.append(np.lib.pad(vv, pad, 'constant', constant_values=values))
                        padded_e_batch = []
                        for ee in batch_e:
                            m = max_len - ee.shape[0]
                            pad = ((0, m), (0, m), (0, 0))
                            values = ((0, 0), (0, 0), (0, 0))
                            padded_e_batch.append(np.lib.pad(ee, pad, 'constant', constant_values=values))
                        batch = [np.dstack(padded_v_batch),
                                 np.stack(padded_e_batch, axis=3),
                                 np.hstack(batch_lbl).reshape(-1, 1)]
                        model.train(batch)
                        # model.predict(data)
                        del batch_v[:]
                        del batch_e[:]
                        del batch_lbl[:]
                        del padded_v_batch[:]
                        del padded_e_batch[:]
