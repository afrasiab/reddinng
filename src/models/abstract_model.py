from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractModel(AbstractLogger):
    def __init__(self):
        super(AbstractModel, self).__init__()
        self.losses = []
        self.train_loss_history = []
        self.predictions = []
