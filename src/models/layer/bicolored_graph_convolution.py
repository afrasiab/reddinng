from src.models.layer.abstract_layer import AbstractLayer
import tensorflow as tf

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class BicoloredGraphConvolution(AbstractLayer):
    """
        Inputs to this layer are two matrices V and E.
        V is a n x (1+d_v) matrix where the first feature is the
        color of each vertex and the rest are the features assigned to each vertex.
        E is a n x n x (1+d_e) matrix where the first feature is the
        adjacency matrix and the rest are the features assigned to each edge.
        Adjacency matrix should be zeros and ones.
    """

    def __init__(self, inputs, configuration):
        super(BicoloredGraphConvolution, self).__init__(inputs, configuration)

        vertices, edges = self._inputs  #
        inner_mask, outer_mask = tf.expand_dims(edges[:, :, 0], 2), tf.expand_dims(edges[:, :, 1], 2)
        v = vertices[:, :-1]  # n x d_v
        e = edges[:, :, 2:]  # n x n x d_e
        nonlin = self._configuration["nonlin"] if "nonlin" in self._configuration else "relu"
        nonlin_args = self._configuration["nonlin_args"] if "nonlin_args" in self._configuration else None
        if nonlin_args is not None:
            nonlin, nonlin_params = self.nonlinearity(nonlin, **nonlin_args)
        else:
            nonlin, nonlin_params = self.nonlinearity(nonlin)

        [w_v_c, b_v_c, w_v_n, b_v_n, w_e_i, b_e_i, w_e_o, b_e_o, w_e_e, b_e_e, w_e_v, b_e_v] = self._get_parameters()
        f = self._configuration["args"]["filters"]
        self.next_layer_conf["d_v"] = f
        v_c_a = nonlin(tf.matmul(v, w_v_c) + b_v_c)  # n x d_v . d_v x f -> n x f
        v_n_a = nonlin(tf.matmul(v, w_v_n) + b_v_n)  # n x d_v . d_v x f -> n x f
        v_e_a = nonlin(tf.matmul(v, w_e_v) + b_e_v)  # n x d_v . d_v x f -> n x f
        v_c_n = tf.einsum('ik,jk->ijk', v_c_a, v_n_a)  # n x n x f
        e_i = nonlin(tf.tensordot(e, w_e_i, axes=[[2], [0]])+b_e_i)  # n x n x f
        e_o = nonlin(tf.tensordot(e, w_e_o, axes=[[2], [0]])+b_e_o)  # n x n x f
        a_i = tf.multiply(v_c_n, e_i)  # n x n x f
        a_o = tf.multiply(v_c_n, e_o)  # n x n x f
        self.outputs = tf.reduce_mean(tf.multiply(a_i, inner_mask) + tf.multiply(a_o, outer_mask), axis=[1]), edges

    def _get_parameters(self):
        vertices, edges = self._inputs
        n_f = self._configuration["args"]["filters"]
        trainable = self._configuration["trainable"] if "trainable" in self._configuration else True
        w_init = self._configuration["w_init"] if "w_init" in self._configuration else "xavier_uniform"
        b_init = self._configuration["b_init"] if "b_init" in self._configuration else "one"
        d_v = vertices.get_shape()[1].value - 1 if 'd_v' not in self._configuration else self._configuration["d_v"] - 1
        d_e = edges.get_shape()[2].value - 2 if 'd_e' not in self._configuration else self._configuration["d_e"] - 2

        if "params" not in self._configuration:
            parameters = {
                "w_v_c": tf.Variable(self.initializer(w_init, (d_v, n_f)), name="w_v_c", trainable=trainable),
                "w_v_n": tf.Variable(self.initializer(w_init, (d_v, n_f)), name="w_v_n", trainable=trainable),
                "b_v_c": tf.Variable(self.initializer(b_init, (n_f,)),     name="b_v_c", trainable=trainable),
                "b_v_n": tf.Variable(self.initializer(b_init, (n_f,)),     name="b_v_n", trainable=trainable),

                "w_e_i": tf.Variable(self.initializer(w_init, (d_e, n_f)), name="w_e_i", trainable=trainable),
                "w_e_o": tf.Variable(self.initializer(w_init, (d_e, n_f)), name="w_e_o", trainable=trainable),
                "b_e_i": tf.Variable(self.initializer(b_init, (n_f,)),     name="b_e_i", trainable=trainable),
                "b_e_o": tf.Variable(self.initializer(b_init, (n_f,)),     name="b_e_o", trainable=trainable),

                "w_e_e": tf.Variable(self.initializer(w_init, (d_v, n_f)), name="w_e_e", trainable=trainable),
                "b_e_e": tf.Variable(self.initializer(b_init, (n_f)),      name="b_e_e", trainable=trainable),
                "w_e_v": tf.Variable(self.initializer(w_init, (d_e, n_f)), name="w_e_v", trainable=trainable),
                "b_e_v": tf.Variable(self.initializer(b_init, (n_f)),      name="b_e_v", trainable=trainable)
            }
        else:
            parameters = self._configuration["params"]

        parameters = [parameters[p] for p in ["w_v_c", "b_v_c", "w_v_n",
                                              "b_v_n", "w_e_i", "b_e_i",
                                              "w_e_o", "b_e_o", "w_e_e",
                                              "b_e_e", "w_e_v", "b_e_v"]]
        return parameters

