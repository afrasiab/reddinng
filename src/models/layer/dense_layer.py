# from modeling.models.nn_components import initializer
from tensorflow.contrib.layers import xavier_initializer

from src.models.layer.abstract_layer import AbstractLayer
import tensorflow as tf

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class DenseLayer(AbstractLayer):
    def __init__(self, inputs, configuration):
        super(DenseLayer, self).__init__(inputs, configuration)
        trainable = self._configuration["trainable"] if "trainable" in self._configuration else True
        linear = "nonlin" in self._configuration["args"] and self._configuration["args"]["nonlin"] == "linear"
        out_dim = self._configuration["args"]["out_dims"]
        a = None if linear else tf.nn.relu
        self.outputs = tf.layers.dense(inputs,
                                       out_dim,
                                       trainable=trainable,
                                       activation=a,
                                       # kernel_initializer=self,
                                       # bias_initializer=tf.constant_initializer(1.)
                                       )
        pass
