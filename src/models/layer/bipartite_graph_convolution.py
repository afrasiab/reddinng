from src.models.layer.abstract_layer import AbstractLayer
import tensorflow as tf

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class BipartiteGraphConvolution(AbstractLayer):
    """
        Inputs to this layer are two matrices V and E.
        V is a n x (1+d_v) matrix where the first feature is the
        color of each vertex and the rest are the features assigned to each vertex.
        E is a n x n x (1+d_e) matrix where the first feature is the
        adjacency matrix and the rest are the features assigned to each edge.
        Adjacency matrix should be zeros and ones.
    """

    def __init__(self, inputs, configuration):
        super(BipartiteGraphConvolution, self).__init__(inputs, configuration)
        vertices, edges = self._inputs
        # vertices -> n x d_v x batch
        # edges    -> n x n x d_e x batch
        nonlinearity = self._configuration["nonlinearity"] if "nonlinearity" in self._configuration else "relu"
        nonlinearity_args = self._configuration["nonlinearity_args"] if "nonlinearity_args" in self._configuration else None
        if nonlinearity_args is not None:
            nonlinearity, nonlinearity_args = self.nonlinearity(nonlinearity, **nonlinearity_args)
        else:
            nonlinearity, nonlinearity_args = self.nonlinearity(nonlinearity)

        [v_w_v_c, v_b_v_c, v_w_v_n, v_b_v_n, v_w_e, v_b_e, e_w_e, e_b_e, e_w_v, e_b_v] = self._get_parameters()
        f = self._configuration["args"]["filters"]
        self.next_layer_conf["d_v"] = f
        """"computing the vertex activations for the vertex convolution"""
        # n x d_v x b . d_v x f -> n x f x b
        v_c_a = nonlinearity(tf.transpose(tf.tensordot(vertices, v_w_v_c, axes=[[1], [0]]), perm=[0, 2, 1]) + v_b_v_c)
        # n x d_v x b . d_v x f -> n x f x b
        v_n_a = nonlinearity(tf.transpose(tf.tensordot(vertices, v_w_v_n, axes=[[1], [0]]), perm=[0, 2, 1]) + v_b_v_n)
        # n x n x d_e x b . d_e x f -> n x n x f x b
        e_a = nonlinearity(tf.transpose(tf.tensordot(edges,    v_w_e, axes=[[2], [0]]),   perm=[0, 1, 3, 2]) + v_b_e)
        v_c_n = tf.einsum('ifb,jfb->ijfb', v_c_a, v_n_a)  # n x n x f x b
        z_v_v = tf.multiply(v_c_n, e_a)  # n x n x f x b
        z_v = tf.divide(tf.reduce_sum(z_v_v, axis=[1]), tf.count_nonzero(z_v_v, axis=[1]))  # n x f x b
        if "edge_convolution" in self._configuration["args"] and self._configuration["args"]["edge_convolution"]:
            """computing the edge activations for the edge convolution"""
            # n x d_v x b . d_v x f -> n x f x b
            e_v_a = nonlinearity(tf.transpose(tf.tensordot(vertices, e_w_v, axes=[[1], [0]]), perm=[0, 2, 1]) + e_b_v)
            e_v_v = tf.einsum('ifb,jfb->ijfb', e_v_a, e_v_a)  # n x n x f x b
            # n x n x d_e x b . d_e x f -> n x n x f x b
            e_a = nonlinearity(tf.transpose(tf.tensordot(edges,    e_w_e, axes=[[2], [0]]),   perm=[0, 1, 3, 2]) + e_b_e)
            z_e = tf.multiply(e_v_v, e_a)  # n x n x f x b
            self.outputs = z_v, tf.concat((edges, z_e), axis=2)
        else:
            self.outputs = z_v, edges

    def _get_parameters(self):
        vertices, edges = self._inputs
        n_f = self._configuration["args"]["filters"]
        trainable = self._configuration["trainable"] if "trainable" in self._configuration else True
        w_init = self._configuration["w_init"] if "w_init" in self._configuration else "xavier_uniform"
        b_init = self._configuration["b_init"] if "b_init" in self._configuration else "one"
        d_v = vertices.get_shape()[1].value if 'd_v' not in self._configuration else self._configuration["d_v"]
        d_e = edges.get_shape()[2].value if 'd_e' not in self._configuration else self._configuration["d_e"]

        if "params" not in self._configuration:
            parameters = {
                # central vertex weights and bias
                "v_w_v_c": tf.Variable(self.initializer(w_init, (d_v, n_f)), name="v_w_v_c", trainable=trainable),
                "v_b_v_c": tf.Variable(self.initializer(b_init, (n_f, 1)),   name="v_b_v_c", trainable=trainable),
                # neighbors in the other part weights and bias
                "v_w_v_n": tf.Variable(self.initializer(w_init, (d_v, n_f)), name="v_w_v_n", trainable=trainable),
                "v_b_v_n": tf.Variable(self.initializer(b_init, (n_f, 1)),   name="v_b_v_n", trainable=trainable),
                # inter-protein edge weights
                "v_w_e": tf.Variable(self.initializer(w_init, (d_e, n_f)), name="v_w_e", trainable=trainable),
                "v_b_e": tf.Variable(self.initializer(b_init, (n_f, 1)),   name="v_b_e", trainable=trainable),
                # edge convolution weights and biases
                "e_w_e": tf.Variable(self.initializer(w_init, (d_v, n_f)), name="e_w_e", trainable=trainable),
                "e_b_e": tf.Variable(self.initializer(b_init, (n_f, 1)),   name="e_b_e", trainable=trainable),
                "e_w_v": tf.Variable(self.initializer(w_init, (d_e, n_f)), name="e_w_v", trainable=trainable),
                "e_b_v": tf.Variable(self.initializer(b_init, (n_f, 1)),   name="e_b_v", trainable=trainable)
            }
        else:
            parameters = self._configuration["params"]

        parameters = [parameters[p] for p in ["v_w_v_c", "v_b_v_c",
                                              "v_w_v_n", "v_b_v_n",
                                              "v_w_e",   "v_b_e",
                                              "e_w_e",   "e_b_e",
                                              "e_w_v",   "e_b_v"]]
        return parameters

