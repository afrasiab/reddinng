from src.models.layer.abstract_layer import AbstractLayer
import tensorflow as tf

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class TopK(AbstractLayer):
    def __init__(self, inputs, configuration):
        super(TopK, self).__init__(inputs, configuration)
        k = self._configuration["args"]["k"] if "k" in self._configuration["args"] else 1
        self.outputs = tf.reshape(tf.nn.top_k(tf.transpose(inputs), k=k)[0], [1, -1])
