from src.models.layer.abstract_layer import AbstractLayer
import tensorflow as tf
__author__ = 'basir shariat (basir@rams.colostate.edu)'


class GraphMerge(AbstractLayer):
    def __init__(self, inputs, configuration):
        super(GraphMerge, self).__init__(inputs, configuration)
        vertices, edges = self._inputs
        self.outputs = vertices
