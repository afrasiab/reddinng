from src.models.layer.bipartite_graph_convolution import BipartiteGraphConvolution
from src.models.layer.dense_layer import DenseLayer

from src.models.dockq_regressor import DockQRegressor
from src.models.layer.bicolored_graph_convolution import BicoloredGraphConvolution
from src.models.layer.energy_pool import EnergyPool
from src.models.layer.graph_merge import GraphMerge
from src.models.layer.histogram_layer import Histogram
from src.models.layer.top_k import TopK

__author__ = 'basir shariat (basir@rams.colostate.edu)'

Layers = {
    "full_bicolored_convolution": BicoloredGraphConvolution,
    "bipartite_convolution": BipartiteGraphConvolution,
    "histogram": Histogram,
    "graph_merge": GraphMerge,
    "dense": DenseLayer,
    "top_k": TopK,
    "energy_pool": EnergyPool
}

Models = {
    "dockq_regressor": DockQRegressor
}
