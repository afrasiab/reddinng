from src.models.layer.abstract_layer import AbstractLayer
import tensorflow as tf

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Histogram(AbstractLayer):
    def __init__(self, inputs, configuration):
        super(Histogram, self).__init__(inputs, configuration)
        self.bins = self._configuration["args"]["bins"]
        hists = []
        for i in range(inputs.shape[1].value):
            hists.append(self.feature_hist(inputs[:, i]))
        self.outputs = tf.reshape(tf.stack(hists), [1, -1], name="hist_output")

    def feature_hist(self, feature):
        normalized = tf.div(feature - tf.reduce_min(feature), (tf.reduce_max(feature)) - tf.reduce_min(feature))
        hist = tf.histogram_fixed_width(normalized, [-1., 1.], nbins=self.bins, dtype=tf.float32)
        return tf.div(hist, tf.reduce_sum(hist))
