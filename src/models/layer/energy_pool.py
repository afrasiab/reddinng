from src.models.layer.abstract_layer import AbstractLayer
import tensorflow as tf

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class EnergyPool(AbstractLayer):
    """
        Inputs to this layer are two matrices V and E.
        V is a n x (1+d_v) matrix where the first feature is the
        color of each vertex and the rest are the features assigned to each vertex.
        E is a n x n x (1+d_e) matrix where the first feature is the
        adjacency matrix and the rest are the features assigned to each edge.
        Adjacency matrix should be zeros and ones.
    """

    def __init__(self, inputs, configuration):
        super(EnergyPool, self).__init__(inputs, configuration)

        vertices, edges = self._inputs
        nonlinearity = self._configuration["nonlinearity"] if "nonlinearity" in self._configuration else "relu"
        nonlinearity_args = self._configuration[
            "nonlinearity_args"] if "nonlinearity_args" in self._configuration else None
        if nonlinearity_args is not None:
            nonlinearity, nonlinearity_params = self.nonlinearity(nonlinearity, **nonlinearity_args)
        else:
            nonlinearity, nonlinearity_params = self.nonlinearity(nonlinearity)

        [w_v, b_v, w_e, b_e] = self._get_parameters()
        f = self._configuration["args"]["filters"]
        self.next_layer_conf["d_v"] = f
        # n x d_v x b . d_v x f -> n x f x b
        v_a = nonlinearity(tf.transpose(tf.tensordot(vertices, w_v, axes=[[1], [0]]), perm=[0, 2, 1]) + b_v)
        e_v_v = tf.einsum('ifb,jfb->ijfb', v_a, v_a)  # n x n x f x b
        # n x n x d_e x b . d_e x f -> n x n x f x b
        e_a = nonlinearity(tf.transpose(tf.tensordot(edges, w_e, axes=[[2], [0]]), perm=[0, 1, 3, 2]) + b_e)
        z = tf.multiply(e_v_v, e_a)  # n x n x f x b
        self.outputs = tf.transpose(tf.reduce_mean(z, axis=[0, 1]))  # b x f

    def _get_parameters(self):
        vertices, edges = self._inputs
        n_f = self._configuration["args"]["filters"]
        trainable = self._configuration["trainable"] if "trainable" in self._configuration else True
        w_init = self._configuration["w_init"] if "w_init" in self._configuration else "xavier_uniform"
        b_init = self._configuration["b_init"] if "b_init" in self._configuration else "one"
        d_v = vertices.get_shape()[1].value if 'd_v' not in self._configuration else self._configuration["d_v"]
        d_e = edges.get_shape()[2].value if 'd_e' not in self._configuration else self._configuration["d_e"]

        if "params" not in self._configuration:
            parameters = {
                "w_v": tf.Variable(self.initializer(w_init, (d_v, n_f)), name="w_v", trainable=trainable),
                "b_v": tf.Variable(self.initializer(b_init, (n_f, 1)), name="b_v", trainable=trainable),
                "w_e": tf.Variable(self.initializer(w_init, (d_e, n_f)), name="w_e", trainable=trainable),
                "b_e": tf.Variable(self.initializer(b_init, (n_f, 1)), name="b_e", trainable=trainable),
            }
        else:
            parameters = self._configuration["params"]

        parameters = [parameters[p] for p in ["w_v", "b_v",
                                              "w_e", "b_e"]]
        return parameters
