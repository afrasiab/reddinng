from abc import ABCMeta
import numpy as np
import tensorflow as tf
from tensorflow.contrib.layers import xavier_initializer

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractLayer(AbstractLogger):
    __metaclass__ = ABCMeta

    inputs = None
    outputs = None

    def __init__(self, inputs, configuration):
        super(AbstractLayer, self).__init__()
        self._configuration = configuration
        self.next_layer_conf = {}
        self._inputs = inputs

    def initializer(self, init, shape):
        if init == "xavier_uniform":
            a = -np.sqrt(6.)/np.sqrt(np.sum(shape))
            return tf.random_uniform(shape, -a, a)
        elif init == "xavier_normal":
            stddev = np.sqrt(1./np.sum(shape))
            return tf.random_normal(shape, mean=0, stddev=stddev)
        elif init == "xavier_trunc_normal":
            stddev = np.sqrt(1.3/np.sum(shape))
            return tf.truncated_normal(shape, mean=0, stddev=stddev)
        elif init == "tf_xavier_uniform":
            return xavier_initializer(uniform=True)
        elif init == "tf_xavier_normal":
            return xavier_initializer(uniform=False)
        elif init == "one":
            return tf.ones(shape)
        elif init == "zero":
            return tf.zeros(shape)
        elif init == "jon" or init == "he":
            fan_in = np.prod(shape[0:-1])
            std = 1/np.sqrt(fan_in)
            return tf.random_uniform(shape, minval=-std, maxval=std)
        elif init == "pos_jon" or init == "pos_he":
            fan_in = np.prod(shape[0:-1])
            std = 1/np.sqrt(fan_in)
            return tf.random_uniform(shape, minval=0, maxval=2 * std)
        else:
            msg = "unsupported initialization: {}".format(init)
            self._logger.error(msg)
            raise ValueError(msg)

    # noinspection PyTypeChecker
    def nonlinearity(self, nl, trainable=True, **kwargs):
        """ returns a tuple containing a nonlinear function
        and a parameter dictionary of any parameters used """
        if nl == "relu":
            return tf.nn.relu, {}
        elif nl == "lrelu":
            slope = kwargs["slope"]
            assert slope < 1
            return lambda x: tf.maximum(slope * x, x), {}
        elif nl == "global_prelu":
            if "gprelu_slope" in kwargs:
                slope = kwargs["gprelu_slope"]
            else:
                init_slope = kwargs["init_slope"]
                slope = tf.Variable(init_slope, name="global_prelu_slope", trainable=trainable)
            self.tf_summary({"prelu_slope", slope})
            return lambda x: tf.maximum(slope * x, x), {"gprelu_slope": slope}
        elif nl == "prelu":
            init_slope = kwargs["init_slope"]
            slope = tf.Variable(init_slope, name="prelu_slope", trainable=trainable)
            self.tf_summary({"prelu_slope", slope})
            return lambda x: tf.maximum(slope * x, x), {}
        elif nl == "tanh":
            return tf.nn.tanh, {}
        elif nl == "linear" or nl == "none":
            return lambda x: x, {}
        else:
            msg = "unsupported nonlinearity: {}".format(nl)
            self._logger.error(msg)
            raise ValueError(msg)

    @staticmethod
    def tf_summary(params):
        for name, p in params.items():
            tf.summary.histogram(name, p)
            if len(p.get_shape().as_list()) == 2:
                tf.summary.image(name, tf.expand_dims(tf.expand_dims(p, 0), -1))
            elif len(p.get_shape().as_list()) == 3:  # split along last dim
                for i, split in enumerate(tf.split(p, p.get_shape()[-1].value, axis=2)):
                    tf.summary.image("{}-{}".format(i, name), tf.expand_dims(split, 0))
            elif len(p.get_shape().as_list()) == 1:
                tf.summary.image(name, tf.expand_dims(tf.expand_dims(tf.expand_dims(p, 0), 0), -1))
