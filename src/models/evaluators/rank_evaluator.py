from scipy.stats import pearsonr
import numpy as np

from src.models.evaluators.abstract_evaluator import AbstractEvaluator


class DecoyRankEvaluator(AbstractEvaluator):
    def __init__(self, config):
        super(DecoyRankEvaluator, self).__init__(config)

    def evaluate(self, predictions, scores):
        assert len(predictions) == len(scores), self._logger.error(
            "Length of the predictions and scores must be the same")
        n = len(predictions)
        pcc, p_value = pearsonr(predictions, scores)
        p_sort = np.argsort(predictions)
        s_sort = np.argsort(scores)
        rank_of_highest = []
        for i in range(n):
            common = len(set(p_sort[:i]).intersection(s_sort[:i])) / float(n)
            rank_of_highest.append(common)
        return {"Pearson Correlation": pcc,
                "Correlation P-Value": p_value,
                "Rank of Top Predictions": rank_of_highest,
                "AURTP": np.sum(rank_of_highest)}
