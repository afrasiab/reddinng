from abc import ABCMeta, abstractmethod

from src.utils.abstract_logger import AbstractLogger


class AbstractEvaluator(AbstractLogger):
    __metaclass__ = ABCMeta

    def __init__(self, config):
        self._config = config
        super(AbstractEvaluator, self).__init__()

    @abstractmethod
    def evaluate(self, model, data):
        pass
