import os
from abc import ABCMeta, abstractmethod
import tensorflow as tf
from tensorflow.python.training.gradient_descent import GradientDescentOptimizer

from src.models.abstract_model import AbstractModel

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractTensorFlowModel(AbstractModel):
    __metaclass__ = ABCMeta

    def __init__(self, conf, inputs, labels, outputs):
        super(AbstractTensorFlowModel, self).__init__()
        self._conf = conf
        # common tensors
        self.inputs = inputs
        self.outputs = outputs
        self.labels = labels
        self.loss = self._get_loss()
        self.train_op = None
        # session
        self.sess = tf.Session()
        self.summaries = tf.summary.merge_all()
        self.saver = tf.train.Saver()
        self.sess.run(tf.global_variables_initializer())
        self.tensorboard_output = os.path.join(self._conf["output_dir"], "tensorboard")
        if not os.path.exists(self.tensorboard_output):
            os.mkdir(self.tensorboard_output)
        self.summary_writer = tf.summary.FileWriter(self.tensorboard_output, self.sess.graph)

    @abstractmethod
    def train(self, dataset):
        # trains the model for one epoch with the dataset
        pass

    def _get_train_op(self):
        if self._conf["optimizer"]["name"] == "tf_sgd":
            return GradientDescentOptimizer(self._conf["optimizer"]["args"]["learning_rate"]).minimize(self.loss)

    def _get_loss(self):
        self._logger.info("")
        if self._conf["loss"]["type"] == "tf_weighted_mse":
            if "weights" in self._conf["loss"]["args"]:
                weights_type = self._conf["loss"]["args"]["weights"]
                if weights_type == "inverse_proportion":
                    weights = []
                    with tf.name_scope("loss"):
                        return tf.losses.huber_loss(self.labels, self.outputs)
