from src.models.layer import Layers, Models
from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ModelFactory(AbstractLogger):
    def __init__(self, configuration):
        super(ModelFactory, self).__init__()
        self._config = configuration

    def create(self, data_object):
        self._logger.info("Creating the models for {}".format(""))
        mini_batch_size = 32
        if "minibatch_size" in self._config["data"]["train"]:
            mini_batch_size = self._config["data"]["train"]["minibatch_size"]
        inputs = model_outputs = data_object.tf_inputs_tensor(mini_batch_size)
        labels = data_object.tf_labels_tensor(mini_batch_size)
        conf_updates = {}
        for layer_config in self._config["model"]["layers"]:
            layer_config.update(conf_updates)
            layer = Layers[layer_config["type"]](model_outputs, layer_config)
            model_outputs = layer.outputs
            conf_updates = layer.next_layer_conf
        model = Models[self._config["model"]["name"]](self._config["model"], inputs, labels, model_outputs)
        return model

