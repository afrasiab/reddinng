import os

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def extract():
    import multiprocessing
    return multiprocessing.cpu_count()


class Mine(object):
    def __init__(self):
        self.m = 100000
        pass

    def main(self):
        import dispy.httpd
        config_file = open(os.path.join(os.getenv("RD_CONF"), "dispy-machines"))
        nodes = []
        for line in config_file:
            nodes.append(line.strip())
        cluster = dispy.JobCluster(extract, nodes=nodes)
        http_server = dispy.httpd.DispyHTTPServer(cluster)
        jobs = []
        i = 0
        for _ in range(len(nodes)):
            node = i % len(nodes)
            job = cluster.submit_node(nodes[node])
            while job is None:
                i += 1
                node = i % len(nodes)
                job = cluster.submit_node(nodes[node])
            job.id = nodes[node]
            jobs.append(job)
            i += 1
        for job in jobs:
            print job.id, job()
        cluster.wait()
        # cluster.print_status()
        http_server.shutdown()
        cluster.close()


if __name__ == '__main__':
    Mine().main()
