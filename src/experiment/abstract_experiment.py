from abc import ABCMeta, abstractmethod
import logging
import coloredlogs
import os
import yaml
import logging.config
import numpy as np
import tensorflow as tf
from src.models.trainers.batch_trainer import BatchTrainer

from src.utils.abstract_logger import AbstractLogger

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractExperiment(AbstractLogger):
    __metaclass__ = ABCMeta

    def __init__(self, configuration):
        self._config = configuration
        self._setup_folders()
        self._set_random_seeds()
        # setting the log directory
        self._config["data"]["log_dir"] = self._log_dir
        conf_file = None if "logging_conf" not in self._config else self._config["logging_conf"]
        self._setup_logger(self._log_dir, conf_file=conf_file)
        super(AbstractExperiment, self).__init__()

    def _set_random_seeds(self):
        np.random.seed(self._config["seed"])
        tf.set_random_seed(self._config["seed"])

    @abstractmethod
    def run(self):
        pass

    def get_dataset(self):
        pass

    def _setup_logger(self, directory, default_level=logging.DEBUG, conf_file=None):
        if conf_file is None:
            conf_file = os.path.join(os.getenv("RD_CONF", None), "logging.yaml")
        if os.path.exists(conf_file):
            with open(conf_file, 'rt') as f:
                try:
                    config = yaml.safe_load(f.read())
                    info_file = config["handlers"]["info_file_handler"]["filename"]
                    config["handlers"]["info_file_handler"]["filename"] = info_file.replace("<>", directory)
                    info_file = config["handlers"]["error_file_handler"]["filename"]
                    config["handlers"]["error_file_handler"]["filename"] = info_file.replace("<>", directory)
                    info_file = config["handlers"]["debug_file_handler"]["filename"]
                    config["handlers"]["debug_file_handler"]["filename"] = info_file.replace("<>", directory)
                    info_file = config["handlers"]["critical_file_handler"]["filename"]
                    config["handlers"]["critical_file_handler"]["filename"] = info_file.replace("<>", directory)
                    info_file = config["handlers"]["warn_file_handler"]["filename"]
                    config["handlers"]["warn_file_handler"]["filename"] = info_file.replace("<>", directory)
                    logging.config.dictConfig(config)
                except Exception as e:
                    print(e)
                    print('Error in Logging Configuration. Using default configs')
                    logging.basicConfig(level=default_level)
        else:
            logging.basicConfig(level=default_level)
            print('Failed to load configuration file. Using default configs')
        self._logger = logging.getLogger("")
        coloredlogs.install(level=default_level, logger=self._logger)

    def _setup_folders(self):
        self._output_dir = os.path.join(self._config["output"], self._config["name"])
        if not os.path.exists(self._output_dir):
            os.mkdir(self._output_dir)
        self._log_dir = os.path.join(self._output_dir, "logs")
        if not os.path.exists(self._log_dir):
            os.mkdir(self._log_dir)

    def _get_trainer(self):
        if "minibatch_size" in self._config["data"]["train"]:
            return BatchTrainer(self._config["data"]["train"])
        else:
            raise NotImplemented()
