import copy

import numpy as np
import os
import yaml

from src.data import Datasets
from src.experiment.abstract_experiment import AbstractExperiment
from src.models.model_factory import ModelFactory

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class TrainTestExperiment(AbstractExperiment):

    def __init__(self, configuration):
        super(TrainTestExperiment, self).__init__(configuration)
        if "data" not in self._config:
            message = "Data tag is not specified in the configuration file"
            self._logger.error(message)
            raise ValueError(message)
        if "dataset" not in self._config["data"]:
            message = "Dataset tag is not specified in the configuration file"
            self._logger.error(message)
            raise ValueError(message)
        if "train" not in self._config["data"] or "test" not in self._config["data"]:
            message = "No training or testing dataset information specified in the dataset"
            self._logger.error(message)
            raise ValueError(message)
        if "train_list" not in self._config["data"]["train"]:
            self._config["data"]["train"]["train_list"] = "*"
        if "test_list" not in self._config["data"]["test"]:
            self._config["data"]["test"]["test_list"] = "*"

    def run(self):
        data_conf = copy.copy(self._config["data"])
        data_conf["reddinng"] = self._config["reddinng"]
        data_conf["seed"] = self._config["seed"]
        dataset = Datasets[data_conf["dataset"]](data_conf)
        train_ids, validation_ids, test_ids = self._split_dataset(dataset)
        self._logger.info("Preparing the training dataset with {}".format(train_ids.keys()))
        data_conf["sampling"] = self._config["data"]["train"]["sampling"]
        train_dataset = dataset.load(train_ids)
        self._logger.info("Preparing the validation dataset with {}".format(train_ids.keys()))
        data_conf["sampling"] = {"sampler": None, "sampling_ratio": 1.}
        validation_dataset = dataset.load(validation_ids)
        self._logger.info("Preparing the testing dataset with {}".format(test_ids.keys()))
        if "sampling" in self._config["data"]["test"]:
            data_conf["sampling"] = self._config["data"]["test"]["sampling"]
        else:
            data_conf["sampling"] = {"sampler": None, "sampling_ratio": 1.}
        test_dataset = dataset.load(test_ids)

        # creating the model
        an_object = train_dataset[train_ids.keys()[0]][0]
        self._config["model"]["output_dir"] = self._output_dir
        m = ModelFactory(self._config).create(an_object)
        # training the model
        trainer = self._get_trainer()
        trainer.train(m, train_dataset, validation_dataset)
        test_dataset = dataset.load(test_ids)
        m.predict(test_dataset)
        pass

    def _split_dataset(self, dataset_loader):
        complexes = None
        if "complexes" in self._config["data"]:
            if self._config["data"]["complexes"].endswith(".yaml"):
                c_file = os.path.join(self._config["reddinng"]["conf_directory"], self._config["data"]["complexes"])
                complexes = yaml.load(open(c_file))["complexes"]
            elif isinstance(self._config["data"]["complexes"], list):
                complexes = self._config["data"]["complexes"]
        if complexes is None:
            complexes = dataset_loader.get_complexes()
        complexes = list(set(complexes).difference(set(self._config["reddinng"]["problematic_complexes"])))

        split_type = self._config["data"]["split"]["type"]
        split_ratio = self._config["data"]["split"]["ratio"]
        if split_type == "decoy-wise":
            message = "decoy-wise split is not implemented yet!"
            self._logger.error(message)
            raise NotImplemented(message)
        # complex-wise split
        else:
            self._logger.info("splitting the decoys in a complex-wise manner")
            np.random.shuffle(complexes)
            train_split, validation_split = int(len(complexes) * split_ratio[0]), int(len(complexes) * split_ratio[1])
            train = complexes[:train_split]
            validation = complexes[train_split:train_split+validation_split]
            test = complexes[train_split+validation_split:]
            training_complexes = {}
            validation_complexes = {}
            testing_complexes = {}

            for a_complex in train:
                training_complexes[a_complex] = self._config["data"]["train"]["sampling"]["sampling_ratio"]
            for a_complex in validation:
                validation_complexes[a_complex] = 1.
            for a_complex in test:
                if "sampling" in self._config["data"]["test"] and \
                        "sample_ratio" in self._config["data"]["test"]["sampling"]:
                    testing_complexes[a_complex] = self._config["data"]["test"]["sampling"]["sample_ratio"]
                else:
                    testing_complexes[a_complex] = 1.
        return training_complexes, validation_complexes, testing_complexes
